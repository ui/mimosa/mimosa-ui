FROM node:20 as builder

COPY . /app
WORKDIR /app
RUN npm install -g pnpm && \
    pnpm install && \
    pnpm build

FROM nginx:alpine
COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]
