import React from 'react';
import { createRoot } from 'react-dom/client';
import { CacheProvider } from '@data-client/react';

import SuspenseRouter from './SuspenseRouter';
import App from './App';
import '@h5web/lib/dist/styles.css';
import './scss/main.scss';
import { AuthProvider } from 'hooks/useAuth';
import config from 'config/config';

const routerOptions: Record<string, any> = {};
if (config.routerBasename) routerOptions.basename = config.routerBasename;

const container = document.getElementById('root');
const root = createRoot(container!);
root.render(
  <React.StrictMode>
    <CacheProvider>
      <AuthProvider>
        <SuspenseRouter {...routerOptions}>
          <App />
        </SuspenseRouter>
      </AuthProvider>
    </CacheProvider>
  </React.StrictMode>
);
