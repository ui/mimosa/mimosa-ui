import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type SubsampleType = string;
export type Comments = null | string;
export type Blsampleid = number;
/**
 * Number of data collections
 */
export type Datacollections = number;
/**
 * Types of data collections
 */
export type Types = string[];
export type Blsubsampleid = number;
export type Name = string;
export type Posx = number;
export type Posy = number;

export interface SubSample {
  type?: SubsampleType;
  comments?: Comments;
  blSampleId: Blsampleid;
  _metadata: SubSampleMetaData;
  blSubSampleId: Blsubsampleid;
  BLSample: SubSampleSample;
  Position1?: Position;
  Position2?: Position;
}
export interface SubSampleMetaData {
  datacollections: Datacollections;
  types?: Types;
}
export interface SubSampleSample {
  name: Name;
}
export interface Position {
  posX: Posx;
  posY: Posy;
}


export abstract class SubSampleBase extends Entity {
  type?: SubsampleType;
  comments?: Comments;
  blSampleId: Blsampleid;
  _metadata: SubSampleMetaData;
  blSubSampleId: Blsubsampleid;
  BLSample: SubSampleSample;
  Position1?: Position;
  Position2?: Position;
}

export abstract class SubSampleSingletonBase extends SingletonEntity {
  type?: SubsampleType;
  comments?: Comments;
  blSampleId: Blsampleid;
  _metadata: SubSampleMetaData;
  blSubSampleId: Blsubsampleid;
  BLSample: SubSampleSample;
  Position1?: Position;
  Position2?: Position;
}

export abstract class SubSampleMetaDataBase extends Entity {
  datacollections: Datacollections;
  types?: Types;
}

export abstract class SubSampleMetaDataSingletonBase extends SingletonEntity {
  datacollections: Datacollections;
  types?: Types;
}

export abstract class SubSampleSampleBase extends Entity {
  name: Name;
}

export abstract class SubSampleSampleSingletonBase extends SingletonEntity {
  name: Name;
}

export abstract class PositionBase extends Entity {
  posX: Posx;
  posY: Posy;
}

export abstract class PositionSingletonBase extends SingletonEntity {
  posX: Posx;
  posY: Posy;
}

