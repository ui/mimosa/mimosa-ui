import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type Name = string;
export type Task = string;
export type ParamSchema = string;
export type Tasks = AvailableTask[];

export interface AvailableTasks {
  tasks: Tasks;
}
export interface AvailableTask {
  name: Name;
  task: Task;
  param_schema: ParamSchema;
}


export abstract class AvailableTasksBase extends Entity {
  tasks: Tasks;
}

export abstract class AvailableTasksSingletonBase extends SingletonEntity {
  tasks: Tasks;
}

export abstract class AvailableTaskBase extends Entity {
  name: Name;
  task: Task;
  param_schema: ParamSchema;
}

export abstract class AvailableTaskSingletonBase extends SingletonEntity {
  name: Name;
  task: Task;
  param_schema: ParamSchema;
}

