import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type Containerqueuesampleid = number;
export type Datacollectionplanid = number;
export type Blsubsampleid = number;
export type Blsampleid = number;
export type Name = string;
export type Type = string;
export type Diffractionplanid = number;
export type Recordtimestamp = string;
export type Monobandwidth = number;
export type Datacollectionid = number;
export type Runstatus = string;
/**
 * Related data collections
 */
export type Datacollections = ContainerQueueDataCollection[];
/**
 * Related dataCollectionGroupId
 */
export type Datacollectiongroupid = number;
/**
 * Related sessionId
 */
export type Sessionid = number;
/**
 * Related proposal
 */
export type Proposal = string;
/**
 * Time first datacollection started
 */
export type Started = string;
/**
 * Time last datacollection ended
 */
export type Finished = string;
/**
 * Types of data collections
 */
export type Types = string[];

export interface ContainerQueueSample {
  containerQueueSampleId: Containerqueuesampleid;
  dataCollectionPlanId: Datacollectionplanid;
  blSubSampleId?: Blsubsampleid;
  BLSample?: ContainerQueueBLSample;
  BLSubSample?: ContainerQueueBLSubSample;
  DiffractionPlan?: ContainerQueueDiffractionPlan;
  _metadata: ContainerQueueSampleMetaData;
}
export interface ContainerQueueBLSample {
  blSampleId: Blsampleid;
  name: Name;
}
export interface ContainerQueueBLSubSample {
  type: Type;
  BLSample: ContainerQueueBLSample;
}
export interface ContainerQueueDiffractionPlan {
  diffractionPlanId: Diffractionplanid;
  recordTimeStamp: Recordtimestamp;
  scanParameters?: Scanparameters;
  monoBandwidth?: Monobandwidth;
}
export interface Scanparameters {
}
export interface ContainerQueueSampleMetaData {
  datacollections: Datacollections;
  dataCollectionGroupId?: Datacollectiongroupid;
  sessionId?: Sessionid;
  proposal?: Proposal;
  started?: Started;
  finished?: Finished;
  types?: Types;
}
export interface ContainerQueueDataCollection {
  dataCollectionId: Datacollectionid;
  runStatus?: Runstatus;
}


export abstract class ContainerQueueSampleBase extends Entity {
  containerQueueSampleId: Containerqueuesampleid;
  dataCollectionPlanId: Datacollectionplanid;
  blSubSampleId?: Blsubsampleid;
  BLSample?: ContainerQueueBLSample;
  BLSubSample?: ContainerQueueBLSubSample;
  DiffractionPlan?: ContainerQueueDiffractionPlan;
  _metadata: ContainerQueueSampleMetaData;
}

export abstract class ContainerQueueSampleSingletonBase extends SingletonEntity {
  containerQueueSampleId: Containerqueuesampleid;
  dataCollectionPlanId: Datacollectionplanid;
  blSubSampleId?: Blsubsampleid;
  BLSample?: ContainerQueueBLSample;
  BLSubSample?: ContainerQueueBLSubSample;
  DiffractionPlan?: ContainerQueueDiffractionPlan;
  _metadata: ContainerQueueSampleMetaData;
}

export abstract class ContainerQueueBLSampleBase extends Entity {
  blSampleId: Blsampleid;
  name: Name;
}

export abstract class ContainerQueueBLSampleSingletonBase extends SingletonEntity {
  blSampleId: Blsampleid;
  name: Name;
}

export abstract class ContainerQueueBLSubSampleBase extends Entity {
  type: Type;
  BLSample: ContainerQueueBLSample;
}

export abstract class ContainerQueueBLSubSampleSingletonBase extends SingletonEntity {
  type: Type;
  BLSample: ContainerQueueBLSample;
}

export abstract class ContainerQueueDiffractionPlanBase extends Entity {
  diffractionPlanId: Diffractionplanid;
  recordTimeStamp: Recordtimestamp;
  scanParameters?: Scanparameters;
  monoBandwidth?: Monobandwidth;
}

export abstract class ContainerQueueDiffractionPlanSingletonBase extends SingletonEntity {
  diffractionPlanId: Diffractionplanid;
  recordTimeStamp: Recordtimestamp;
  scanParameters?: Scanparameters;
  monoBandwidth?: Monobandwidth;
}

export abstract class ScanparametersBase extends Entity {
}

export abstract class ScanparametersSingletonBase extends SingletonEntity {
}

export abstract class ContainerQueueSampleMetaDataBase extends Entity {
  datacollections: Datacollections;
  dataCollectionGroupId?: Datacollectiongroupid;
  sessionId?: Sessionid;
  proposal?: Proposal;
  started?: Started;
  finished?: Finished;
  types?: Types;
}

export abstract class ContainerQueueSampleMetaDataSingletonBase extends SingletonEntity {
  datacollections: Datacollections;
  dataCollectionGroupId?: Datacollectiongroupid;
  sessionId?: Sessionid;
  proposal?: Proposal;
  started?: Started;
  finished?: Finished;
  types?: Types;
}

export abstract class ContainerQueueDataCollectionBase extends Entity {
  dataCollectionId: Datacollectionid;
  runStatus?: Runstatus;
}

export abstract class ContainerQueueDataCollectionSingletonBase extends SingletonEntity {
  dataCollectionId: Datacollectionid;
  runStatus?: Runstatus;
}

