import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* eslint-disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

/**
 * Displayed at the top of the UI
 */
export type MessageOfTheDay = string;
export type GroupName = string;
/**
 * Display type to use in the UI
 */
export type UIGroup = string;
/**
 * Permission required to view all proposals from these beamlines
 */
export type Permission = string;
export type BeamlineName = string;
/**
 * Whether this beamline is archived (no longer displayed on landing page)
 */
export type Archived = boolean;
export type Beamlines = BeamLineGroupBeamLine[];
export type BeamlineGroups = BeamLineGroup[];
/**
 * Enable query debugging
 */
export type QueryDebugging = boolean;

/**
 * All available application options
 */
export interface Options {
  motd?: MessageOfTheDay;
  beamLineGroups?: BeamlineGroups;
  query_debug?: QueryDebugging;
}
export interface BeamLineGroup {
  groupName: GroupName;
  uiGroup: UIGroup;
  permission: Permission;
  beamLines?: Beamlines;
}
export interface BeamLineGroupBeamLine {
  beamLineName: BeamlineName;
  archived?: Archived;
}


export abstract class OptionsBase extends Entity {
  motd?: MessageOfTheDay;
  beamLineGroups?: BeamlineGroups;
  query_debug?: QueryDebugging;
}

export abstract class OptionsSingletonBase extends SingletonEntity {
  motd?: MessageOfTheDay;
  beamLineGroups?: BeamlineGroups;
  query_debug?: QueryDebugging;
}

export abstract class BeamLineGroupBase extends Entity {
  groupName: GroupName;
  uiGroup: UIGroup;
  permission: Permission;
  beamLines?: Beamlines;
}

export abstract class BeamLineGroupSingletonBase extends SingletonEntity {
  groupName: GroupName;
  uiGroup: UIGroup;
  permission: Permission;
  beamLines?: Beamlines;
}

export abstract class BeamLineGroupBeamLineBase extends Entity {
  beamLineName: BeamlineName;
  archived?: Archived;
}

export abstract class BeamLineGroupBeamLineSingletonBase extends SingletonEntity {
  beamLineName: BeamlineName;
  archived?: Archived;
}

