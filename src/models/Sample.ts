import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type Name = string;
export type Comments = null | string;
/**
 * Location in container
 */
export type Location = number;
export type Containerid = number;
export type Parent = SampleCrystal;
export type CellA = null | number;
export type CellB = null | number;
export type CellC = null | number;
export type CellAlpha = null | number;
export type CellBeta = null | number;
export type CellGamma = null | number;
export type Protein = SampleProtein;
export type Proposalid = string;
export type Name1 = string;
export type Acronym = string;
export type Crystalid = number;
export type Protein1 = number;
export type Blsampleid = number;
export type Container = SampleContainer;
export type Code = string;
/**
 * Position in sample change
 */
export type SampleChangerLocation = string;
/**
 * Beamline if container is assigned
 */
export type BeamlineLocation = string;
/**
 * Number of sub samples
 */
export type Subsamples = number;
/**
 * Number of data collections
 */
export type Datacollections = number;
/**
 * Types of data collections
 */
export type Types = null | string[];
/**
 * Whether this sample is queued for data collection
 */
export type Queued = boolean;
/**
 * Number of successful processings
 */
export type Processings = number;
/**
 * The associated proposal
 */
export type Proposal = string;

export interface Sample {
  name: Name;
  comments?: Comments;
  location?: Location;
  containerId?: Containerid;
  Crystal: Parent;
  blSampleId: Blsampleid;
  extraMetadata?: Extrametadata;
  Container?: Container;
  _metadata?: SampleMetaData;
}
export interface SampleCrystal {
  cell_a?: CellA;
  cell_b?: CellB;
  cell_c?: CellC;
  cell_alpha?: CellAlpha;
  cell_beta?: CellBeta;
  cell_gamma?: CellGamma;
  Protein: Protein;
  crystalId: Crystalid;
  proteinId: Protein1;
}
export interface SampleProtein {
  proposalId: Proposalid;
  name: Name1;
  acronym: Acronym;
}
export interface Extrametadata {
}
export interface SampleContainer {
  code: Code;
  sampleChangerLocation?: SampleChangerLocation;
  beamlineLocation?: BeamlineLocation;
}
export interface SampleMetaData {
  subsamples: Subsamples;
  datacollections: Datacollections;
  types?: Types;
  queued?: Queued;
  processings?: Processings;
  proposal?: Proposal;
}


export abstract class SampleBase extends Entity {
  name: Name;
  comments?: Comments;
  location?: Location;
  containerId?: Containerid;
  Crystal: Parent;
  blSampleId: Blsampleid;
  extraMetadata?: Extrametadata;
  Container?: Container;
  _metadata?: SampleMetaData;
}

export abstract class SampleSingletonBase extends SingletonEntity {
  name: Name;
  comments?: Comments;
  location?: Location;
  containerId?: Containerid;
  Crystal: Parent;
  blSampleId: Blsampleid;
  extraMetadata?: Extrametadata;
  Container?: Container;
  _metadata?: SampleMetaData;
}

export abstract class SampleCrystalBase extends Entity {
  cell_a?: CellA;
  cell_b?: CellB;
  cell_c?: CellC;
  cell_alpha?: CellAlpha;
  cell_beta?: CellBeta;
  cell_gamma?: CellGamma;
  Protein: Protein;
  crystalId: Crystalid;
  proteinId: Protein1;
}

export abstract class SampleCrystalSingletonBase extends SingletonEntity {
  cell_a?: CellA;
  cell_b?: CellB;
  cell_c?: CellC;
  cell_alpha?: CellAlpha;
  cell_beta?: CellBeta;
  cell_gamma?: CellGamma;
  Protein: Protein;
  crystalId: Crystalid;
  proteinId: Protein1;
}

export abstract class SampleProteinBase extends Entity {
  proposalId: Proposalid;
  name: Name1;
  acronym: Acronym;
}

export abstract class SampleProteinSingletonBase extends SingletonEntity {
  proposalId: Proposalid;
  name: Name1;
  acronym: Acronym;
}

export abstract class ExtrametadataBase extends Entity {
}

export abstract class ExtrametadataSingletonBase extends SingletonEntity {
}

export abstract class SampleContainerBase extends Entity {
  code: Code;
  sampleChangerLocation?: SampleChangerLocation;
  beamlineLocation?: BeamlineLocation;
}

export abstract class SampleContainerSingletonBase extends SingletonEntity {
  code: Code;
  sampleChangerLocation?: SampleChangerLocation;
  beamlineLocation?: BeamlineLocation;
}

export abstract class SampleMetaDataBase extends Entity {
  subsamples: Subsamples;
  datacollections: Datacollections;
  types?: Types;
  queued?: Queued;
  processings?: Processings;
  proposal?: Proposal;
}

export abstract class SampleMetaDataSingletonBase extends SingletonEntity {
  subsamples: Subsamples;
  datacollections: Datacollections;
  types?: Types;
  queued?: Queued;
  processings?: Processings;
  proposal?: Proposal;
}

