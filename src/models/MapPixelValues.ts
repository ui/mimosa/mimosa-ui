import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* eslint-disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type Datacollectiongroupid = number;
export type Xrffluorescencemappingroiid = number;
export type X = number;
export type Y = number;
export type Value = number[];

export interface MapPixelValues {
  dataCollectionGroupId: Datacollectiongroupid;
  xrfFluorescenceMappingROIId: Xrffluorescencemappingroiid;
  x?: X;
  y?: Y;
  value: Value;
}


export abstract class MapPixelValuesBase extends Entity {
  dataCollectionGroupId: Datacollectiongroupid;
  xrfFluorescenceMappingROIId: Xrffluorescencemappingroiid;
  x?: X;
  y?: Y;
  value: Value;
}

export abstract class MapPixelValuesSingletonBase extends SingletonEntity {
  dataCollectionGroupId: Datacollectiongroupid;
  xrfFluorescenceMappingROIId: Xrffluorescencemappingroiid;
  x?: X;
  y?: Y;
  value: Value;
}

