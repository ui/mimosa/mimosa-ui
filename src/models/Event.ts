import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type Id = number;
export type Type = string;
export type StartTime = string;
export type EndTime = string;
export type Duration = number;
export type Count = number;
export type Session = string;
export type Sessionid = number;
export type Proposal = string;
/**
 * Sample Name
 */
export type Blsample = string;
/**
 * Sample Id
 */
export type Blsampleid = number;
/**
 * No. of attachments
 */
export type Attachments = number;
export type Item = DataCollection | RobotAction | XFEFluorescenceSpectrum | EnergyScan;
/**
 * `Successful` on success
 */
export type Status = string;
/**
 * Directory where the data is saved
 */
export type Directory = string;
/**
 * File template for data
 */
export type DataFileTemplate = string;
/**
 * For hdf5 files, path to the images
 */
export type ImageSubPath = string;
export type NumberOfImagesPoints = number;
export type NumberOfPassesRepeats = number;
export type Wavelength = number;
export type ExposureTime = number;
export type Flux = number;
export type BeamPositionHorizontal = number;
export type BeamPositionVertical = number;
export type BeamSizeAtSampleHorizontal = number;
export type BeamSizeAtSampleVertical = number;
export type BeamTransmision = number;
/**
 * At edge of detector
 */
export type Resolution = number;
export type DetectorDistance = number;
export type RotationAxisStart = number;
export type RotationAxisEnd = number;
export type RotationAxisOscillation = number;
export type RotationAxisMotor = string;
export type RotationAxisOverlap = number;
export type PhiStart = number;
export type KappaStart = number;
export type OmegaStart = number;
export type ChiStart = number;
export type BeamSizeX = number;
export type BeamSizeY = number;
export type Datacollectionid = number;
export type Datacollectiongroupid = number;
export type Experimenttype = string;
export type Workflowid = number;
export type Comments = string;
export type Status1 = string;
export type Workflowtitle = string;
export type Workflowtype = string;
export type Gridinfoid = number;
export type Xoffset = number;
export type Yoffset = number;
export type DxMm = number;
export type DyMm = number;
export type StepsX = number;
export type StepsY = number;
export type Patchesx = number;
export type Patchesy = number;
export type Meshangle = number;
export type Orientation = string;
export type Pixelspermicronx = number;
export type Pixelspermicrony = number;
export type SnapshotOffsetxpixel = number;
export type SnapshotOffsetypixel = number;
export type Snaked = boolean;
export type Gridinfo = GridInfo[];
export type Blsubsampleid = number;
export type SubsampleType = string;
export type Robotactionid = number;
export type Actiontype = string;
export type Status2 = string;
export type Message = string;
export type Resultfilepath = string;
export type Xfefluorescencespectrumid = number;
export type Energyscanid = number;

export interface Event {
  id: Id;
  type: Type;
  startTime?: StartTime;
  endTime?: EndTime;
  duration?: Duration;
  count: Count;
  session?: Session;
  sessionId: Sessionid;
  proposal: Proposal;
  blSample?: Blsample;
  blSampleId?: Blsampleid;
  attachments?: Attachments;
  Item: Item;
}
export interface DataCollection {
  runStatus?: Status;
  imageDirectory?: Directory;
  fileTemplate?: DataFileTemplate;
  imageContainerSubPath?: ImageSubPath;
  numberOfImages?: NumberOfImagesPoints;
  numberOfPasses?: NumberOfPassesRepeats;
  wavelength?: Wavelength;
  exposureTime?: ExposureTime;
  flux?: Flux;
  xBeam?: BeamPositionHorizontal;
  yBeam?: BeamPositionVertical;
  beamSizeAtSampleX?: BeamSizeAtSampleHorizontal;
  beamSizeAtSampleY?: BeamSizeAtSampleVertical;
  transmission?: BeamTransmision;
  resolution?: Resolution;
  detectorDistance?: DetectorDistance;
  axisStart?: RotationAxisStart;
  axisEnd?: RotationAxisEnd;
  axisRange?: RotationAxisOscillation;
  rotationAxis?: RotationAxisMotor;
  overlap?: RotationAxisOverlap;
  phiStart?: PhiStart;
  kappaStart?: KappaStart;
  omegaStart?: OmegaStart;
  chiStart?: ChiStart;
  xBeamPix?: BeamSizeX;
  yBeamPix?: BeamSizeY;
  dataCollectionId: Datacollectionid;
  DataCollectionGroup: DataCollectionGroup;
  GridInfo?: Gridinfo;
  BLSubSample?: DCSubSample;
  _metadata: DataCollectionMetaData;
}
export interface DataCollectionGroup {
  dataCollectionGroupId: Datacollectiongroupid;
  experimentType?: Experimenttype;
  Workflow?: Workflow;
}
export interface Workflow {
  workflowId: Workflowid;
  comments?: Comments;
  status?: Status1;
  workflowTitle?: Workflowtitle;
  workflowType?: Workflowtype;
}
export interface GridInfo {
  gridInfoId: Gridinfoid;
  xOffset?: Xoffset;
  yOffset?: Yoffset;
  dx_mm?: DxMm;
  dy_mm?: DyMm;
  steps_x?: StepsX;
  steps_y?: StepsY;
  patchesX?: Patchesx;
  patchesY?: Patchesy;
  meshAngle?: Meshangle;
  orientation?: Orientation;
  pixelsPerMicronX?: Pixelspermicronx;
  pixelsPerMicronY?: Pixelspermicrony;
  snapshot_offsetXPixel?: SnapshotOffsetxpixel;
  snapshot_offsetYPixel?: SnapshotOffsetypixel;
  snaked?: Snaked;
}
export interface DCSubSample {
  blSubSampleId: Blsubsampleid;
  type?: SubsampleType;
}
export interface DataCollectionMetaData {
  snapshots: Snapshots;
}
/**
 * Snapshot statuses with ids 1-4
 */
export interface Snapshots {
  [k: string]: boolean;
}
export interface RobotAction {
  robotActionId: Robotactionid;
  actionType: Actiontype;
  status?: Status2;
  message?: Message;
  resultFilePath?: Resultfilepath;
  _metadata: RobotActionMetaData;
}
export interface RobotActionMetaData {
  snapshots: Snapshots1;
}
/**
 * Snapshot statuses with keys `before` and `after`
 */
export interface Snapshots1 {
  [k: string]: boolean;
}
export interface XFEFluorescenceSpectrum {
  xfeFluorescenceSpectrumId: Xfefluorescencespectrumid;
}
export interface EnergyScan {
  energyScanId: Energyscanid;
}


export abstract class EventBase extends Entity {
  id: Id;
  type: Type;
  startTime?: StartTime;
  endTime?: EndTime;
  duration?: Duration;
  count: Count;
  session?: Session;
  sessionId: Sessionid;
  proposal: Proposal;
  blSample?: Blsample;
  blSampleId?: Blsampleid;
  attachments?: Attachments;
  Item: Item;
}

export abstract class EventSingletonBase extends SingletonEntity {
  id: Id;
  type: Type;
  startTime?: StartTime;
  endTime?: EndTime;
  duration?: Duration;
  count: Count;
  session?: Session;
  sessionId: Sessionid;
  proposal: Proposal;
  blSample?: Blsample;
  blSampleId?: Blsampleid;
  attachments?: Attachments;
  Item: Item;
}

export abstract class DataCollectionBase extends Entity {
  runStatus?: Status;
  imageDirectory?: Directory;
  fileTemplate?: DataFileTemplate;
  imageContainerSubPath?: ImageSubPath;
  numberOfImages?: NumberOfImagesPoints;
  numberOfPasses?: NumberOfPassesRepeats;
  wavelength?: Wavelength;
  exposureTime?: ExposureTime;
  flux?: Flux;
  xBeam?: BeamPositionHorizontal;
  yBeam?: BeamPositionVertical;
  beamSizeAtSampleX?: BeamSizeAtSampleHorizontal;
  beamSizeAtSampleY?: BeamSizeAtSampleVertical;
  transmission?: BeamTransmision;
  resolution?: Resolution;
  detectorDistance?: DetectorDistance;
  axisStart?: RotationAxisStart;
  axisEnd?: RotationAxisEnd;
  axisRange?: RotationAxisOscillation;
  rotationAxis?: RotationAxisMotor;
  overlap?: RotationAxisOverlap;
  phiStart?: PhiStart;
  kappaStart?: KappaStart;
  omegaStart?: OmegaStart;
  chiStart?: ChiStart;
  xBeamPix?: BeamSizeX;
  yBeamPix?: BeamSizeY;
  dataCollectionId: Datacollectionid;
  DataCollectionGroup: DataCollectionGroup;
  GridInfo?: Gridinfo;
  BLSubSample?: DCSubSample;
  _metadata: DataCollectionMetaData;
}

export abstract class DataCollectionSingletonBase extends SingletonEntity {
  runStatus?: Status;
  imageDirectory?: Directory;
  fileTemplate?: DataFileTemplate;
  imageContainerSubPath?: ImageSubPath;
  numberOfImages?: NumberOfImagesPoints;
  numberOfPasses?: NumberOfPassesRepeats;
  wavelength?: Wavelength;
  exposureTime?: ExposureTime;
  flux?: Flux;
  xBeam?: BeamPositionHorizontal;
  yBeam?: BeamPositionVertical;
  beamSizeAtSampleX?: BeamSizeAtSampleHorizontal;
  beamSizeAtSampleY?: BeamSizeAtSampleVertical;
  transmission?: BeamTransmision;
  resolution?: Resolution;
  detectorDistance?: DetectorDistance;
  axisStart?: RotationAxisStart;
  axisEnd?: RotationAxisEnd;
  axisRange?: RotationAxisOscillation;
  rotationAxis?: RotationAxisMotor;
  overlap?: RotationAxisOverlap;
  phiStart?: PhiStart;
  kappaStart?: KappaStart;
  omegaStart?: OmegaStart;
  chiStart?: ChiStart;
  xBeamPix?: BeamSizeX;
  yBeamPix?: BeamSizeY;
  dataCollectionId: Datacollectionid;
  DataCollectionGroup: DataCollectionGroup;
  GridInfo?: Gridinfo;
  BLSubSample?: DCSubSample;
  _metadata: DataCollectionMetaData;
}

export abstract class DataCollectionGroupBase extends Entity {
  dataCollectionGroupId: Datacollectiongroupid;
  experimentType?: Experimenttype;
  Workflow?: Workflow;
}

export abstract class DataCollectionGroupSingletonBase extends SingletonEntity {
  dataCollectionGroupId: Datacollectiongroupid;
  experimentType?: Experimenttype;
  Workflow?: Workflow;
}

export abstract class WorkflowBase extends Entity {
  workflowId: Workflowid;
  comments?: Comments;
  status?: Status1;
  workflowTitle?: Workflowtitle;
  workflowType?: Workflowtype;
}

export abstract class WorkflowSingletonBase extends SingletonEntity {
  workflowId: Workflowid;
  comments?: Comments;
  status?: Status1;
  workflowTitle?: Workflowtitle;
  workflowType?: Workflowtype;
}

export abstract class GridInfoBase extends Entity {
  gridInfoId: Gridinfoid;
  xOffset?: Xoffset;
  yOffset?: Yoffset;
  dx_mm?: DxMm;
  dy_mm?: DyMm;
  steps_x?: StepsX;
  steps_y?: StepsY;
  patchesX?: Patchesx;
  patchesY?: Patchesy;
  meshAngle?: Meshangle;
  orientation?: Orientation;
  pixelsPerMicronX?: Pixelspermicronx;
  pixelsPerMicronY?: Pixelspermicrony;
  snapshot_offsetXPixel?: SnapshotOffsetxpixel;
  snapshot_offsetYPixel?: SnapshotOffsetypixel;
  snaked?: Snaked;
}

export abstract class GridInfoSingletonBase extends SingletonEntity {
  gridInfoId: Gridinfoid;
  xOffset?: Xoffset;
  yOffset?: Yoffset;
  dx_mm?: DxMm;
  dy_mm?: DyMm;
  steps_x?: StepsX;
  steps_y?: StepsY;
  patchesX?: Patchesx;
  patchesY?: Patchesy;
  meshAngle?: Meshangle;
  orientation?: Orientation;
  pixelsPerMicronX?: Pixelspermicronx;
  pixelsPerMicronY?: Pixelspermicrony;
  snapshot_offsetXPixel?: SnapshotOffsetxpixel;
  snapshot_offsetYPixel?: SnapshotOffsetypixel;
  snaked?: Snaked;
}

export abstract class DCSubSampleBase extends Entity {
  blSubSampleId: Blsubsampleid;
  type?: SubsampleType;
}

export abstract class DCSubSampleSingletonBase extends SingletonEntity {
  blSubSampleId: Blsubsampleid;
  type?: SubsampleType;
}

export abstract class DataCollectionMetaDataBase extends Entity {
  snapshots: Snapshots;
}
/**
 * Snapshot statuses with ids 1-4
 */

export abstract class DataCollectionMetaDataSingletonBase extends SingletonEntity {
  snapshots: Snapshots;
}
/**
 * Snapshot statuses with ids 1-4
 */

export abstract class RobotActionBase extends Entity {
  robotActionId: Robotactionid;
  actionType: Actiontype;
  status?: Status2;
  message?: Message;
  resultFilePath?: Resultfilepath;
  _metadata: RobotActionMetaData;
}

export abstract class RobotActionSingletonBase extends SingletonEntity {
  robotActionId: Robotactionid;
  actionType: Actiontype;
  status?: Status2;
  message?: Message;
  resultFilePath?: Resultfilepath;
  _metadata: RobotActionMetaData;
}

export abstract class RobotActionMetaDataBase extends Entity {
  snapshots: Snapshots1;
}
/**
 * Snapshot statuses with keys `before` and `after`
 */

export abstract class RobotActionMetaDataSingletonBase extends SingletonEntity {
  snapshots: Snapshots1;
}
/**
 * Snapshot statuses with keys `before` and `after`
 */

export abstract class XFEFluorescenceSpectrumBase extends Entity {
  xfeFluorescenceSpectrumId: Xfefluorescencespectrumid;
}

export abstract class XFEFluorescenceSpectrumSingletonBase extends SingletonEntity {
  xfeFluorescenceSpectrumId: Xfefluorescencespectrumid;
}

export abstract class EnergyScanBase extends Entity {
  energyScanId: Energyscanid;
}

export abstract class EnergyScanSingletonBase extends SingletonEntity {
  energyScanId: Energyscanid;
}

