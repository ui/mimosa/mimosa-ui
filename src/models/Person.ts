import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type Personid = number;
export type Givenname = string;
export type Familyname = string;
/**
 * Number of sessions this person has been on
 */
export type Sessions = number;
/**
 * Last session date
 */
export type Lastsession = string;
export type Role = string;
export type Remote = boolean;

export interface Person {
  personId: Personid;
  givenName: Givenname;
  familyName: Familyname;
  _metadata?: PersonMetaData;
}
export interface PersonMetaData {
  sessions?: Sessions;
  lastSession?: Lastsession;
  role?: Role;
  remote?: Remote;
}


export abstract class PersonBase extends Entity {
  personId: Personid;
  givenName: Givenname;
  familyName: Familyname;
  _metadata?: PersonMetaData;
}

export abstract class PersonSingletonBase extends SingletonEntity {
  personId: Personid;
  givenName: Givenname;
  familyName: Familyname;
  _metadata?: PersonMetaData;
}

export abstract class PersonMetaDataBase extends Entity {
  sessions?: Sessions;
  lastSession?: Lastsession;
  role?: Role;
  remote?: Remote;
}

export abstract class PersonMetaDataSingletonBase extends SingletonEntity {
  sessions?: Sessions;
  lastSession?: Lastsession;
  role?: Role;
  remote?: Remote;
}

