import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* eslint-disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type Blsampleimageid = number;
export type Blsampleid = number;
export type Micronsperpixelx = number;
export type Micronsperpixely = number;
export type Offsetx = number;
export type Offsety = number;
export type Imagefullpath = string;
/**
 * Url to sample image
 */
export type Url = string;
export type Width = number;
export type Height = number;

export interface SampleImage {
  blSampleImageId: Blsampleimageid;
  blSampleId: Blsampleid;
  micronsPerPixelX: Micronsperpixelx;
  micronsPerPixelY: Micronsperpixely;
  offsetX: Offsetx;
  offsetY: Offsety;
  imageFullPath: Imagefullpath;
  _metadata: SampleImageMetaData;
}
export interface SampleImageMetaData {
  url: Url;
  width?: Width;
  height?: Height;
}


export abstract class SampleImageBase extends Entity {
  blSampleImageId: Blsampleimageid;
  blSampleId: Blsampleid;
  micronsPerPixelX: Micronsperpixelx;
  micronsPerPixelY: Micronsperpixely;
  offsetX: Offsetx;
  offsetY: Offsety;
  imageFullPath: Imagefullpath;
  _metadata: SampleImageMetaData;
}

export abstract class SampleImageSingletonBase extends SingletonEntity {
  blSampleImageId: Blsampleimageid;
  blSampleId: Blsampleid;
  micronsPerPixelX: Micronsperpixelx;
  micronsPerPixelY: Micronsperpixely;
  offsetX: Offsetx;
  offsetY: Offsety;
  imageFullPath: Imagefullpath;
  _metadata: SampleImageMetaData;
}

export abstract class SampleImageMetaDataBase extends Entity {
  url: Url;
  width?: Width;
  height?: Height;
}

export abstract class SampleImageMetaDataSingletonBase extends SingletonEntity {
  url: Url;
  width?: Width;
  height?: Height;
}

