import { Entity } from '@data-client/rest';
import { SingletonEntity } from 'api/resources/Base/Singleton';

/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

/**
 * An enumeration.
 */
export type StatusEnum = null | 0 | 1 | 2;
export type Autoprocprogramid = number;
export type Automatic = boolean;

export interface ProcessingStatusesList {
  statuses: Statuses;
}
export interface Statuses {
  [k: string]: ProcessingStatuses;
}
export interface ProcessingStatuses {
  processing?: Processing;
}
export interface Processing {
  [k: string]: ProcessingProcessingStatus[];
}
export interface ProcessingProcessingStatus {
  status?: StatusEnum;
  autoProcProgramId: Autoprocprogramid;
  automatic: Automatic;
}


export abstract class ProcessingStatusesListBase extends Entity {
  statuses: Statuses;
}

export abstract class ProcessingStatusesListSingletonBase extends SingletonEntity {
  statuses: Statuses;
}

export abstract class ProcessingStatusesBase extends Entity {
  processing?: Processing;
}

export abstract class ProcessingStatusesSingletonBase extends SingletonEntity {
  processing?: Processing;
}

export abstract class ProcessingProcessingStatusBase extends Entity {
  status?: StatusEnum;
  autoProcProgramId: Autoprocprogramid;
  automatic: Automatic;
}

export abstract class ProcessingProcessingStatusSingletonBase extends SingletonEntity {
  status?: StatusEnum;
  autoProcProgramId: Autoprocprogramid;
  automatic: Automatic;
}

