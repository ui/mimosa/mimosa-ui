export function initMode() {
  setMode(getMode());
}

export function getMode() {
  const osDarkMode = window.matchMedia('(prefers-color-scheme:dark)').matches
    ? '1'
    : '0';
  return Boolean(
    parseFloat(
      window.localStorage.getItem('mimosa:dark-mode') ?? osDarkMode ?? '0'
    )
  );
}

export function setMode(darkMode: boolean, save = false) {
  document.documentElement.setAttribute(
    'data-bs-theme',
    darkMode ? 'dark' : 'light'
  );
  if (save) {
    window.localStorage.setItem('mimosa:dark-mode', darkMode ? '1' : '0');
  }
}
