import { DateTime } from 'luxon';

export default function formattedDateTime(
  datetime: string,
  seconds: boolean = true
) {
  return (
    datetime &&
    DateTime.fromISO(datetime)
      .toLocaleString({
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: seconds ? 'numeric' : undefined,
      })
      .replace(',', '')
  );
}
