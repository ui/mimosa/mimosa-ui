import { Link } from 'react-router-dom';
import { useProposalInfo } from 'hooks/useProposalInfo';

interface ISampleLink {
  proposal: string;
  blSampleId?: number;
  blSample?: string;
}

export default function SampleLink(props: ISampleLink) {
  const proposalInfo = useProposalInfo();
  const isMapping =
    proposalInfo &&
    proposalInfo._metadata.uiGroups &&
    proposalInfo._metadata.uiGroups.includes('mapping');
  const link = isMapping
    ? `/proposals/${props.proposal}/samples/review?blSampleId=${props.blSampleId}`
    : `/proposals/${props.proposal}/samples/${props.blSampleId}`;
  return <Link to={link}>{props.blSample}</Link>;
}
