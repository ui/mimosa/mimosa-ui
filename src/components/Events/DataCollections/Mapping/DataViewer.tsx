import { Suspense, useState, useMemo } from 'react';
import { Form } from 'react-bootstrap';
import { useInView } from 'react-intersection-observer';
import { useSuspense } from '@data-client/react';
import {
  HeatmapVis,
  Domain,
  DomainWidget,
  LineVis,
  getDomain,
  Toolbar,
  ScaleSelector,
  ColorMapSelector,
  ColorMap,
  Separator,
  ScaleType,
  CustomDomain,
  useVisDomain,
  useSafeDomain,
  VisScaleType,
  ToggleBtn,
} from '@h5web/lib';
import ndarray from 'ndarray';

import { H5DataEndpoint } from 'api/resources/H5Grove/Data';
import { H5MetaEndpoint } from 'api/resources/H5Grove/Meta';
import { H5HistogramEndpoint } from 'api/resources/H5Grove/Histogram';
import NetworkErrorPage from 'components/NetworkErrorPage';
import { DataCollection as DataCollectionType } from 'models/Event';
import { DataHistogram as DataHistogramType } from 'models/DataHistogram';

function useDataSeries({
  dataCollectionId,
  path,
}: {
  dataCollectionId: number;
  path: string;
}): Record<string, Series[]> {
  const meta = useSuspense(H5MetaEndpoint, {
    dataCollectionId,
    path: path,
  });
  const children = meta.children as Series[];
  return useMemo(() => {
    const twoD = children.filter((child) => child.shape.length === 2);
    const threeD = children.filter((child) => child.shape.length === 3);
    const seriesTypes: Record<string, Series[]> = {};

    if (twoD.length) seriesTypes['2'] = twoD;
    if (threeD.length) seriesTypes['3'] = threeD;
    return seriesTypes;
  }, [children]);
}

function useDataPoint({
  dataCollectionId,
  path,
  selectedPoint,
  flatten = false,
  fetch = true,
}: {
  dataCollectionId: number;
  path: string;
  selectedPoint: number;
  flatten: boolean;
  fetch: boolean;
}) {
  const data = useSuspense(
    H5DataEndpoint,
    fetch
      ? {
          dataCollectionId,
          path: path,
          // Selection in h5grove is zero-offset
          selection: selectedPoint - 1,
          flatten,
        }
      : null
  );
  return data && data.data;
}

function useDataHistogram({
  dataCollectionId,
  path,
  selectedPoint,
  fetch = true,
}: {
  dataCollectionId: number;
  path: string;
  selectedPoint: number;
  fetch: boolean;
}) {
  return useSuspense(
    H5HistogramEndpoint,
    fetch
      ? {
          dataCollectionId,
          path: path,
          // Selection in h5grove is zero-offset
          selection: selectedPoint - 1,
        }
      : null
  );
}

// const COLORS = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728'];
const DEFAULT_DOMAIN: Domain = [0.1, 1];

interface PlotData {
  data: number[];
  series: Series[];
}

interface PlotData2D extends PlotData {
  histogram?: DataHistogramType;
  autoscale?: boolean;
}

function Plot1d({ data, series }: PlotData) {
  const dataArray = useMemo(() => ndarray(data), [data]);
  const yDomain = getDomain(dataArray) || DEFAULT_DOMAIN;

  return (
    <div className="h5web-plot">
      <LineVis
        dataArray={dataArray}
        domain={yDomain}
        abscissaParams={{
          label: 'Channel',
        }}
        ordinateLabel="Count"
        className="h5web-plot-inner"
      />
    </div>
  );
}

export const ZOOM_KEY = 'Shift';

function Plot2d({ data, series, histogram, autoscale }: PlotData2D) {
  const [autoScale, setAutoScale] = useState<boolean>(
    autoscale == undefined ? true : autoscale
  );
  const dataArray = useMemo(
    () =>
      ndarray(new Float32Array(data), [series[0].shape[1], series[0].shape[2]]),
    [data, series]
  );
  const maxBin = histogram ? histogram.bins[histogram?.bins.length - 1] : 0;
  const dataDomain =
    useMemo(
      () => (autoScale ? ([0, maxBin] as Domain) : getDomain(dataArray)),
      [dataArray, autoScale]
    ) || DEFAULT_DOMAIN;
  const [customDomain, setCustomDomain] = useState<CustomDomain>([null, null]);
  const [scaleType, setScaleType] = useState<ScaleType>(ScaleType.Linear);
  const [colorMap, setColorMap] = useState<ColorMap>('Viridis');
  const [invertColorMap, toggleColorMapInversion] = useState<boolean>(false);

  const visDomain = useVisDomain(customDomain, dataDomain);
  const [safeDomain] = useSafeDomain(visDomain, dataDomain, scaleType);

  return (
    <div className="h5web-plot" style={{ flexDirection: 'column' }}>
      <div style={{ flex: 0 }}>
        <Toolbar>
          {dataDomain && (
            <DomainWidget
              customDomain={customDomain}
              dataDomain={dataDomain}
              onCustomDomainChange={setCustomDomain}
              scaleType={ScaleType.Linear}
              histogram={histogram}
            />
          )}
          <Separator />
          <ColorMapSelector
            onInversionChange={() => toggleColorMapInversion(!invertColorMap)}
            onValueChange={setColorMap}
            value={colorMap}
            invert={invertColorMap}
          />
          <Separator />
          <ScaleSelector
            onScaleChange={setScaleType}
            options={[ScaleType.Linear, ScaleType.Log, ScaleType.SymLog]}
            value={scaleType}
          />
          <ToggleBtn
            label="Autoscale"
            onToggle={() => setAutoScale(!autoScale)}
            value={autoScale}
          />
        </Toolbar>
      </div>
      <HeatmapVis
        aspect="equal"
        dataArray={dataArray}
        domain={safeDomain}
        colorMap={colorMap}
        invertColorMap={invertColorMap}
        scaleType={scaleType as VisScaleType}
        className="h5web-plot-inner"
        ignoreValue={(v) => v > (histogram?.mask_value || 0) - 1}
      />
    </div>
  );
}

interface Series {
  attributes: Record<string, any>[];
  name: string;
  shape: number[];
}

interface IDataPlot extends Omit<IDataViewer, 'lastSeries' | 'setLastSeries'> {
  series: Series[];
  imageContainerSubPath?: string;
}

function DataPlot(props: IDataPlot) {
  const { dataCollection, selectedPoint, series, imageContainerSubPath } =
    props;
  const { dataCollectionId } = dataCollection;
  const data = useDataPoint({
    dataCollectionId,
    path: `${imageContainerSubPath}/${series?.[0].name}`,
    selectedPoint,
    flatten: series?.[0].shape.length === 3,
    fetch: series && series.length > 0,
  });
  const histogram = useDataHistogram({
    dataCollectionId,
    path: `${imageContainerSubPath}/${series?.[0].name}`,
    selectedPoint,
    fetch: series && series.length > 0,
  });

  if (!data) return <p>No Data</p>;
  if (!(series && series.length > 0)) return <p>No Data</p>;
  return series[0].shape.length === 2 ? (
    <Plot1d data={data} series={series} />
  ) : (
    <Plot2d data={data} series={series} histogram={histogram} autoscale />
  );
}

interface IDataViewer {
  selectedPoint: number;
  dataCollection: DataCollectionType;
  lastSeries?: string;
  setLastSeries?: (series: string) => void;
}

export function DataViewerMain(props: IDataViewer) {
  const { dataCollectionId } = props.dataCollection;
  let selectedPoint = props.selectedPoint;
  let imageContainerSubPath = props.dataCollection.imageContainerSubPath || '/';

  const gridInfo = props.dataCollection.GridInfo?.[0];
  // Handle patched scan data
  if (
    gridInfo &&
    ((gridInfo.patchesX || 1) > 1 || (gridInfo.patchesY || 1) > 1)
  ) {
    selectedPoint--;
    const patchStepsX = (gridInfo.steps_x || 1) / (gridInfo.patchesX || 1);
    const patchStepsY = (gridInfo.steps_y || 1) / (gridInfo.patchesY || 1);
    const patchX = Math.floor(
      (selectedPoint % (gridInfo.steps_x || 1)) / patchStepsX
    );
    const patchY = Math.floor(
      selectedPoint / (gridInfo.steps_x || 1) / patchStepsY
    );
    const patchXStep = (selectedPoint % (gridInfo.steps_x || 1)) % patchStepsX;
    const patchYStep = Math.floor(
      (selectedPoint / (gridInfo.steps_x || 1)) % patchStepsY
    );
    const scanId = patchY * (gridInfo.patchesX || 1) + patchX + 2;
    const scanPointId = patchXStep + patchYStep * patchStepsX;
    imageContainerSubPath = imageContainerSubPath.replace('1.1', `${scanId}.1`);
    selectedPoint = scanPointId + 1;
  }

  const seriesTypes = useDataSeries({
    dataCollectionId,
    path: '/' + imageContainerSubPath,
  });

  const [selectedSeries, setSelectedSeries] = useState<string>(
    props.lastSeries
      ? props.lastSeries
      : Object.keys(seriesTypes).length
      ? Object.keys(seriesTypes)[0]
      : '2'
  );

  return (
    <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
      {Object.keys(seriesTypes).length > 1 && (
        <Form.Control
          as="select"
          defaultValue={selectedSeries}
          onChange={(evt) => {
            setSelectedSeries(evt.target.value);
            props.setLastSeries?.(evt.target.value);
          }}
        >
          {Object.entries(seriesTypes).map(
            ([seriesType, series]: [string, Series[]]) => (
              <option key={seriesType} value={seriesType}>
                {series.map((serie) => serie.name).join(',')}
              </option>
            )
          )}
        </Form.Control>
      )}
      {selectedPoint !== props.selectedPoint && (
        <div className="text-center p-1">
          Scan: {imageContainerSubPath} Point: {selectedPoint}
        </div>
      )}
      <Suspense fallback="Loading...">
        <DataPlot
          dataCollection={props.dataCollection}
          selectedPoint={selectedPoint}
          series={seriesTypes[selectedSeries]}
          imageContainerSubPath={imageContainerSubPath}
        />
      </Suspense>
    </div>
  );
}

export default function DataViewer(props: IDataViewer) {
  const { ref, inView } = useInView({ threshold: 0.1, triggerOnce: true });
  return (
    <>
      {inView && (
        <Suspense fallback={'Loading...'}>
          <NetworkErrorPage>
            <DataViewerMain {...props} />
          </NetworkErrorPage>
        </Suspense>
      )}
      {!inView && <div ref={ref}></div>}
    </>
  );
}
