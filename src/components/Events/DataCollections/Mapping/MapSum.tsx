import { Suspense } from 'react';
import { useSuspense } from '@data-client/react';
import { MapPixelValuesEndpoint } from 'api/resources/MapPixelValues';
import { useInView } from 'react-intersection-observer';
import PlotEnhancer from 'components/Stats/PlotEnhancer';
import { range } from 'lodash';

interface MapSumProps {
  dataCollectionGroupId: number;
  xrfFluorescenceMappingROIId: number;
  selectedXY?: { x: number; y: number };
  onPointClick?: (pointIndex: number) => void;
}

function MapSumMain({
  dataCollectionGroupId,
  xrfFluorescenceMappingROIId,
  selectedXY,
  onPointClick,
}: MapSumProps) {
  const pixelValues = useSuspense(MapPixelValuesEndpoint, {
    dataCollectionGroupId,
    xrfFluorescenceMappingROIId,
    ...(selectedXY ? { x: selectedXY.x } : null),
    ...(selectedXY ? { y: selectedXY.y } : null),
  });

  return (
    <PlotEnhancer
      data={[
        {
          x: range(pixelValues.value.length),
          y: pixelValues.value,
          type: 'scatter',
        },
      ]}
      layout={{
        title: selectedXY ? 'Pixel' : 'Map Sum',
        xaxis: { title: 'Map' },
        yaxis: {
          title: 'Count',
        },
      }}
      onClick={(e) => {
        if (onPointClick) {
          const { points } = e;
          if (!points.length) return;
          onPointClick(points[0].pointIndex);
        }
      }}
    />
  );
}

function Loading() {
  return <div className="ms-1 border border-light">Loading...</div>;
}

export default function MapSum(props: MapSumProps) {
  const { ref, inView } = useInView({ threshold: 0.1, triggerOnce: true });
  return (
    <>
      {inView && (
        <Suspense fallback={<Loading />}>
          <MapSumMain {...props} />
        </Suspense>
      )}
      {!inView && <div ref={ref}></div>}
    </>
  );
}
