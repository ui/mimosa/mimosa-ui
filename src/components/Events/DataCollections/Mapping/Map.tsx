import { useState } from 'react';
import { Badge, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import Metadata from 'components/Events/Metadata';
import { DataCollectionBox } from 'components/Events/DataCollection';
import { IDataCollection } from 'components/Events/DataCollections/Default';
import SubSampleType from 'components/Samples/SubSampleType';
import { toEnergy } from 'utils/numbers';

import GridPlot from './GridPlot';
import DataViewer from './DataViewer';
import StatusBadge from '../StatusBadge';
import SampleLink from '../SampleLink';

export default function Map(props: IDataCollection) {
  const { parent, item } = props;
  const [selectedPoint, setSelectedPoint] = useState<number | undefined>(
    undefined
  );

  const res = (
    <Row className="g-0">
      <Col md="4">
        <Metadata
          properties={[
            {
              title: 'Sample',
              test: parent.blSample,
              content: (
                <>
                  <SampleLink
                    proposal={parent.proposal}
                    blSample={parent.blSample}
                    blSampleId={parent.blSampleId}
                  />
                  {item.BLSubSample?.type && (
                    <SubSampleType
                      className="ms-1"
                      type={item.BLSubSample.type}
                    />
                  )}
                </>
              ),
            },
            {
              title: 'Group',
              test: parent.count > 1,
              content: (
                <Link
                  to={`/proposals/${parent.proposal}/sessions/${parent.sessionId}?dataCollectionGroupId=${item.DataCollectionGroup.dataCollectionGroupId}`}
                >
                  {parent.count} Data Collections
                </Link>
              ),
            },
            {
              title: 'Type',
              content: (
                <>
                  {item.DataCollectionGroup.experimentType}
                  {((item.GridInfo?.[0]?.patchesX || 1) > 1 ||
                    (item.GridInfo?.[0]?.patchesY || 1) > 1) && (
                    <Badge className="ms-1">Patched</Badge>
                  )}
                </>
              ),
            },
            {
              title: 'Status',
              content: <StatusBadge status={item.runStatus} />,
            },
            {
              title: 'Finished',
              content: parent.endTime,
              test: parent.endTime,
            },
            {
              title: 'Duration',
              content: parent.duration ? Math.round(parent.duration) : 0,
              unit: 'min',
            },
            {
              title: 'Energy',
              content: item.wavelength && toEnergy(item.wavelength),
              unit: 'keV',
            },
            { title: 'No. Points', content: item.numberOfImages },
            {
              title: 'Exposure Time',
              content: item.exposureTime,
              unit: 's',
            },
            ...(item.GridInfo?.length
              ? [
                  {
                    title: 'Step Size',
                    content: `${(item.GridInfo?.[0].dx_mm || 0) * 1e3} x ${
                      (item.GridInfo?.[0].dy_mm || 0) * 1e3
                    }`,
                    unit: 'µm',
                  },
                  {
                    title: 'Steps',
                    content: `${item.GridInfo?.[0].steps_x} x ${item.GridInfo?.[0].steps_y}`,
                  },
                  ...((item.GridInfo?.[0].patchesX &&
                    item.GridInfo?.[0].patchesX > 1) ||
                  (item.GridInfo?.[0].patchesY &&
                    item.GridInfo?.[0].patchesY > 1)
                    ? [
                        {
                          title: 'Patches',
                          content: `${item.GridInfo?.[0].patchesX} x ${item.GridInfo?.[0].patchesY}`,
                        },
                      ]
                    : []),
                ]
              : []),
          ]}
        />
      </Col>
      <Col xs="12" md="4" className="bg-light dataviewer">
        {selectedPoint !== undefined && (
          <DataViewer selectedPoint={selectedPoint} dataCollection={item} />
        )}
        {selectedPoint === undefined && (
          <p>Select a point to view the related data</p>
        )}
      </Col>
      <Col xs="12" md="4">
        {item.GridInfo && item.GridInfo.length > 0 && (
          <GridPlot
            gridInfo={item.GridInfo[0]}
            dataCollectionId={item.dataCollectionId}
            setSelectedPoint={setSelectedPoint}
            snapshotAvailable={item._metadata.snapshots['1']}
          />
        )}
      </Col>
    </Row>
  );

  return <DataCollectionBox {...props}>{res}</DataCollectionBox>;
}
