import {
  Suspense,
  useState,
  useRef,
  useEffect,
  useCallback,
  useMemo,
} from 'react';
import { Stage, Layer, Rect, Image as KonvaImage } from 'react-konva';
import { useController, useSuspense } from '@data-client/react';
import { Button, Form } from 'react-bootstrap';
import { useInView } from 'react-intersection-observer';
import Konva from 'konva';

import { MapResource } from 'api/resources/Map';
import { getXHRBlob } from 'api/resources/XHRFile';
import { awaitImage } from 'components/Samples/Mapping/Images';
import { getROIName } from 'components/Samples/Mapping/Maps';
import { GridInfo } from 'models/Event';
import config from 'config/config';
import CreateScalarMapButton from 'components/Samples/Mapping/CreateScalarMap';
import { X } from 'react-bootstrap-icons';

function getMapSize(gridInfo: GridInfo) {
  const pixelsPerMicronX = gridInfo.pixelsPerMicronX || 1;
  const pixelsPerMicronY = gridInfo.pixelsPerMicronY || -1;

  const mapWidth =
    gridInfo.steps_x && gridInfo.dx_mm
      ? Math.abs(
          gridInfo.steps_x * (1 / pixelsPerMicronX) * gridInfo.dx_mm * 1e3
        )
      : 1;
  const mapHeight =
    gridInfo.steps_y && gridInfo.dy_mm
      ? Math.abs(
          gridInfo.steps_y * (1 / pixelsPerMicronY) * gridInfo.dy_mm * 1e3
        )
      : 1;

  return { mapWidth, mapHeight };
}

interface SnapshotImageProps {
  dataCollectionId: number;
  gridInfo: GridInfo;
  snapshotId: number;
  snapshotAvailable: boolean;
}

function SnapshotImage(props: SnapshotImageProps) {
  const { snapshotId, dataCollectionId, snapshotAvailable, gridInfo } = props;
  const { fetch } = useController();
  const [sampleSnapshot, setSampleSnapshot] = useState<HTMLImageElement>();

  const { mapWidth, mapHeight } = getMapSize(gridInfo);
  const snapshot_offsetXPixel = gridInfo.snapshot_offsetXPixel || 0;
  const snapshot_offsetYPixel = gridInfo.snapshot_offsetYPixel || 0;

  const left = Math.round(snapshot_offsetXPixel - mapWidth);
  const top = Math.round(snapshot_offsetYPixel - mapHeight);
  const width = Math.round(mapWidth * 3);
  const height = Math.round(mapHeight * 3);

  useEffect(() => {
    async function loadSnapshot() {
      const imageData = await fetch(getXHRBlob, {
        src: `${config.baseUrl}/datacollections/images/${dataCollectionId}?imageId=${snapshotId}&crop=${left},${top},${width},${height}`,
      });
      const image = await awaitImage(imageData);
      setSampleSnapshot(image);
    }
    if (snapshotAvailable) loadSnapshot();
  }, [dataCollectionId, fetch, snapshotAvailable, snapshotId]);

  return (
    <>
      {sampleSnapshot && <KonvaImage image={sampleSnapshot} x={left} y={top} />}
    </>
  );
}

interface GridPlotProps {
  gridInfo: GridInfo;
  dataCollectionId: number;
  dataCollectionGroupId?: number;
  setSelectedPoint?: (point: number) => void;
  setSelectedXY?: (selection: { x: number; y: number } | undefined) => void;
  setSelectedDataCollectionId?: (dataCollectionId: number) => void;
  updateSelectedMapROI?: (xrfFluorescenceMappingROIId: number) => void;
  snapshotAvailable: boolean;
  snapshotId?: number;
  scrollMaps?: boolean;
  selectedMapIndex?: number;
}

function GridPlotMain({
  gridInfo,
  dataCollectionId,
  dataCollectionGroupId,
  setSelectedPoint,
  setSelectedXY,
  setSelectedDataCollectionId,
  updateSelectedMapROI,
  snapshotAvailable,
  snapshotId = 1,
  scrollMaps,
  selectedMapIndex,
}: GridPlotProps) {
  const { fetch } = useController();

  const wrapRef = useRef<HTMLDivElement>(null);
  const stageRef = useRef<Konva.Stage>(null);
  const rangeRef = useRef<HTMLInputElement>(null);

  const [parentSize, setParentSize] = useState<number[]>([150, 150]);

  const maps = useSuspense(MapResource.getList, {
    ...(dataCollectionGroupId
      ? { dataCollectionGroupId }
      : { dataCollectionId }),
    limit: 9999,
  });

  const [selectedMap, setSelectedMap] = useState<number | undefined>(
    maps.results.length ? maps.results[0].xrfFluorescenceMappingId : undefined
  );
  const [currentMapIndex, setCurrentMapIndex] = useState<number>(1);
  const [mapImage, setMapImage] = useState<HTMLImageElement>();
  const [selectedPoint, setLocalSelectedPoint] = useState<
    Record<string, number | undefined>
  >({
    x: undefined,
    y: undefined,
  });

  function selectMap(mapIndex: number) {
    const newMap = filteredMaps[mapIndex];
    if (newMap) {
      if (setSelectedDataCollectionId) {
        if (newMap._metadata.dataCollectionId)
          setSelectedDataCollectionId(newMap._metadata.dataCollectionId);
      }
    }
    setSelectedMap(newMap.xrfFluorescenceMappingId);
    setCurrentMapIndex(mapIndex + 1);
    if (rangeRef.current) rangeRef.current.value = `${mapIndex}`;
  }

  useEffect(() => {
    if (selectedMapIndex !== undefined) {
      selectMap(selectedMapIndex);
    }
  }, [selectedMapIndex]);

  useEffect(() => {
    async function loadMap() {
      const map = maps.results.filter(
        (map) => map.xrfFluorescenceMappingId === selectedMap
      )[0];
      if (!map) return;

      const imageData = await fetch(getXHRBlob, {
        src: config.host + map._metadata.url,
      });
      const image = await awaitImage(imageData);
      setMapImage(image);
    }
    if (maps.results.length) loadMap();
  }, [selectedMap, fetch, maps.results]);

  useEffect(() => {
    if (wrapRef.current) {
      const parent = wrapRef.current.parentElement;
      if (parent) setParentSize([parent.clientWidth - 2, parent.clientHeight]);
    }
  }, [wrapRef]);

  const snapshot_offsetXPixel = gridInfo.snapshot_offsetXPixel || 0;
  const snapshot_offsetYPixel = gridInfo.snapshot_offsetYPixel || 0;
  const { mapWidth, mapHeight } = getMapSize(gridInfo);

  const mapROIs = useMemo(
    () =>
      maps &&
      maps.results
        .map((map) => getROIName(map))
        .filter((v, i, a) => a.indexOf(v) === i),
    [maps]
  );

  const [selectedMapROI, setSelectedMapROI] = useState<string | undefined>(
    mapROIs.length ? mapROIs[0] : undefined
  );

  useEffect(() => {
    if (selectedMapROI) {
      const selectedMap = maps.results.find(
        (map) => getROIName(map) == selectedMapROI
      );
      if (selectedMap) {
        updateSelectedMapROI?.(
          selectedMap.XRFFluorescenceMappingROI.xrfFluorescenceMappingROIId
        );
      }
    }
  }, [selectedMapROI]);

  const filteredMaps = useMemo(
    () =>
      maps && maps.results.filter((map) => getROIName(map) === selectedMapROI),
    [maps, selectedMapROI]
  );

  useEffect(() => {
    if (filteredMaps && filteredMaps.length) {
      setSelectedMap(filteredMaps[0].xrfFluorescenceMappingId);
      if (rangeRef.current) {
        rangeRef.current.value = '0';
      }
    }
  }, [selectedMapROI, filteredMaps]);

  const zoomStage = useCallback(
    (event: any) => {
      event.evt.preventDefault();
      const delta =
        event.evt.deltaMode === 1 ? event.evt.deltaY * 18 : event.evt.deltaY;
      if (stageRef.current !== null) {
        const stage = stageRef.current;
        const oldScale = stage.scaleX();
        // @ts-expect-error
        const { x: pointerX, y: pointerY } = stage.getPointerPosition();
        const mousePointTo = {
          x: (pointerX - stage.x()) / oldScale,
          y: (pointerY - stage.y()) / oldScale,
        };
        const newScale = (-1 * delta) / (400 / oldScale) + oldScale;
        stage.scale({ x: newScale, y: newScale });
        const newPos = {
          x: pointerX - mousePointTo.x * newScale,
          y: pointerY - mousePointTo.y * newScale,
        };
        stage.position(newPos);
        stage.batchDraw();
      }
    },
    [stageRef]
  );

  useEffect(() => {
    const stage = stageRef.current;
    if (stage) {
      const scaleX = parentSize[0] / mapWidth;
      const scaleY = parentSize[1] / mapHeight;
      const scale = scaleX < scaleY ? scaleX : scaleY;
      stage.scale({ x: scale, y: scale });
      stage.position({
        x:
          -snapshot_offsetXPixel * scale +
          (parentSize[0] - mapWidth * scale) / 2,
        y:
          -snapshot_offsetYPixel * scale +
          (parentSize[1] - mapHeight * scale) / 2,
      });
      stage.batchDraw();
    }
  }, [
    stageRef,
    snapshot_offsetXPixel,
    snapshot_offsetYPixel,
    mapHeight,
    mapWidth,
    parentSize,
  ]);

  const selectPoint = useCallback(
    (evt: any) => {
      const rel = evt.target.getRelativePointerPosition();
      const x = Math.floor(rel.x);
      const y = Math.floor(rel.y);

      setLocalSelectedPoint({ x, y });
      if (setSelectedXY) setSelectedXY({ x, y });

      // point is 1-offset
      const point = x + y * (gridInfo.steps_x ? gridInfo.steps_x : 1) + 1;
      // console.log('point', point);
      if (setSelectedPoint) setSelectedPoint(point);
    },
    [gridInfo.steps_x, setSelectedPoint]
  );

  const setPointer = useCallback(
    (pointerType: string = 'default') => {
      if (stageRef.current) {
        stageRef.current.content.style.cursor = pointerType;
      }
    },
    [stageRef]
  );

  return (
    <div
      className="gridplot position-relative border border-light ms-2"
      ref={wrapRef}
    >
      <div
        className="position-absolute text-nowrap"
        style={{
          right: 0,
          top: 0,
          zIndex: 100,
          display: 'flex',
        }}
      >
        {selectedPoint.x && (
          <Button
            onClick={() => {
              setLocalSelectedPoint({ x: undefined, y: undefined });
              if (setSelectedXY) setSelectedXY(undefined);
            }}
            size="sm"
          >
            <X />
            <span className="visually-hidden">Clear</span>
          </Button>
        )}
        {!scrollMaps && (
          <>
            <CreateScalarMapButton dataCollectionId={dataCollectionId} />
            {maps.results.length > 0 && (
              <Form.Control
                size="sm"
                as="select"
                style={{ display: 'block' }}
                onChange={(evt) => setSelectedMap(parseInt(evt.target.value))}
              >
                {maps.results.map((map) => (
                  <option
                    key={map.xrfFluorescenceMappingId}
                    value={map.xrfFluorescenceMappingId}
                  >
                    {getROIName(map)}
                  </option>
                ))}
              </Form.Control>
            )}
          </>
        )}
        {scrollMaps && (
          <>
            <Form.Control
              size="sm"
              as="select"
              onChange={(evt) => setSelectedMapROI(evt.target.value)}
            >
              {mapROIs.map((mapROI) => (
                <option key={mapROI} value={mapROI}>
                  {mapROI}
                </option>
              ))}
            </Form.Control>
            <Form.Range
              ref={rangeRef}
              min={0}
              max={filteredMaps && filteredMaps.length - 1}
              defaultValue={0}
              onChange={(evt) => {
                selectMap(parseInt(evt.target.value));
              }}
            />
            <div className="text-end text-gray-80">Map: {currentMapIndex}</div>
          </>
        )}
      </div>
      <Stage
        key="canvas"
        width={parentSize[0]}
        height={parentSize[1]}
        ref={stageRef}
        draggable
        onWheel={zoomStage}
      >
        <Layer imageSmoothingEnabled={false}>
          <SnapshotImage
            dataCollectionId={dataCollectionId}
            gridInfo={gridInfo}
            snapshotId={snapshotId}
            snapshotAvailable={snapshotAvailable}
          />
          {mapImage && gridInfo.steps_x && gridInfo.steps_y && (
            <KonvaImage
              opacity={0.7}
              x={snapshot_offsetXPixel}
              y={snapshot_offsetYPixel}
              scaleX={mapWidth / gridInfo.steps_x}
              scaleY={mapHeight / gridInfo.steps_y}
              image={mapImage}
              onClick={selectPoint}
              onTouchStart={selectPoint}
              onMouseEnter={() => setPointer('pointer')}
              onMouseLeave={() => setPointer()}
            />
          )}
          {selectedPoint &&
            selectedPoint.x !== undefined &&
            selectedPoint.y !== undefined &&
            gridInfo.steps_x &&
            gridInfo.steps_y && (
              <Rect
                opacity={0.7}
                x={
                  snapshot_offsetXPixel +
                  (mapWidth / gridInfo.steps_x) * selectedPoint.x
                }
                y={
                  snapshot_offsetYPixel +
                  +((mapHeight / gridInfo.steps_y) * selectedPoint.y)
                }
                width={mapWidth / gridInfo.steps_x}
                height={mapHeight / gridInfo.steps_y}
                fill="red"
              />
            )}
        </Layer>
      </Stage>
    </div>
  );
}

function Loading() {
  return <div className="ms-1 border border-light">Loading...</div>;
}

export default function GridPlot(props: GridPlotProps) {
  const { ref, inView } = useInView({ threshold: 0.1, triggerOnce: true });
  return (
    <>
      {inView && (
        <Suspense fallback={<Loading />}>
          <GridPlotMain {...props} />
        </Suspense>
      )}
      {!inView && <div ref={ref}></div>}
    </>
  );
}
