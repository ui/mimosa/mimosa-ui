import { Suspense, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import Metadata from 'components/Events/Metadata';
import { DataCollectionBox } from 'components/Events/DataCollection';
import { IDataCollection } from 'components/Events/DataCollections/Default';
import SubSampleType from 'components/Samples/SubSampleType';

import GridPlot from './GridPlot';
import DataViewer from './DataViewer';
import { toEnergy } from 'utils/numbers';
import StatusBadge from '../StatusBadge';
import MapSum from './MapSum';
import { useDataCollection } from 'hooks/useDataCollection';
import SampleLink from '../SampleLink';

function SuspendedDataViewer({
  dataCollectionId,
  selectedPoint,
}: {
  dataCollectionId: number;
  selectedPoint: number;
}) {
  const dataCollection = useDataCollection(dataCollectionId);
  return dataCollection ? (
    <DataViewer selectedPoint={selectedPoint} dataCollection={dataCollection} />
  ) : null;
}

export default function MapXAS(props: IDataCollection) {
  const { parent, item, embedded } = props;
  const [selectedPoint, setSelectedPoint] = useState<number | undefined>(
    undefined
  );
  const [selectedXY, setSelectedXY] = useState<{ x: number; y: number }>();
  const [selectedMapIndex, setSelectedMapIndex] = useState<number>();
  const [selectedDataCollectionId, setSelectedDataCollectionId] =
    useState<number>(item.dataCollectionId);
  const [selectedMapROIId, setSelectedMapROIId] = useState<number>();
  const res = (
    <Row className="g-0">
      {!embedded && (
        <Col md="4">
          <Metadata
            properties={[
              {
                title: 'Sample',
                test: parent.blSample,
                content: (
                  <>
                    <SampleLink
                      proposal={parent.proposal}
                      blSample={parent.blSample}
                      blSampleId={parent.blSampleId}
                    />
                    {item.BLSubSample?.type && (
                      <SubSampleType
                        className="ms-1"
                        type={item.BLSubSample.type}
                      />
                    )}
                  </>
                ),
              },
              {
                title: 'Group',
                content: (
                  <Link
                    to={`/proposals/${parent.proposal}/sessions/${parent.sessionId}?dataCollectionGroupId=${item.DataCollectionGroup.dataCollectionGroupId}`}
                  >
                    {parent.count} Data Collections
                  </Link>
                ),
              },
              {
                title: 'Type',
                content: item.DataCollectionGroup.experimentType,
              },
              {
                title: 'Status',
                content: <StatusBadge status={item.runStatus} />,
              },
              {
                title: 'Finished',
                content: parent.endTime,
                test: parent.endTime,
              },
              {
                title: 'Duration',
                content: parent.duration ? Math.round(parent.duration) : 0,
                unit: 'min',
              },
              {
                title: 'Energy',
                content: item.wavelength && toEnergy(item.wavelength),
                unit: 'keV',
              },
              { title: 'No. Points', content: item.numberOfImages },
              {
                title: 'Exposure Time',
                content: item.exposureTime,
                unit: 's',
              },
              ...(item.GridInfo?.length
                ? [
                    {
                      title: 'Step Size',
                      content: `${(item.GridInfo?.[0].dx_mm || 0) * 1e3} x ${
                        (item.GridInfo?.[0].dy_mm || 0) * 1e3
                      }`,
                      unit: 'µm',
                    },
                    {
                      title: 'Steps',
                      content: `${item.GridInfo?.[0].steps_x} x ${item.GridInfo?.[0].steps_y}`,
                    },
                  ]
                : []),
            ]}
          />
        </Col>
      )}
      <Col
        xs="12"
        md={embedded ? 4 : 3}
        className="bg-light"
        style={{ display: 'flex', minHeight: embedded ? 250 : undefined }}
      >
        {selectedPoint !== undefined && (
          <Suspense fallback={<p>Loading...</p>}>
            <SuspendedDataViewer
              selectedPoint={selectedPoint}
              dataCollectionId={selectedDataCollectionId}
            />
          </Suspense>
        )}
        {selectedPoint === undefined && (
          <p>Select a point to view the related data</p>
        )}
      </Col>
      <Col xs="12" md={embedded ? 4 : 3}>
        {item.GridInfo && item.GridInfo.length > 0 && (
          <GridPlot
            gridInfo={item.GridInfo[0]}
            dataCollectionGroupId={
              item.DataCollectionGroup.dataCollectionGroupId
            }
            dataCollectionId={item.dataCollectionId}
            setSelectedPoint={setSelectedPoint}
            setSelectedXY={setSelectedXY}
            setSelectedDataCollectionId={setSelectedDataCollectionId}
            updateSelectedMapROI={setSelectedMapROIId}
            snapshotAvailable={item._metadata.snapshots['1']}
            scrollMaps={true}
            selectedMapIndex={selectedMapIndex}
          />
        )}
      </Col>
      {selectedMapROIId && (
        <Col className="text-center bg-light" xs="12" md={embedded ? 4 : 2}>
          <MapSum
            dataCollectionGroupId={
              item.DataCollectionGroup.dataCollectionGroupId
            }
            xrfFluorescenceMappingROIId={selectedMapROIId}
            selectedXY={selectedXY}
            onPointClick={setSelectedMapIndex}
          />
        </Col>
      )}
    </Row>
  );

  return <DataCollectionBox {...props}>{res}</DataCollectionBox>;
}
