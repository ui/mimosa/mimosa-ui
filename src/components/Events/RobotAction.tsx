import { Container } from 'react-bootstrap';

import { EventHeader } from './Events';
import { RobotAction as RobotActionType, Event } from 'models/Event';
import Default from './SampleActions/Default';
import Mosaic from './SampleActions/Mosaic';
import Reference from './SampleActions/Reference';

function renderInner({
  item,
  parent,
}: {
  item: RobotActionType;
  parent: Event;
}) {
  const renderMap: Record<string, any> = {
    MOSAIC: Mosaic,
    REFERENCE: Reference,
  };

  const Component =
    item.actionType in renderMap ? renderMap[item.actionType] : Default;

  return <Component item={item} parent={parent} />;
}

export default function RobotAction(props: {
  item: RobotActionType;
  parent: Event;
}) {
  const { item, parent } = props;
  return (
    <>
      <EventHeader event={parent} title={item.actionType} />
      <Container fluid className="g-0">
        {renderInner({ item, parent })}
      </Container>
    </>
  );
}
