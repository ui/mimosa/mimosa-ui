import { useState } from 'react';
import { useSuspense } from '@data-client/react';
import { Form as BSForm } from 'react-bootstrap';
import { Gear } from 'react-bootstrap-icons';

import { AvailableTasksEndpoint } from 'api/resources/Reprocess/AvailableTasks';
import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { ReprocessEntity } from 'api/resources/Reprocess/Reprocess';
import { AvailableTask } from 'models/AvailableTasks';
import { useSchema, useUiSchemaWithFormData } from 'hooks/useSpec';
import Modal from 'components/Common/Modal';
import Form from 'components/Common/Form';

function TaskForm({
  task,
  formData,
}: {
  task: AvailableTask;
  formData: Record<string, any>;
}) {
  const schema = useSchema(task.param_schema, 'Parameters');
  const uiSchema = useUiSchemaWithFormData(schema['ui:schema'] || {}, formData);

  const ReprocessEndpoint = new AuthenticatedEndpoint({
    path: `/reprocess/${task.task}`,
    entity: ReprocessEntity,
    method: 'POST',
  });

  return (
    <Form
      schemaName={task.param_schema}
      resource={ReprocessEndpoint}
      initialFormData={formData}
      uiSchema={uiSchema}
      buttonTitle={
        <>
          <Gear className="me-1" /> Reprocess Data Collection
        </>
      }
    ></Form>
  );
}

function TaskFormWrap(props: { task: string; formData: Record<string, any> }) {
  const { task, ...rest } = props;
  const tasks = useSuspense(AvailableTasksEndpoint);
  const taskItem = tasks.tasks.find((t) => t.task === task);
  if (!taskItem) return <p>No such task</p>;
  return <TaskForm task={taskItem} {...rest} />;
}

export function Reprocess({
  dataCollectionId,
  experimentType,
}: {
  dataCollectionId: number;
  experimentType?: string;
}) {
  const tasks = useSuspense(AvailableTasksEndpoint);
  const [selectedTask, setSelectedTask] = useState<string>(tasks.tasks[0].task);
  return (
    <>
      <BSForm.Control
        as="select"
        defaultValue={selectedTask}
        onChange={(event) => setSelectedTask(event.target.value)}
      >
        {tasks.tasks.map((task) => (
          <option key={task.task} value={task.task}>
            {task.name}
          </option>
        ))}
      </BSForm.Control>
      <TaskFormWrap task={selectedTask} formData={{ dataCollectionId }} />
    </>
  );
}

export function ReprocessModal({
  dataCollectionId,
  experimentType,
  show,
  onHide,
}: {
  dataCollectionId: number;
  experimentType?: string;
  show?: boolean;
  onHide?: () => void;
}) {
  return (
    <Modal
      show={show || false}
      onHide={() => onHide && onHide()}
      title="Reprocess"
    >
      <Reprocess
        dataCollectionId={dataCollectionId}
        experimentType={experimentType}
      />
    </Modal>
  );
}
