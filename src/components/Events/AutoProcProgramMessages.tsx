import React from 'react';
import {
  ExclamationCircleFill,
  ExclamationTriangleFill,
  InfoCircleFill,
} from 'react-bootstrap-icons';

import {
  AutoProcProgram as AutoProcProgramType,
  AutoProcProgramMessage as AutoProcProgramMessageType,
} from 'models/AutoProcProgram';
import Table from 'components/Layout/Table';

export const messageStatusIcons: Record<string, JSX.Element> = {
  INFO: <InfoCircleFill color="#198754" />,
  WARNING: <ExclamationTriangleFill color="#ffc107" />,
  ERROR: <ExclamationCircleFill color="#dc3545" />,
};

export function AutoProcProgramMessageIcons(props: AutoProcProgramType) {
  const messageTypes = [
    // @ts-ignore
    ...new Set(
      props._metadata.autoProcProgramMessages?.map(
        (message) => message.severity
      )
    ),
  ];

  return (
    <>
      {messageTypes.map((messageType: string) => (
        <React.Fragment key={messageType}>
          {messageStatusIcons[messageType]}
        </React.Fragment>
      ))}
    </>
  );
}

function SeverityCell(row: AutoProcProgramMessageType) {
  return (
    <>
      {messageStatusIcons[row.severity]}{' '}
      <span className="d-sm-none">{row.severity}</span>
    </>
  );
}

export function AutoProcProgramMessages({
  messages,
}: {
  messages: AutoProcProgramMessageType[];
}) {
  return (
    <Table
      responsive
      titleColumn="severity"
      keyId="autoProcProgramMessageId"
      results={messages}
      columns={[
        { label: '', key: 'severity', formatter: SeverityCell },
        { label: 'Message', key: 'message' },
        { label: 'Description', key: 'description' },
      ]}
      emptyText="No messages yet"
    />
  );
}
