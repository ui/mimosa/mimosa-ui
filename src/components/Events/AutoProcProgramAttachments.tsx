import { useState } from 'react';
import { useSuspense } from '@data-client/react';
import { Download, FileBarGraph, FileEarmark } from 'react-bootstrap-icons';
import { Button, Popover, OverlayTrigger } from 'react-bootstrap';

import { AutoProcProgramAttachment } from 'models/AutoProcProgramAttachment';
import { AutoProcProgramAttachmentResource } from 'api/resources/Processing/AutoProcProgramAttachment';
import Table from 'components/Layout/Table';
import ButtonFileViewer from 'components/FileViewer';
import { useSign } from 'hooks/useSign';
import config from 'config/config';
import H5ViewerModal from './H5Viewer';
import FileNameButton from 'components/Common/FileNameButton';

function ActionsCell(row: AutoProcProgramAttachment) {
  const { signHandler } = useSign();
  const [showH5Web, setShowH5Web] = useState<boolean>(false);

  return (
    <>
      {['Log', 'Input', 'Debug'].includes(row.fileType) && (
        <ButtonFileViewer
          buttonClasses="me-1"
          url={row._metadata.url}
          title={`View Attachment: ${row.fileName}`}
        />
      )}
      {row.fileName.endsWith('h5') && (
        <>
          <H5ViewerModal
            onHide={() => setShowH5Web(false)}
            show={showH5Web}
            autoProcProgramAttachmentId={row.autoProcProgramAttachmentId}
            fileName={row.fileName}
          />
          <Button
            size="sm"
            onClick={() => setShowH5Web(true)}
            className="me-1"
            title="H5 Viewer"
          >
            <FileBarGraph />
          </Button>
        </>
      )}
      <FileNameButton filename={row.filePath + '/' + row.fileName} />
      <a
        href={config.host + row._metadata.url}
        className="btn btn-primary btn-sm"
        onClick={(e) => signHandler(e)}
      >
        <Download />
      </a>
    </>
  );
}

export function AutoProcProgramAttachments({
  autoProcProgramId,
}: {
  autoProcProgramId: number;
}) {
  const attachments = useSuspense(AutoProcProgramAttachmentResource.getList, {
    skip: 0,
    limit: 10,
    autoProcProgramId,
  });

  return (
    <Table
      responsive
      titleColumn="fileName"
      keyId="autoProcProgramAttachmentId"
      results={attachments.results}
      paginator={{
        total: attachments.total,
        skip: attachments.skip,
        limit: attachments.limit,
      }}
      columns={[
        { label: 'Name', key: 'fileName', className: 'text-break' },
        { label: 'Type', key: 'fileType', className: 'text-nowrap' },
        {
          label: '',
          key: 'actions',
          formatter: ActionsCell,
          className: 'text-end text-nowrap',
        },
      ]}
      emptyText="No attachments yet"
    />
  );
}
