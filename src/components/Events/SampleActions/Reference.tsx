import { Col, Row } from 'react-bootstrap';

import Metadata from '../Metadata';
import { RobotAction as RobotActionType, Event } from 'models/Event';
import { Snapshot } from './Snapshot';
import SampleLink from '../DataCollections/SampleLink';

export default function Default({
  item,
  parent,
}: {
  item: RobotActionType;
  parent: Event;
}) {
  return (
    <Row className="g-0">
      <Col xs="12" md="6">
        <Metadata
          properties={[
            {
              title: 'Sample',
              test: parent.blSample,
              content: (
                <SampleLink
                  proposal={parent.proposal}
                  blSample={parent.blSample}
                  blSampleId={parent.blSampleId}
                />
              ),
            },
            { title: 'Status', content: item.status },
            { title: 'Comment', content: item.message },
            { title: 'Output File', content: item.resultFilePath },
          ]}
        />
      </Col>
      <Col
        className="text-center bg-light"
        xs="12"
        md="6"
        style={{ maxHeight: 250, overflow: 'hidden' }}
      >
        <Snapshot item={item} after />
      </Col>
    </Row>
  );
}
