import { LazyImage } from 'api/resources/XHRFile';
import LightBox from 'components/LightBox';
import { RobotAction as RobotActionType } from 'models/Event';

export function Snapshot({
  item,
  after = false,
}: {
  item: RobotActionType;
  after?: boolean;
}) {
  const beforeOrAfter = after ? 'After' : 'Before';
  return (
    <>
      {item._metadata && item._metadata.snapshots[after ? 'after' : 'before'] && (
        <LightBox
          images={[
            `/datacollections/images/action/${item.robotActionId}${
              after ? '?after=1' : ''
            }`,
          ]}
        >
          <LazyImage
            className="img-fluid"
            src={`/datacollections/images/action/${item.robotActionId}${
              after ? '?after=1' : ''
            }`}
            alt={`Snapshot ${beforeOrAfter}`}
          />
        </LightBox>
      )}
    </>
  );
}
