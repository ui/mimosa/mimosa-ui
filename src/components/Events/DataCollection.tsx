import { PropsWithChildren, useState } from 'react';
import { Container } from 'react-bootstrap';
import {
  FileEarmark,
  Link45deg,
  Gear,
  FileBarGraph,
} from 'react-bootstrap-icons';
import { useNavigate } from 'react-router-dom';

import { DataCollection as DataCollectionType, Event } from 'models/Event';
import { ProcessingStatuses as ProcessingStatusesType } from 'models/ProcessingStatusesList';
import { AutoProcProgramMessageStatus as AutoProcProgramMessageStatusType } from 'models/AutoProcProgramMessageStatuses';
import { EventHeader, IButtonProps } from './Events';
import { DataCollectionFileAttachmentsModal } from './DataCollectionFileAttachments';
import { ReprocessModal } from './Reprocess';
import { ProcessingStatuses } from './Processing';
import MessageStatus from './MessageStatus';
import Workflow from './Workflow';

import Default from './DataCollections/Default';
import Mesh from './DataCollections/Mesh';
import Map from './DataCollections/Mapping/Map';
import MapXAS from './DataCollections/Mapping/MapXAS';
import EnergyScan from './DataCollections/Mapping/EnergyScan';
import { useUIOptions } from 'hooks/useUIOptions';
import H5ViewerModal from './H5Viewer';

function renderInner({
  item,
  parent,
  processingStatuses,
  messageStatuses,
  embedded,
}: {
  item: DataCollectionType;
  parent: Event;
  processingStatuses?: ProcessingStatusesType;
  messageStatuses?: AutoProcProgramMessageStatusType;
  embedded?: boolean;
}) {
  const renderMap: Record<string, any> = {
    Mesh: Mesh,
    'XRF map': Map,
    'XRD map': Map,
    'XRF xrd map': Map,
    'XRF map xas': MapXAS,
    'Energy scan': EnergyScan,
  };

  const Component =
    item.DataCollectionGroup.experimentType &&
    item.DataCollectionGroup.experimentType in renderMap
      ? renderMap[item.DataCollectionGroup.experimentType]
      : Default;

  return (
    <Component
      item={item}
      parent={parent}
      processingStatuses={processingStatuses}
      messageStatuses={messageStatuses}
      isGroup={parent.count > 1}
      embedded={embedded}
    />
  );
}

export function DataCollectionBox(
  props: PropsWithChildren<{
    item: DataCollectionType;
    parent: Event;
    processingStatuses?: ProcessingStatusesType;
    messageStatuses?: AutoProcProgramMessageStatusType;
    buttons?: Array<IButtonProps>;
    showProcessing?: boolean;
  }>
) {
  const { item, parent, children, buttons, showProcessing = true } = props;
  const navigate = useNavigate();
  const [showAttachments, setShowAttachments] = useState<boolean>(false);
  const [showH5Web, setShowH5Web] = useState<boolean>(false);

  const groupOrId = {
    ...(parent.count > 1
      ? {
          dataCollectionGroupId: item.DataCollectionGroup.dataCollectionGroupId,
        }
      : null),
    ...(parent.count === 1
      ? { dataCollectionId: item.dataCollectionId }
      : null),
  };

  return (
    <>
      <DataCollectionFileAttachmentsModal
        onHide={() => setShowAttachments(false)}
        show={showAttachments}
        {...groupOrId}
      />
      <H5ViewerModal
        onHide={() => setShowH5Web(false)}
        show={showH5Web}
        dataCollectionId={item.dataCollectionId}
        fileName={item.fileTemplate}
      />
      <EventHeader
        event={parent}
        title={[item.imageDirectory, item.fileTemplate]
          .filter((s) => s)
          .join('/')}
        buttons={
          buttons
            ? buttons
            : [
                {
                  icon: <Link45deg />,
                  hint: 'Permalink',
                  onClick: () =>
                    navigate(
                      parent.count > 1
                        ? '?dataCollectionGroupId=' +
                            item.DataCollectionGroup.dataCollectionGroupId
                        : '?dataCollectionId=' + item.dataCollectionId
                    ),
                },
                {
                  icon: <FileEarmark />,
                  content: <>{parent.attachments}</>,
                  hint: 'Attachments',
                  disabled: parent.attachments === 0,
                  onClick: () => setShowAttachments(true),
                },
                {
                  icon: <FileBarGraph />,
                  hint: 'H5 Viewer',
                  onClick: () => setShowH5Web(true),
                },
                {
                  icon: <MessageStatus statuses={props.messageStatuses} />,
                  variant: 'outline-light',
                  hint: 'Processing Status Messages',
                  hidden:
                    !props.messageStatuses ||
                    (props.messageStatuses?.errors === 0 &&
                      props.messageStatuses?.warnings === 0),
                },
              ]
        }
      />
      <Container fluid className="g-0">
        {children}
      </Container>
      {item.DataCollectionGroup.Workflow && parent.count > 1 && (
        <Workflow {...item.DataCollectionGroup.Workflow} />
      )}
      {showProcessing ? (
        <ProcessingStatuses
          statuses={props.processingStatuses}
          dataCollectionId={parent.id}
        />
      ) : null}
    </>
  );
}

export default function DataCollection(props: {
  item: DataCollectionType;
  parent: Event;
  processingStatuses?: ProcessingStatusesType;
  messageStatuses?: AutoProcProgramMessageStatusType;
  embedded?: boolean;
}) {
  const { item, parent, processingStatuses, messageStatuses, embedded } = props;
  return renderInner({
    item,
    parent,
    processingStatuses,
    messageStatuses,
    embedded,
  });
}
