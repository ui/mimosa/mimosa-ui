import { useSuspense } from '@data-client/react';
import { AutoProcProgram as AutoProcProgramType } from 'models/AutoProcProgram';
import { AutoProcProgramResource } from 'api/resources/Processing/AutoProcProgram';

import Table from 'components/Layout/Table';
import {
  AutoProcProgramMessageIcons,
  AutoProcProgramMessages,
} from '../AutoProcProgramMessages';
import {
  AttachmentsButton,
  ParametersButton,
  statusIcons,
  statusTexts,
  statusRunning,
  statusRunningText,
} from '../Processing';
import { ModalButton } from 'components/Common/Modal';

function StatusCell(row: AutoProcProgramType) {
  return (
    <>
      {row.processingStatus !== undefined &&
        (row.processingStatus === null ? (
          <>
            {statusRunning}{' '}
            <span className="d-sm-none">{statusRunningText}</span>
          </>
        ) : (
          <>
            {statusIcons[row.processingStatus]}{' '}
            <span className="d-sm-none">
              {statusTexts[row.processingStatus]}
            </span>
          </>
        ))}
    </>
  );
}

export function ActionsCell(row: AutoProcProgramType) {
  return (
    <>
      {row._metadata.autoProcProgramMessages &&
        row._metadata.autoProcProgramMessages.length > 0 && (
          <ModalButton
            title="Processing Messages"
            buttonTitle={<AutoProcProgramMessageIcons {...row} />}
            buttonClasses="me-1"
            buttonVariant="outline-primary"
          >
            {() => (
              <AutoProcProgramMessages
                // @ts-expect-error
                messages={row._metadata.autoProcProgramMessages}
              />
            )}
          </ModalButton>
        )}
      <ParametersButton {...row} />
      <AttachmentsButton {...row} />
    </>
  );
}

export default function ProcessingResult({
  dataCollectionId,
}: {
  dataCollectionId: number;
}) {
  const processings = useSuspense(AutoProcProgramResource.getList, {
    dataCollectionId,
  });

  return (
    <Table
      responsive
      titleColumn="processingPrograms"
      size="sm"
      keyId="autoProcProgramId"
      results={processings.results}
      columns={[
        { label: 'Program', key: 'processingPrograms' },
        { label: '', key: 'processingStatus', formatter: StatusCell },
        { label: 'Comments', key: 'ProcessingJob?.comments' },
        { label: 'Message', key: 'processingMessage' },
        { label: 'Start', key: 'processingStartTime' },
        { label: 'End', key: 'processingEndTime' },
        {
          label: '',
          key: 'actions',
          formatter: ActionsCell,
          className: 'text-end',
        },
      ]}
      emptyText="No processings yet"
    />
  );
}
