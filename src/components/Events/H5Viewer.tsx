import { H5GroveProvider, App } from '@h5web/app';
import config from 'config/config';
import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import '@h5web/app/dist/styles.css';
import { useMemo } from 'react';
import Modal from 'components/Common/Modal';
import { getMode } from 'utils/theme';

interface Props {
  dataCollectionId?: number;
  autoProcProgramAttachmentId?: number;
  fileName?: string;
  show?: boolean;
  onHide?: () => void;
}

export function H5Viewer(props: Props) {
  const { dataCollectionId, autoProcProgramAttachmentId, fileName } = props;

  const H5App = useMemo(
    () => (
      <H5GroveProvider
        url={`${config.baseUrl}/data/h5grove`}
        filepath={fileName ?? 'File.h5'}
        axiosConfig={{
          params: { dataCollectionId, autoProcProgramAttachmentId },
          headers: {
            Authorization: `Bearer ${AuthenticatedEndpoint.accessToken}`,
          },
        }}
      >
        <App disableDarkMode={!getMode()} />
      </H5GroveProvider>
    ),
    [dataCollectionId, autoProcProgramAttachmentId, fileName]
  );

  return <div style={{ height: '80vh' }}>{H5App}</div>;
}

export default function H5ViewerModal(props: Props) {
  const { show, onHide, ...rest } = props;
  return (
    <Modal
      show={show || false}
      size="xl"
      onHide={() => onHide && onHide()}
      title="H5 Viewer"
    >
      <H5Viewer {...rest} />
    </Modal>
  );
}
