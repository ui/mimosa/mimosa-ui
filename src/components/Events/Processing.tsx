import React, { Suspense, useState } from 'react';
import {
  FileEarmark,
  CheckCircle,
  Exclamation,
  Gear,
  XCircle,
} from 'react-bootstrap-icons';
import { ProcessingStatuses as ProcessingStatusesType } from 'models/ProcessingStatusesList';
import { ProcessingStatus as ProcessingStatusType } from 'models/ProcessingStatus';
import { AutoProcProgram as AutoProcProgramType } from 'models/AutoProcProgram';

import { AutoProcProgramAttachments } from './AutoProcProgramAttachments';
import Loading from 'components/Loading';
import ProcessingResult from './Processing/ProcessingResult';
import { ModalButton } from 'components/Common/Modal';
import NestedObjectTable from 'components/Layout/NestedObjectTable';

export const statusIcons: Record<StatusEnum, JSX.Element> = {
  0: <XCircle color="red" className="ms-1" />,
  1: <CheckCircle color="green" className="ms-1" />,
  2: <Exclamation color="blue" className="ms-1" />,
};
export const statusRunning = <Gear className="ms-1" color="gray" />;

export const statusTexts: Record<StatusEnum, string> = {
  0: 'Failed',
  1: 'Completed',
  2: 'Did Not Start',
};

export const statusRunningText = 'Running';

export function ParametersButton(row: AutoProcProgramType) {
  return (
    <ModalButton
      disabled={row.ProcessingJob?.ProcessingJobParameters?.length === 0}
      title="Parameters"
      buttonClasses="me-1"
      buttonTitle={
        <>
          <Gear />
        </>
      }
    >
      {() => (
        <NestedObjectTable
          object={Object.fromEntries(
            // @ts-expect-error
            row.ProcessingJob?.ProcessingJobParameters?.map((row) => [
              row.parameterKey,
              row.parameterValue,
            ])
          )}
        />
      )}
    </ModalButton>
  );
}

export function AttachmentsButton(row: AutoProcProgramType) {
  return (
    <ModalButton
      disabled={row._metadata.attachments === 0}
      title="Attachments"
      buttonTitle={
        <>
          <FileEarmark /> {row._metadata.attachments}
        </>
      }
    >
      {() => (
        <AutoProcProgramAttachments autoProcProgramId={row.autoProcProgramId} />
      )}
    </ModalButton>
  );
}

function ProcessingResults(props: {
  dataCollectionId: number;
  processingType: string;
}) {
  return (
    <div className="processing-results border-top p-2">
      <Suspense fallback={<Loading />}>
        {props.processingType === 'processing' && (
          <ProcessingResult dataCollectionId={props.dataCollectionId} />
        )}
      </Suspense>
    </div>
  );
}

function groupStatuses(statuses: ProcessingStatusType[]) {
  const statusCounts: Record<string | number, number> = {};
  statuses.forEach((status) => {
    const statusType = status.status === null ? 'running' : status.status;
    if (statusType !== undefined) {
      if (!(statusType in statusCounts)) statusCounts[statusType] = 0;
      statusCounts[statusType]++;
    }
  });
  return Object.entries(statusCounts).map(
    ([status, count]: [string | number, number]) => (
      <React.Fragment key={status}>
        {count > 1 && <>{count}x</>}
        {status === 'running'
          ? statusRunning
          : statusIcons[status as StatusEnum]}
      </React.Fragment>
    )
  );
}

export type StatusEnum = 0 | 1 | 2;

export function ProcessingStatuses({
  statuses,
  dataCollectionId,
}: {
  statuses?: ProcessingStatusesType;
  dataCollectionId: number;
}) {
  const [showResults, setShowResults] = useState<Record<string, boolean>>({});
  const hasProcessing =
    statuses &&
    Object.entries(statuses)
      .map(([_, value]) => (value !== null ? 1 : 0))
      .reduce((a: number, b: number) => a + b, 0);

  const titles: Record<string, string> = {
    processing: 'Auto Processing',
  };

  return (
    <div className="processing mt-1 border rounded bg-light">
      {statuses &&
        Object.entries(statuses).map(
          ([statusType, statusTypeValues]: [
            string,
            Record<string, ProcessingStatusType[]>
          ]) => (
            <React.Fragment key={statusType}>
              {statusTypeValues !== null && (
                <>
                  <div
                    className="d-flex justify-content-between p-2"
                    style={{ cursor: 'pointer' }}
                    onClick={() =>
                      setShowResults((prevState) => ({
                        ...prevState,
                        [statusType]: !showResults[statusType],
                      }))
                    }
                  >
                    <div>{titles[statusType]}</div>
                    <div>
                      {Object.entries(statusTypeValues).map(
                        ([processName, processStatuses]: [
                          string,
                          ProcessingStatusType[]
                        ]) => (
                          <span key={processName} className="ms-2">
                            {processName}:<>{groupStatuses(processStatuses)}</>
                          </span>
                        )
                      )}
                    </div>
                  </div>
                  {showResults[statusType] && (
                    <ProcessingResults
                      dataCollectionId={dataCollectionId}
                      processingType={statusType}
                    />
                  )}
                </>
              )}
            </React.Fragment>
          )
        )}

      {!hasProcessing && <div className="p-2">No Processings</div>}
    </div>
  );
}
