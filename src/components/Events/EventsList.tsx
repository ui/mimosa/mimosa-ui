import { JSXElementConstructor, Suspense } from 'react';
import { useSuspense, useSubscription } from '@data-client/react';
import { useSearchParams, Link } from 'react-router-dom';
import { Alert } from 'react-bootstrap';
import {
  Archive,
  PieChartFill,
  PeopleFill,
  Rewind,
} from 'react-bootstrap-icons';

import { EventResource } from 'api/resources/Event';
import { EventTypeResource } from 'api/resources/EventType';
import { ProcessingStatusEndpoint } from 'api/resources/Processing/ProcessingStatus';
import { ProcessingMessageStatusEndpoint } from 'api/resources/Processing/ProcessingMessageStatus';
import { usePath } from 'hooks/usePath';
import { usePaging } from 'hooks/usePaging';
import NetworkErrorPage from 'components/NetworkErrorPage';
import Paginator from 'components/Layout/Paginator';
import Filter from 'components/Filter';
import TimesBar from 'components/Stats/TimesBar';
import { Event } from 'models/Event';
import { Statuses as ProcessingStatusesType } from 'models/ProcessingStatusesList';
import { Statuses as ProcessingMessageStatusesType } from 'models/AutoProcProgramMessageStatuses';
import { EventBase } from './Events';
import Default from './Default';
import RobotAction from './RobotAction';
import DataCollection from './DataCollection';
import { useSessionInfo } from 'hooks/useSessionInfo';
import SessionOverview from 'components/Stats/SessionOverview';
import SessionPeople from 'components/Proposals/SessionPeople';
import AssignContainer from 'components/Samples/AssignContainer';
import AddSampleButton from 'components/Samples/AddSampleButton';
import { ModalButton } from 'components/Common/Modal';

function EventTypeFilter({
  urlKey,
  sessionId,
  blSampleId,
  proteinId,
  beamLineName,
}: {
  urlKey: string;
  sessionId?: string;
  blSampleId?: string;
  proteinId?: string;
  beamLineName?: string;
}) {
  const eventTypes = useSuspense(EventTypeResource.getList, {
    ...(sessionId ? { sessionId } : null),
    ...(blSampleId ? { blSampleId } : null),
    ...(proteinId ? { proteinId } : null),
    ...(beamLineName ? { beamLineName } : null),
  });

  const filterTypes = eventTypes.results.map((eventType) => ({
    filterKey: eventType.eventTypeName,
    filterValue: eventType.eventType,
  }));

  return (
    <Suspense>
      <Filter urlKey={urlKey} filters={filterTypes} />
    </Suspense>
  );
}

function EventStatusFilter({ urlKey }: { urlKey: string }) {
  const filterTypes = [
    {
      filterKey: 'Success',
      filterValue: 'success',
    },
    {
      filterKey: 'Failed',
      filterValue: 'failed',
    },
    {
      filterKey: 'Processed',
      filterValue: 'processed',
    },
    {
      filterKey: 'Processing Error',
      filterValue: 'processerror',
    },
  ];

  return <Filter urlKey={urlKey} filters={filterTypes} />;
}

function EventListButtons() {
  const proposal = usePath('proposal');
  const sessionId = usePath('sessionId');
  const sessionInfo = useSessionInfo(sessionId);
  return (
    <div className="eventlist-buttons">
      {sessionId && <AddSampleButton sessionId={sessionId} />}
      {sessionId && (
        <ModalButton
          title="Registered Users"
          buttonTitle={
            <>
              <PeopleFill className="me-1" />
              Users
            </>
          }
          buttonClasses="me-1"
        >
          {() => <SessionPeople sessionId={sessionId} />}
        </ModalButton>
      )}
      {sessionInfo && (
        <ModalButton
          title="Assign Container"
          buttonTitle={
            <>
              <Archive className="me-1" />
              Assign
            </>
          }
          buttonClasses="me-1"
        >
          {({ hideModal }) => (
            <AssignContainer
              beamLineName={sessionInfo.beamLineName}
              onHide={() => hideModal()}
            />
          )}
        </ModalButton>
      )}
      <Link
        className="btn btn-primary btn-sm me-1"
        to={`/proposals/${proposal}/stats/${sessionId}`}
      >
        <PieChartFill className="me-1" />
        Stats
      </Link>
    </div>
  );
}

function renderTemplate(
  event: Event,
  processingStatuses: ProcessingStatusesType,
  processngMessageStatuses: ProcessingMessageStatusesType,
  embedded?: boolean
) {
  const templates: Record<string, JSXElementConstructor<any>> = {
    dc: DataCollection,
    robot: RobotAction,
  };

  if (event.type in templates) {
    const Template = templates[event.type];
    const extraProps: Record<string, any> = {};
    if (event.type === 'dc') {
      extraProps.processingStatuses = processingStatuses[event.id];
      extraProps.messageStatuses = processngMessageStatuses[event.id];
    }
    return (
      <Template
        item={event.Item}
        parent={event}
        {...extraProps}
        embedded={embedded}
      />
    );
  }

  return <Default {...event} />;
}

interface IEventsList {
  blSampleId?: string;
  proteinId?: string;
  beamLineName?: string;
  refresh?: boolean;
  limit?: number;
  dataCollectionId?: number;
  dataCollectionGroupId?: number;
  single?: boolean;
  embedded?: boolean;
}

function EventListMain({
  blSampleId,
  proteinId,
  beamLineName,
  refresh,
  limit: initialLimit,
  dataCollectionId: initDataCollectionId,
  dataCollectionGroupId: initDataCollectionGroupId,
  single,
  embedded,
}: IEventsList) {
  const [searchParams] = useSearchParams();
  const sessionId = usePath('sessionId');
  const proposal = usePath('proposal');
  const { skip, limit } = usePaging(initialLimit);
  const dataCollectionId =
    initDataCollectionId || searchParams.get('dataCollectionId');
  const dataCollectionGroupId =
    initDataCollectionGroupId || searchParams.get('dataCollectionGroupId');
  const eventType = searchParams.get('eventType');
  const status = searchParams.get('status');

  const opts = {
    skip,
    limit,
    ...(dataCollectionId ? { dataCollectionId } : {}),
    ...(dataCollectionGroupId ? { dataCollectionGroupId } : {}),
    ...(blSampleId ? { blSampleId } : {}),
    ...(proteinId ? { proteinId } : {}),
    ...(sessionId ? { sessionId } : {}),
    ...(beamLineName ? { beamLineName } : {}),
    ...(eventType ? { eventType } : {}),
    ...(status ? { status } : {}),
    ...(initDataCollectionGroupId ? { group: true } : {}),
  };

  const events = useSuspense(EventResource.getList, opts);
  useSubscription(EventResource.getList, refresh ? opts : null);

  const dataCollectionIds = events.results
    .filter((event) => event.type === 'dc')
    .map((event) => event.id);

  const processingStatuses = useSuspense(
    ProcessingStatusEndpoint,
    dataCollectionIds.length > 0
      ? { dataCollectionIds: JSON.stringify(dataCollectionIds) }
      : null
  );
  useSubscription(
    ProcessingStatusEndpoint,
    refresh && dataCollectionIds.length > 0 && refresh
      ? { dataCollectionIds: JSON.stringify(dataCollectionIds) }
      : null
  );

  const messageStatuses = useSuspense(
    ProcessingMessageStatusEndpoint,
    dataCollectionIds.length > 0
      ? { dataCollectionIds: JSON.stringify(dataCollectionIds) }
      : null
  );

  useSubscription(
    ProcessingMessageStatusEndpoint,
    dataCollectionIds.length > 0 && refresh
      ? { dataCollectionIds: JSON.stringify(dataCollectionIds) }
      : null
  );

  const title = dataCollectionGroupId
    ? `Data Collections for group ${dataCollectionGroupId}`
    : '';

  return (
    <section>
      {!initDataCollectionId && !initDataCollectionGroupId && (
        <h1 className="session-title">
          <div className="d-flex justify-content-between">
            <span>
              {title
                ? `: ${title}`
                : sessionId
                ? 'Session'
                : 'Data Collections'}
            </span>
            {sessionId && <TimesBar />}
          </div>
          <div>{sessionId && <SessionOverview />}</div>
        </h1>
      )}

      {sessionId && <EventListButtons />}
      {!initDataCollectionId && !initDataCollectionGroupId && (
        <div className="d-flex filter-container">
          <EventStatusFilter urlKey="status" />
          <EventTypeFilter
            urlKey="eventType"
            sessionId={sessionId}
            blSampleId={blSampleId}
            proteinId={proteinId}
            beamLineName={beamLineName}
          />
        </div>
      )}
      {proposal && sessionId && (dataCollectionId || dataCollectionGroupId) && (
        <div className="d-grid">
          <Link
            to={`/proposals/${proposal}/sessions/${sessionId}`}
            className="btn btn-secondary"
          >
            <Rewind /> Back to Session
          </Link>
        </div>
      )}
      {!single && (
        <Paginator
          total={events.total}
          skip={events.skip}
          limit={events.limit}
        />
      )}
      {events.results.map((event) => (
        <EventBase key={event.pk()}>
          {renderTemplate(
            event,
            processingStatuses ? processingStatuses.statuses : {},
            messageStatuses ? messageStatuses.statuses : {},
            embedded
          )}
        </EventBase>
      ))}
      {!events.results.length && <Alert>No events yet</Alert>}
      {!single && (
        <Paginator
          total={events.total}
          skip={events.skip}
          limit={events.limit}
        />
      )}
    </section>
  );
}

export default function EventList(props: IEventsList) {
  return (
    <NetworkErrorPage>
      <EventListMain {...props} />
    </NetworkErrorPage>
  );
}
