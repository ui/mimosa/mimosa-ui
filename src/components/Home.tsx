import { useSuspense } from '@data-client/react';
import { Link, useNavigate, createSearchParams } from 'react-router-dom';
import { Form, Alert, Card, CardGroup } from 'react-bootstrap';
import classNames from 'classnames';

import { SessionResource, SessionGroupResource } from 'api/resources/Session';
import { useCurrentUser } from 'hooks/useCurrentUser';
import NetworkErrorPage from 'components/NetworkErrorPage';
import { useUIOptions } from 'hooks/useUIOptions';
import { useSearchParamsObj } from 'hooks/useSearchParamsObj';

interface ISessionRow {
  upcoming?: boolean;
  previous?: boolean;
  commissioning?: boolean;
  beamLineGroup?: string;
  linkSampleReview?: boolean;
}

function SessionRow(props: ISessionRow) {
  const { upcoming, previous, commissioning, beamLineGroup, linkSampleReview } =
    props;

  const resource = beamLineGroup ? SessionGroupResource : SessionResource;
  const sessions = useSuspense(resource.getList, {
    limit: 5,
    ...(upcoming ? { upcoming: true } : {}),
    ...(previous ? { previous: true } : {}),
    ...(commissioning ? { sessionType: 'commissioning' } : {}),
    ...(beamLineGroup ? { beamLineGroup } : {}),
  });

  return (
    <CardGroup>
      {sessions.results.map((session) => (
        <Card
          key={session.session}
          className={classNames({ 'session-active': session._metadata.active })}
        >
          <Card.Header>
            <span>{session.beamLineName}</span>.{' '}
            <span>{session.beamLineOperator}</span>
          </Card.Header>

          <Card.Body>
            <Card.Title>
              <Card.Link
                as={Link}
                to={
                  linkSampleReview
                    ? `/proposals/${session.proposal}/samples/review/?sessionId=${session.sessionId}`
                    : `/proposals/${session.proposal}/sessions/${session.sessionId}`
                }
              >
                {session.session}
              </Card.Link>
            </Card.Title>

            <Card.Text>
              <span>Start: {session.startDate}</span>
              <br />
              <span>End: {session.endDate}</span>
            </Card.Text>
            <Card.Text>
              <span>{session._metadata.sessionTypes.join(',')}</span>
            </Card.Text>
          </Card.Body>
        </Card>
      ))}
      {!sessions.results.length && <li>No sessions found</li>}
    </CardGroup>
  );
}

function UserHome() {
  return (
    <>
      <h1>Current &amp; Upcoming Sessions</h1>
      <SessionRow upcoming linkSampleReview />
      <h1>Previous Sessions</h1>
      <SessionRow previous linkSampleReview />
    </>
  );
}

function BeamLineGroupHome({ beamLineGroups }: { beamLineGroups: string[] }) {
  const navigate = useNavigate();
  const { beamLineGroup: groupParam } = useSearchParamsObj();
  const beamLineGroup =
    groupParam !== undefined
      ? groupParam
      : beamLineGroups.length > 0
      ? beamLineGroups[0]
      : undefined;

  function selectGroup(value: string) {
    navigate({
      pathname: '',
      search: createSearchParams({
        beamLineGroup: value,
      }).toString(),
    });
  }

  return (
    <>
      {beamLineGroups.length > 1 && (
        <Form.Control as="select" onChange={(e) => selectGroup(e.target.value)}>
          {beamLineGroups.map((group) => (
            <option key={group} value={group}>
              {group}
            </option>
          ))}
        </Form.Control>
      )}
      <h1>Upcoming Sessions</h1>
      <SessionRow upcoming beamLineGroup={beamLineGroup} linkSampleReview />
      <h1>Previous Sessions</h1>
      <SessionRow previous beamLineGroup={beamLineGroup} linkSampleReview />
      <h1>Commissioning Sessions</h1>
      <SessionRow
        commissioning
        beamLineGroup={beamLineGroup}
        linkSampleReview
      />
    </>
  );
}

function HomeMain() {
  const currentUser = useCurrentUser();
  const uiOptions = useUIOptions();

  return (
    <>
      {uiOptions.motd && <Alert variant="success">{uiOptions.motd}</Alert>}
      <section>
        {currentUser.beamLineGroups.length > 0 && (
          <BeamLineGroupHome beamLineGroups={currentUser.beamLineGroups} />
        )}
        {currentUser.beamLineGroups.length === 0 && <UserHome />}
      </section>
    </>
  );
}

export default function Home() {
  return (
    <NetworkErrorPage>
      <HomeMain />
    </NetworkErrorPage>
  );
}
