import { createSearchParams, Link, useNavigate } from 'react-router-dom';
import { DateTime, Info } from 'luxon';
import classNames from 'classnames';

import { useSuspense } from '@data-client/react';
import { SessionResource } from 'api/resources/Session';
import { Button, ButtonGroup, Col, Row, Form } from 'react-bootstrap';
import { Session } from 'models/Session';
import { usePath } from 'hooks/usePath';
import { range } from 'lodash';
import { useCurrentUser } from 'hooks/useCurrentUser';
import { useSearchParamsObj } from 'hooks/useSearchParamsObj';

function CalendarNav({ year, month }: { year: number; month: number }) {
  const { year: yearParam, month: monthParam, ...rest } = useSearchParamsObj();
  const navigate = useNavigate();
  const today = DateTime.now();
  const currentMonth = DateTime.fromObject({ year, month });

  const prevYear = currentMonth.minus({ year: 1 });
  const nextYear = currentMonth.plus({ year: 1 });

  const prevMonth = currentMonth.minus({ month: 1 });
  const nextMonth = currentMonth.plus({ month: 1 });

  function goto(date: DateTime) {
    navigate({
      search: createSearchParams({
        year: date.year.toString(),
        month: date.month.toString(),
        ...rest,
      }).toString(),
    });
  }

  return (
    <Row>
      <Col md={4}>
        <ButtonGroup className="calendar-nav mb-1">
          <Button variant="secondary" onClick={() => goto(today)}>
            Go to today
          </Button>
        </ButtonGroup>
      </Col>
      <Col>
        <ButtonGroup className="calendar-nav mb-1">
          <Button variant="outline-primary" onClick={() => goto(prevYear)}>
            {'<<'} {prevYear.year}
          </Button>
          <Button variant="outline-primary" onClick={() => goto(prevMonth)}>
            {'<'} {prevMonth.monthLong}
          </Button>
          <Button variant="outline-primary" onClick={() => goto(nextMonth)}>
            {nextMonth.monthLong} {'>'}
          </Button>
          <Button variant="outline-primary" onClick={() => goto(nextYear)}>
            {nextYear.year} {'>>'}
          </Button>
        </ButtonGroup>
      </Col>
    </Row>
  );
}

function CalendarHeader() {
  const days = Array(7).fill(0);
  return (
    <div className="calendar-header">
      {days.map((_, day) => (
        <div key={`head-${day}`} className="calendar-header-day">
          {Info.weekdays()[day]}
        </div>
      ))}
    </div>
  );
}

function SessionList({
  sessions,
  day,
  linkSampleReview,
}: {
  sessions: Session[];
  day: DateTime;
  linkSampleReview?: boolean;
}) {
  const relevantSessions = sessions.filter((session) => {
    const dayEnd = day.endOf('day');
    const startTime = DateTime.fromISO(session.startDate);
    return startTime >= day && startTime <= dayEnd;
  });
  return (
    <ul>
      {relevantSessions.map((session) => {
        const sessionStartDate = DateTime.fromISO(session.startDate);
        const sessionEndDate = DateTime.fromISO(session.endDate);
        const duration = sessionEndDate.diff(sessionStartDate, 'hours');

        return (
          <li key={session.sessionId}>
            {sessionStartDate.toFormat('HH:mm')}
            <br />
            {session.beamLineName}:
            <Link
              to={
                linkSampleReview
                  ? `/proposals/${session.proposal}/samples/review/?sessionId=${session.sessionId}`
                  : `/proposals/${session.proposal}/sessions/${session.sessionId}`
              }
            >
              {session.session}
            </Link>{' '}
            ({Math.round(duration.hours)}h)
            <br />
            {session.beamLineOperator}
          </li>
        );
      })}
    </ul>
  );
}

function CalendarDays({
  year,
  month,
  beamLineGroup,
}: {
  year: number;
  month: number;
  beamLineGroup?: string;
}) {
  const proposal = usePath('proposal');
  const sessions = useSuspense(SessionResource.getList, {
    year,
    month,
    limit: 9999,
    ...(proposal ? { proposal } : {}),
    ...(beamLineGroup ? { beamLineGroup } : {}),
  });

  const now = DateTime.now();
  const monthObj = DateTime.fromObject({ year: year, month: month });
  if (!monthObj.daysInMonth) return null;
  const startDay = monthObj.startOf('month');

  const days = range(1, monthObj.daysInMonth + 1);
  const preDays = startDay.weekday > 0 ? range(1, startDay.weekday) : [];

  return (
    <div className="calendar-days">
      {preDays.map((day) => (
        <div className="calendar-pre-day" key={`pre-${day}`}></div>
      ))}
      {days.map((day) => (
        <div
          key={`day-${day}`}
          className={classNames('calendar-day', {
            'calendar-day-today':
              day === now.day && month === now.month && year === now.year,
          })}
        >
          <h2>{day}</h2>
          <SessionList
            sessions={sessions.results}
            day={DateTime.fromObject({ year, month, day: day })}
            linkSampleReview
          />
        </div>
      ))}
    </div>
  );
}

export default function Calendar() {
  const navigate = useNavigate();
  const proposal = usePath('proposal');
  const currentUser = useCurrentUser();
  const now = DateTime.now();
  const {
    year: yearParam,
    month: monthParam,
    beamLineGroup: groupParam,
  } = useSearchParamsObj();
  const year = yearParam !== undefined ? parseInt(yearParam) : now.year;
  const month = monthParam !== undefined ? parseInt(monthParam) : now.month;
  const beamLineGroup =
    groupParam !== undefined
      ? groupParam
      : currentUser.beamLineGroups.length > 0
      ? currentUser.beamLineGroups[0]
      : undefined;

  function selectGroup(value: string) {
    navigate({
      pathname: '',
      search: createSearchParams({
        ...(yearParam ? { year: yearParam } : {}),
        ...(monthParam ? { month: monthParam } : {}),
        beamLineGroup: value,
      }).toString(),
    });
  }

  return (
    <section>
      <h1>
        {Info.months()[month - 1]} {year}
      </h1>
      {currentUser.beamLineGroups.length > 1 && !proposal && (
        <Form.Control
          as="select"
          className="mb-2"
          onChange={(e) => selectGroup(e.target.value)}
        >
          {currentUser.beamLineGroups.map((group) => (
            <option key={group} value={group}>
              {group}
            </option>
          ))}
        </Form.Control>
      )}
      <div className="calendar">
        <CalendarNav month={month} year={year} />
        <CalendarHeader />
        <CalendarDays month={month} year={year} beamLineGroup={beamLineGroup} />
      </div>
    </section>
  );
}
