import { useSuspense, useController } from '@data-client/react';
import { useParams } from 'react-router-dom';
import { set } from 'lodash';

import NetworkErrorPage from 'components/NetworkErrorPage';
import { useSchema } from 'hooks/useSpec';
import { ProteinResource } from 'api/resources/Protein';

import EventList from 'components/Events/EventsList';
import InlineEditable from 'components/RJSF/InlineEditable';
import SamplesList from 'components/Samples/SamplesList';
import useComponentsTitle from 'hooks/useComponentsTitle';
import { hiddenField, textArea } from 'components/RJSF/uiSchemaHelpers';

function ViewProteinMain() {
  const { proteinId } = useParams();
  const controller = useController();
  const componentsTitle = useComponentsTitle();
  const protein = useSuspense(
    ProteinResource.get,
    proteinId
      ? {
          proteinId,
        }
      : null
  );

  const schema = useSchema('Protein', `View ${componentsTitle.slice(0, -1)}`);
  const uiSchema = {
    proposalId: hiddenField,
    proteinId: hiddenField,
    containmentLevel: hiddenField,
    hazardGroup: hiddenField,
    sequence: textArea,
    _metadata: hiddenField,
    ComponentType: hiddenField,
  };

  if (!proteinId) return <div>No protein id provided</div>;

  function onChange({ field, value }: { field: string; value: any }) {
    const obj = {};
    set(obj, field, value);
    // @ts-expect-error
    return controller.fetch(ProteinResource.partialUpdate, { proteinId }, obj);
  }

  const editable = [
    'root_name',
    'root_acronym',
    'root_sequence',
    'root_density',
    'root_molecularMass',
  ];

  return (
    <div>
      <section>
        <InlineEditable
          editable={editable}
          schema={schema}
          uiSchema={uiSchema}
          formData={protein}
          onChange={onChange}
        />
      </section>
      <section>
        <SamplesList proteinId={proteinId} />
      </section>
      <section>
        <EventList proteinId={proteinId} limit={5} />
      </section>
    </div>
  );
}

export default function ViewProtein() {
  return (
    <NetworkErrorPage>
      <ViewProteinMain />
    </NetworkErrorPage>
  );
}
