import { LazyImage } from 'api/resources/XHRFile';
import EnumBadge from 'components/Common/EnumBadge';
import LightBox from 'components/LightBox';
import { get } from 'lodash';
import { Badge } from 'react-bootstrap';

import { round } from 'utils/numbers';
import { IResults, IColumn } from './Table';

export function toPrecision(precision: number) {
  return function (row: IResults, column: IColumn) {
    const value = get(row, column.key);
    return round(value, precision);
  };
}

export function enumBadge(row: IResults, column: IColumn) {
  return (
    <EnumBadge
      value={get(row, column.key)}
      colorEnum={column.formatterParams?.enum}
    />
  );
}

export function tagCell(row: IResults, column: IColumn) {
  const metadata = get(row, column.key);
  return (
    <>
      {metadata &&
        metadata.tags &&
        metadata.tags.map((tag: string) => (
          <Badge key={tag} className="me-1">
            {tag}
          </Badge>
        ))}
    </>
  );
}

export function imageCell(row: IResults, column: IColumn) {
  let url = get(row, column.key);
  const alt = column.formatterParams?.alt(row);
  if (column.formatterParams?.maxSize) {
    url = url + `?maxSize=${column.formatterParams.maxSize}`;
  }
  return (
    <LightBox images={[get(row, column.key)]} fullUrl style={{ maxWidth: 250 }}>
      <LazyImage className="img-fluid" src={url} alt={alt} fullUrl />
    </LightBox>
  );
}
