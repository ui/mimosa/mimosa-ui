import Table, { IColumn } from './Table';

export default function NestedObjectTable({
  object,
  top = true,
}: {
  object: any;
  top?: boolean;
}) {
  if (typeof object == 'boolean')
    return <span>{object ? 'True' : 'False'}</span>;
  if (object === undefined || object === null)
    return <span>No Parameters</span>;
  if (typeof object === 'object') {
    const columns = [
      { key: 'key', label: 'Key' },
      {
        key: 'value',
        label: 'Value',
        formatter: (row: any, column: IColumn) => (
          <NestedObjectTable top={false} object={row[column.key]} />
        ),
      },
    ];

    const args = Object.entries(object).map(([key, value]: [string, any]) => ({
      key,
      value,
    }));

    return (
      <Table
        responsive
        responsiveColumnSize={top ? 3 : undefined}
        keyId="key"
        results={args}
        columns={columns}
        emptyText="No Parameters"
      />
    );
  }
  return <span>{object}</span>;
}
