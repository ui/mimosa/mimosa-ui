import {
  Row,
  Col,
  Card,
  CardGroup,
  Table as BootstrapTable,
  Button,
} from 'react-bootstrap';
import { get } from 'lodash';

import Paginator from 'components/Layout/Paginator';
import useBreakpoint from 'hooks/useBreakpoint';
import { Search } from 'react-bootstrap-icons';

export interface IColumn {
  label: string;
  key: string;
  formatter?: (row: any, column?: any) => string | JSX.Element | null;
  formatterParams?: IFormatterParams;
  className?: string;
}

export interface IResults {
  [key: string]: any;
}

export interface IFormatterParams {
  [key: string]: any;
}

interface IPaginatorProps {
  total: number;
  skip: number;
  limit: number;
}

interface ITable {
  columns: Array<IColumn>;
  results: Array<IResults>;
  keyId: string;
  onRowClick?: any;
  rowClasses?: any;
  emptyText?: string;
  paginator?: IPaginatorProps;
  size?: string;
  urlPrefix?: string;
  responsive?: boolean;
  titleColumn?: string;
  responsiveColumnSize?: number;
}

function getValue(row: IResults, column: IColumn) {
  return column.formatter
    ? column.formatter(row, column)
    : get(row, column.key);
}

function ReflowedTable(props: ITable) {
  const titleColumn = props.columns.find(
    (column) => column.key === props.titleColumn
  );
  return (
    <>
      <CardGroup className="table-reflowed mt-2">
        {props.results.map((row) => (
          <Card
            bg="light"
            key={row[props.keyId]}
            className={props.rowClasses && props.rowClasses(row)}
          >
            {props.titleColumn && titleColumn && row[props.titleColumn] && (
              <Card.Header>
                <Row key="col-title">
                  {titleColumn.label && (
                    <Col className="text-white">{titleColumn.label}</Col>
                  )}
                  <Col className="text-light">{getValue(row, titleColumn)}</Col>
                </Row>
              </Card.Header>
            )}
            <Card.Body>
              {props.columns
                .filter((column) => column.key !== props.titleColumn)
                .map((column) => (
                  <Row key={`col-${column.key}`}>
                    {column.label && (
                      <Col
                        className="text-primary"
                        xs={props.responsiveColumnSize ?? undefined}
                      >
                        {column.label}
                      </Col>
                    )}
                    <Col>
                      <Col>
                        {getValue(row, column) || (
                          <span
                            className="text-black-50
                          "
                          >
                            None
                          </span>
                        )}
                      </Col>
                    </Col>
                  </Row>
                ))}
            </Card.Body>
            {props.onRowClick && (
              <Card.Footer className="d-grid">
                <Button
                  variant="secondary"
                  size="sm"
                  onClick={(e) => {
                    if (props.onRowClick) props.onRowClick(row);
                  }}
                >
                  <Search /> View
                </Button>
              </Card.Footer>
            )}
          </Card>
        ))}
      </CardGroup>
      {!props.results.length && (
        <Card bg="light" body>
          {props.emptyText}
        </Card>
      )}
      {props.paginator && (
        <Paginator
          total={props.paginator.total}
          skip={props.paginator.skip}
          limit={props.paginator.limit}
          urlPrefix={props.urlPrefix}
        />
      )}
    </>
  );
}

export default function Table(props: ITable) {
  const breakpoint = useBreakpoint();
  if (breakpoint === 'xs' && props.responsive) {
    return <ReflowedTable {...props} />;
  }

  return (
    <>
      <BootstrapTable striped hover size={props.size}>
        <thead>
          <tr>
            {props.columns.map((column) => (
              <th key={column.key}>{column.label}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {props.results.map((row) => (
            <tr
              style={{ cursor: props.onRowClick ? 'pointer' : '' }}
              key={row[props.keyId]}
              onClick={(e) => {
                console.log('row on click', (e.target as HTMLElement)?.tagName);
                if (
                  (e.target as HTMLElement)?.tagName !== 'TD' &&
                  (e.target as HTMLElement)?.tagName !== 'SPAN'
                )
                  return;
                if (props.onRowClick) props.onRowClick(row);
              }}
              className={props.rowClasses && props.rowClasses(row)}
            >
              {props.columns.map((column) => (
                <td key={column.key} className={column.className}>
                  {column.formatter
                    ? column.formatter(row, column)
                    : get(row, column.key)}
                </td>
              ))}
            </tr>
          ))}
          {!props.results.length && (
            <tr>
              <td colSpan={props.columns.length}>{props.emptyText}</td>
            </tr>
          )}
        </tbody>
      </BootstrapTable>
      {props.paginator && (
        <Paginator
          total={props.paginator.total}
          skip={props.paginator.skip}
          limit={props.paginator.limit}
          urlPrefix={props.urlPrefix}
        />
      )}
    </>
  );
}
