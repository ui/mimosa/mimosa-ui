import { Button, OverlayTrigger, Popover } from 'react-bootstrap';
import { FileEarmark } from 'react-bootstrap-icons';

export default function FileNameButton({ filename }: { filename: string }) {
  const popover = (
    <Popover id="popover-filename">
      <Popover.Header as="h3">File Name</Popover.Header>
      <Popover.Body>{filename}</Popover.Body>
    </Popover>
  );
  return (
    <OverlayTrigger
      trigger="click"
      placement="left"
      overlay={popover}
      rootClose
    >
      <Button size="sm" className="me-1">
        <FileEarmark />
      </Button>
    </OverlayTrigger>
  );
}
