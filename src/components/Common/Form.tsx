import { ReactElement, useEffect, useRef } from 'react';
import { Spinner, Button } from 'react-bootstrap';

import RJSFForm from 'components/RJSF/Form';
import { useSchema } from 'hooks/useSpec';
import { useInformativeSubmit } from 'hooks/useInformativeSubmit';
import ParsedError from 'components/ParsedError';

export default function Form({
  schemaName,
  resource,
  resourceData,
  buttonTitle,
  formTitle,
  onSuccess,
  initialFormData = {},
  uiSchema = {},
  method,
  withSchemaRootComponents,
}: {
  schemaName: string;
  resource: any;
  resourceData?: {};
  initialFormData?: any;
  buttonTitle?: string | ReactElement;
  formTitle?: string;
  onSuccess?: () => void;
  uiSchema?: any;
  method?: string;
  withSchemaRootComponents?: boolean;
}) {
  const alertRef = useRef<any>();

  const { onSubmit, pending, error, lastFormData, success } =
    useInformativeSubmit({
      resource,
      resourceData,
      alertRef,
      initialFormData,
      ...(method ? { method } : null),
    });

  useEffect(() => {
    if (success) {
      onSuccess && onSuccess();
    }
  }, [success, onSuccess]);

  const schema = useSchema(
    schemaName,
    formTitle || '',
    withSchemaRootComponents
  );

  return (
    <section>
      <ParsedError error={error} ref={alertRef} />
      <RJSFForm
        liveValidate
        schema={schema}
        uiSchema={uiSchema}
        onSubmit={({ formData }) => onSubmit({ formData })}
        formData={lastFormData}
        disabled={pending}
      >
        <div className="d-grid">
          <Button type="submit" disabled={pending}>
            <>
              {pending && (
                <Spinner
                  size="sm"
                  animation="border"
                  role="status"
                  variant="light"
                  className="me-1"
                >
                  <span className="visually-hidden">Loading...</span>
                </Spinner>
              )}
              {buttonTitle || 'Create'}
            </>
          </Button>
        </div>
      </RJSFForm>
    </section>
  );
}
