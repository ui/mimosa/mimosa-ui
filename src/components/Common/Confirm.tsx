import type { ReactElement } from 'react';
import { useState } from 'react';

import { Button } from 'react-bootstrap';
import Modal from './Modal';

interface IConfirmProps {
  title: string;
  message: string;
  action: () => void;
  children: ({ showConfirm }: { showConfirm: () => void }) => ReactElement;
}

export default function Confirm({
  message,
  title,
  action,
  children,
}: IConfirmProps) {
  const [show, setShow] = useState<boolean>(false);

  function showConfirm() {
    setShow(true);
  }

  function doConfirm() {
    action();
    setShow(false);
  }

  return (
    <>
      {children({ showConfirm })}
      <Modal
        title={title}
        show={show}
        showClose
        buttons={
          <Button className="confirm" onClick={doConfirm}>
            OK
          </Button>
        }
        closeText="Cancel"
        onHide={() => setShow(false)}
      >
        <p>{message}</p>
      </Modal>
    </>
  );
}
