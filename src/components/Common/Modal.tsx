import { PropsWithChildren, ReactElement, Suspense, useState } from 'react';
import { Modal as BSModal, Button } from 'react-bootstrap';
import Loading from 'components/Loading';

interface IModalProps
  extends Omit<Omit<IModalButtonProps, 'buttonTitle'>, 'children'> {
  show: boolean;
  onHide: () => void;
  buttons?: ReactElement;
  closeText?: string;
}

interface IModalButtonProps {
  title: string;
  showClose?: boolean;
  buttonTitle: string | ReactElement;
  buttonClasses?: string;
  buttonVariant?: string;
  children: ({ hideModal }: { hideModal: () => void }) => ReactElement;
  disabled?: boolean;
  size?: 'lg' | 'sm' | 'xl';
}

export default function Modal({
  show,
  onHide,
  children,
  title,
  buttons,
  showClose = false,
  closeText = 'Close',
  size = 'lg',
}: PropsWithChildren<IModalProps>) {
  return (
    <BSModal
      size={size}
      fullscreen="sm-down"
      show={show}
      onHide={onHide}
      title={title}
    >
      <BSModal.Header closeButton>
        <BSModal.Title>{title}</BSModal.Title>
      </BSModal.Header>

      <BSModal.Body>
        <Suspense fallback={<Loading />}>{children}</Suspense>
      </BSModal.Body>
      {(showClose || buttons) && (
        <BSModal.Footer>
          {buttons}
          {showClose && <Button onClick={onHide}>{closeText}</Button>}
        </BSModal.Footer>
      )}
    </BSModal>
  );
}

export function ModalButton(props: IModalButtonProps) {
  const [showModal, setShowModal] = useState<boolean>(false);
  const {
    buttonTitle,
    buttonClasses,
    buttonVariant,
    children,
    disabled,
    ...rest
  } = props;
  return (
    <>
      <Modal show={showModal} onHide={() => setShowModal(false)} {...rest}>
        {children({
          hideModal: () => setShowModal(false),
        })}
      </Modal>
      <Button
        className={buttonClasses}
        size="sm"
        variant={buttonVariant}
        onClick={() => setShowModal(true)}
        disabled={disabled}
      >
        {buttonTitle}
      </Button>
    </>
  );
}
