import { useSuspense } from '@data-client/react';

import { PermissionResource } from 'api/resources/Permission';
import Table from 'components/Layout/Table';
import { usePaging } from 'hooks/usePaging';
import { Permission } from 'models/Permission';
import { ReactElement } from 'react';
import { ModalButton } from 'components/Common/Modal';
import CreatePermission from './CreatePermission';
import { Pencil } from 'react-bootstrap-icons';

function EditPermission(row: Permission) {
  return (
    <ModalButton
      title="Edit Permission"
      buttonTitle={
        <>
          <Pencil />
        </>
      }
    >
      {({ hideModal }) => (
        <CreatePermission onSuccess={() => hideModal()} initialFormData={row} />
      )}
    </ModalButton>
  );
}

export default function Permissions({
  userGroupId,
  extraButtons,
  editable = false,
}: {
  userGroupId?: string;
  extraButtons?: (row: Permission) => ReactElement;
  editable?: boolean;
}) {
  const { skip, limit } = usePaging(10, 0, 'permission');
  const permissions = useSuspense(PermissionResource.getList, {
    ...(userGroupId ? { userGroupId } : null),
    skip,
    limit,
  });
  return (
    <Table
      urlPrefix="permission"
      keyId="permissionId"
      results={permissions.results}
      paginator={{
        total: permissions.total,
        skip: permissions.skip,
        limit: permissions.limit,
      }}
      columns={[
        { label: 'Type', key: 'type' },
        { label: 'Description', key: 'description' },
        ...(extraButtons || editable
          ? [
              {
                label: '',
                key: 'delete',
                className: 'text-end',
                formatter: (row: Permission) => (
                  <>
                    {extraButtons && extraButtons(row)}
                    {editable && <EditPermission {...row} />}
                  </>
                ),
              },
            ]
          : []),
      ]}
      emptyText="No permissions yet"
    />
  );
}
