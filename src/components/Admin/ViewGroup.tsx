import { useController, useSuspense } from '@data-client/react';
import { UserGroupResource } from 'api/resources/UserGroup';
import { useParams } from 'react-router-dom';
import Permissions from './Permissions';
import { PersonResource } from 'api/resources/Person';
import Table from 'components/Layout/Table';
import { usePaging } from 'hooks/usePaging';

import { Button } from 'react-bootstrap';
import { Plus, Trash } from 'react-bootstrap-icons';
import { Permission } from 'models/Permission';
import { Person } from 'models/Person';
import { UserGroupPermissionResource } from 'api/resources/UserGroupPermission';
import { UserGroupPersonResource } from 'api/resources/UserGroupPerson';
import { PermissionResource } from 'api/resources/Permission';
import { ModalButton } from 'components/Common/Modal';
import Form from 'components/Common/Form';
import Confirm from 'components/Common/Confirm';

function AddPermission({
  userGroupId,
  onSuccess,
}: {
  userGroupId: string;
  onSuccess?: () => void;
}) {
  const { invalidateAll } = useController();

  function success() {
    invalidateAll(PermissionResource.getList);
    onSuccess && onSuccess();
  }

  return (
    <Form
      schemaName="NewUserGroupPermission"
      resource={UserGroupPermissionResource}
      resourceData={{ userGroupId }}
      onSuccess={() => success()}
      buttonTitle="Add Permission"
      uiSchema={{
        permissionId: {
          'ui:options': {
            resource: PermissionResource,
            key: 'type',
            value: 'permissionId',
          },
          'ui:widget': 'remoteReactSelect',
        },
      }}
    />
  );
}

function AddPerson({
  userGroupId,
  onSuccess,
}: {
  userGroupId: string;
  onSuccess?: () => void;
}) {
  const { invalidateAll } = useController();

  function success() {
    invalidateAll(PersonResource.getList);
    onSuccess && onSuccess();
  }

  return (
    <Form
      schemaName="NewUserGroupPerson"
      resource={UserGroupPersonResource}
      resourceData={{ userGroupId }}
      onSuccess={() => success()}
      buttonTitle="Add Person"
      uiSchema={{
        personId: {
          'ui:options': {
            resource: PersonResource,
            params: {
              showAll: true,
            },
            key: (row: Person) => `${row.givenName} ${row.familyName}`,
            value: 'personId',
          },
          'ui:widget': 'remoteReactSelect',
        },
      }}
    />
  );
}

function DeletePermission({
  userGroupId,
  permission,
}: {
  userGroupId: string;
  permission: Permission;
}) {
  const { fetch, invalidateAll } = useController();

  function deletePermission() {
    fetch(UserGroupPermissionResource.delete, {
      userGroupId: userGroupId,
      permissionId: permission.permissionId,
    }).then(() => {
      invalidateAll(PermissionResource.getList);
    });
  }

  return (
    <Confirm
      action={deletePermission}
      message="Are you sure you want to remove this permission from this group"
      title="Delete Permission from Group"
    >
      {({ showConfirm }) => (
        <Button
          size="sm"
          onClick={() => showConfirm()}
          title="Delete Permission"
        >
          <Trash />
        </Button>
      )}
    </Confirm>
  );
}

function DeletePerson({
  userGroupId,
  person,
}: {
  userGroupId: string;
  person: Person;
}) {
  const { fetch, invalidateAll } = useController();

  function deletePerson() {
    fetch(UserGroupPersonResource.delete, {
      userGroupId: userGroupId,
      personId: person.personId,
    }).then(() => {
      invalidateAll(PersonResource.getList);
    });
  }

  return (
    <Confirm
      action={deletePerson}
      message="Are you sure you want to remove this person from this group"
      title="Delete Person from Group"
    >
      {({ showConfirm }) => (
        <Button size="sm" onClick={() => showConfirm()} title="Delete Person">
          <Trash />
        </Button>
      )}
    </Confirm>
  );
}

function People({ userGroupId }: { userGroupId?: string }) {
  const { skip, limit } = usePaging(10, 0, 'group');
  const people = useSuspense(PersonResource.getList, {
    ...(userGroupId ? { userGroupId } : null),
    showAll: true,
    skip,
    limit,
  });

  return (
    <>
      <Table
        urlPrefix="group"
        keyId="userGroupId"
        results={people.results}
        paginator={{
          total: people.total,
          skip: people.skip,
          limit: people.limit,
        }}
        columns={[
          { label: 'Given Name', key: 'givenName' },
          { label: 'Family Name', key: 'familyName' },
          { label: 'Login', key: 'login' },
          {
            label: '',
            key: 'delete',
            className: 'text-end',
            formatter: (row) =>
              userGroupId ? (
                <DeletePerson userGroupId={userGroupId} person={row} />
              ) : null,
          },
        ]}
        emptyText="No people yet"
      />
    </>
  );
}

export default function ViewGroup() {
  const { userGroupId } = useParams();
  const group = useSuspense(
    UserGroupResource.get,
    userGroupId ? { userGroupId } : null
  );
  if (!userGroupId) return null;
  return (
    <section>
      <h1>Group: {group?.name}</h1>

      <h2>Permissions</h2>
      <div className="text-end">
        <ModalButton
          title="Add Permission"
          buttonTitle={
            <>
              <Plus />
              Add
            </>
          }
        >
          {({ hideModal }) => (
            <AddPermission
              userGroupId={userGroupId}
              onSuccess={() => hideModal()}
            />
          )}
        </ModalButton>
      </div>
      <Permissions
        userGroupId={userGroupId}
        extraButtons={(row) => (
          <DeletePermission userGroupId={userGroupId} permission={row} />
        )}
      />

      <h2>People</h2>
      <div className="text-end">
        <ModalButton
          title="Add Person"
          buttonTitle={
            <>
              <Plus />
              Add
            </>
          }
        >
          {({ hideModal }) => (
            <AddPerson
              userGroupId={userGroupId}
              onSuccess={() => hideModal()}
            />
          )}
        </ModalButton>
      </div>
      <People userGroupId={userGroupId} />
    </section>
  );
}
