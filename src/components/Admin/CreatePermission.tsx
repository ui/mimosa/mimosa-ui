import { useController } from '@data-client/react';
import { PermissionResource } from 'api/resources/Permission';
import Form from 'components/Common/Form';
import { Permission } from 'models/Permission';

export default function CreatePermission({
  onSuccess,
  initialFormData,
}: {
  onSuccess?: () => void;
  initialFormData?: Permission;
}) {
  const { invalidateAll } = useController();

  function success() {
    invalidateAll(PermissionResource.getList);
    onSuccess && onSuccess();
  }

  const extraProps: Record<string, any> = {};
  if (initialFormData) {
    extraProps.initialFormData = {
      type: initialFormData.type,
      description: initialFormData.description,
    };
    extraProps.method = 'partialUpdate';
    extraProps.buttonTitle = 'Save';
    extraProps.resourceData = { permissionId: initialFormData.permissionId };
  }

  return (
    <Form
      schemaName="NewPermission"
      resource={PermissionResource}
      onSuccess={() => success()}
      {...extraProps}
    />
  );
}
