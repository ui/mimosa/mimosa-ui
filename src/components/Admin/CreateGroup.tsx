import { useController } from '@data-client/react';
import { UserGroupResource } from 'api/resources/UserGroup';
import Form from 'components/Common/Form';
import { UserGroup } from 'models/UserGroup';

export default function CreateGroup({
  onSuccess,
  initialFormData,
}: {
  onSuccess?: () => void;
  initialFormData?: UserGroup;
}) {
  const { invalidateAll } = useController();

  function success() {
    invalidateAll(UserGroupResource.getList);
    onSuccess && onSuccess();
  }

  const extraProps: Record<string, any> = {};
  if (initialFormData) {
    extraProps.initialFormData = { name: initialFormData.name };
    extraProps.method = 'partialUpdate';
    extraProps.buttonTitle = 'Save';
    extraProps.resourceData = { userGroupId: initialFormData.userGroupId };
  }

  return (
    <Form
      schemaName="NewUserGroup"
      resource={UserGroupResource}
      onSuccess={() => success()}
      {...extraProps}
    />
  );
}
