import { useNavigate } from 'react-router-dom';
import { useSuspense } from '@data-client/react';

import { UserGroupResource } from 'api/resources/UserGroup';
import Search from 'components/Layout/Search';
import Table from 'components/Layout/Table';
import { usePaging } from 'hooks/usePaging';
import { useSearch } from 'hooks/useSearch';
import { UserGroup } from 'models/UserGroup';
import Permissions from './Permissions';
import { ModalButton } from 'components/Common/Modal';
import CreatePermission from './CreatePermission';
import CreateGroup from './CreateGroup';
import { Pencil, Plus } from 'react-bootstrap-icons';

function EditGroup(row: UserGroup) {
  return (
    <ModalButton
      title="Edit Group"
      buttonTitle={
        <>
          <Pencil />
        </>
      }
    >
      {({ hideModal }) => (
        <CreateGroup onSuccess={() => hideModal()} initialFormData={row} />
      )}
    </ModalButton>
  );
}

function Groups({ editable }: { editable?: boolean }) {
  const navigate = useNavigate();
  const { skip, limit } = usePaging(10, 0, 'group');
  const search = useSearch();
  const groups = useSuspense(UserGroupResource.getList, {
    ...(search ? { search } : null),
    skip,
    limit,
  });

  const onRowClick = (row: UserGroup) => {
    navigate(`/admin/groups/${row.userGroupId}`);
  };

  return (
    <>
      <Search focus={true} />
      <Table
        urlPrefix="group"
        keyId="userGroupId"
        results={groups.results}
        onRowClick={onRowClick}
        paginator={{
          total: groups.total,
          skip: groups.skip,
          limit: groups.limit,
        }}
        columns={[
          { label: 'Name', key: 'name' },
          { label: '# Permissions', key: '_metadata.permissions' },
          { label: '# People', key: '_metadata.people' },
          ...(editable
            ? [
                {
                  label: '',
                  key: 'delete',
                  className: 'text-end',
                  formatter: (row: UserGroup) => <EditGroup {...row} />,
                },
              ]
            : []),
        ]}
        emptyText="No groups yet"
      />
    </>
  );
}

export default function GroupsPermissions() {
  return (
    <section>
      <h2>Groups</h2>
      <div className="text-end mb-1">
        <ModalButton
          title="Create Group"
          buttonTitle={
            <>
              <Plus />
              Create
            </>
          }
        >
          {({ hideModal }) => <CreateGroup onSuccess={() => hideModal()} />}
        </ModalButton>
      </div>
      <Groups editable />

      <h2>Permissions</h2>
      <div className="text-end">
        <ModalButton
          title="Create Permission"
          buttonTitle={
            <>
              <Plus />
              Create
            </>
          }
        >
          {({ hideModal }) => (
            <CreatePermission onSuccess={() => hideModal()} />
          )}
        </ModalButton>
      </div>
      <Permissions editable />
    </section>
  );
}
