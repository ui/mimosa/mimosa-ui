import Form from 'components/Common/Form';
import { useSuspense } from '@data-client/react';
import { OptionsResource } from 'api/resources/Options';

export default function Options() {
  const options = useSuspense(OptionsResource.get);
  return (
    <Form
      schemaName="Options"
      withSchemaRootComponents
      resource={OptionsResource}
      method="partialUpdate"
      initialFormData={options}
      buttonTitle="Save Options"
    />
  );
}
