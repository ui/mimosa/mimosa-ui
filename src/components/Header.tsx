import { Suspense, useEffect, useState } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { useController } from '@data-client/react';
import {
  Navbar,
  NavDropdown,
  Container,
  Nav,
  Button,
  ToggleButton,
  ButtonGroup,
} from 'react-bootstrap';
import { MoonFill, PersonBadge, SunFill } from 'react-bootstrap-icons';

import { useAuth } from 'hooks/useAuth';
import { useProposal } from 'hooks/useProposal';
import { useProposalInfo } from 'hooks/useProposalInfo';
import { useCurrentUser } from 'hooks/useCurrentUser';
import AuthErrorBoundary from './AuthErrorBoundary';
import useComponentsTitle from 'hooks/useComponentsTitle';
import { getMode, setMode } from 'utils/theme';

function PersonMenu() {
  const currentUser = useCurrentUser();
  return (
    <NavDropdown
      title={
        <>
          <PersonBadge className="me-1" />
          {currentUser.login}
        </>
      }
      id="admin-nav-dropdown"
      align="end"
    >
      <NavDropdown.Header>
        {currentUser.givenName} {currentUser.familyName}
      </NavDropdown.Header>
      <AdminMenu />
      <BeamlineMenu />
    </NavDropdown>
  );
}

export function Logout() {
  const { clearToken } = useAuth();
  const { resetEntireStore } = useController();
  return (
    <Button
      onClick={() => {
        clearToken();
        resetEntireStore();
      }}
    >
      Logout
    </Button>
  );
}

function AdminMenu() {
  const currentUser = useCurrentUser();
  const adminPermissions = ['manage_options', 'manage_groups'];
  const userAdminPermissions = adminPermissions.filter((adminPermission) =>
    currentUser.Permissions.includes(adminPermission)
  );
  return (
    <>
      {userAdminPermissions.length > 0 && (
        <>
          <NavDropdown.Divider />
          <NavDropdown.Header>Administration</NavDropdown.Header>
          {currentUser.Permissions.includes('manage_options') && (
            <NavDropdown.Item as={Link} to={`/admin/options`}>
              Manage Options
            </NavDropdown.Item>
          )}
          {currentUser.Permissions.includes('manage_groups') && (
            <NavDropdown.Item as={Link} to={`/admin/groups`}>
              Manage Groups
            </NavDropdown.Item>
          )}
        </>
      )}
    </>
  );
}

function BeamlineMenu() {
  const currentUser = useCurrentUser();
  return (
    <>
      {currentUser.beamLines.length > 0 && (
        <>
          <NavDropdown.Divider />
          <NavDropdown.Header>My Beamlines</NavDropdown.Header>
          {currentUser.beamLines.map((beamLine) => (
            <NavDropdown.Item
              key={beamLine}
              as={Link}
              to={`/beamline/${beamLine}`}
            >
              {beamLine}
            </NavDropdown.Item>
          ))}
        </>
      )}
    </>
  );
}

function ThemeButton() {
  const [darkMode, setDarkMode] = useState<boolean>(getMode());

  return (
    <ButtonGroup>
      <ToggleButton
        id="dark-mode"
        type="checkbox"
        checked={darkMode}
        value="1"
        onChange={(e) => {
          setDarkMode(e.currentTarget.checked);
          setMode(e.currentTarget.checked, true);
        }}
      >
        {darkMode ? <MoonFill /> : <SunFill />}
        <span className="visually-hidden">Color Mode</span>
      </ToggleButton>
    </ButtonGroup>
  );
}

export default function Header() {
  const { isAuthenticated } = useAuth();

  return (
    <Navbar
      bg="primary"
      variant="dark"
      expand="md"
      sticky="top"
      className="main-header p-2"
    >
      <Container>
        <Navbar.Brand as={Link} to="/">
          Home
        </Navbar.Brand>
        <Suspense fallback="...">
          <AuthErrorBoundary>
            <>{isAuthenticated && <HeaderMain />}</>
          </AuthErrorBoundary>
        </Suspense>
      </Container>
    </Navbar>
  );
}

function HeaderMain() {
  const { proposalName } = useProposal();
  const proposalInfo = useProposalInfo();
  const { pathname } = useLocation();
  const componentsTitle = useComponentsTitle();

  return (
    <>
      <Navbar.Toggle aria-controls="main-navbar" />
      <Navbar.Collapse id="main-navbar">
        <Nav className="me-auto">
          <Nav.Link as={NavLink} to="/calendar">
            Calendar
          </Nav.Link>
          <Nav.Link as={NavLink} to="/proposals" end>
            Proposals
          </Nav.Link>
          {!proposalName && (
            <Nav.Link className="nav-link" eventKey="disabled" disabled>
              No Proposal
            </Nav.Link>
          )}
          {proposalName && (
            <NavDropdown
              active={pathname.includes(proposalName)}
              title={proposalName}
              id="proposal-nav-dropdown"
            >
              <>
                <NavDropdown.Item
                  as={NavLink}
                  to={`/proposals/${proposalName}/sessions`}
                >
                  Sessions
                </NavDropdown.Item>
                <NavDropdown.Item
                  as={NavLink}
                  to={`/proposals/${proposalName}/${componentsTitle.toLowerCase()}`}
                >
                  {componentsTitle}
                </NavDropdown.Item>
                <NavDropdown.Item
                  as={NavLink}
                  end
                  to={`/proposals/${proposalName}/samples/containers`}
                >
                  Containers
                </NavDropdown.Item>
                <NavDropdown.Item
                  as={NavLink}
                  end
                  to={`/proposals/${proposalName}/samples`}
                >
                  Samples
                </NavDropdown.Item>
                {proposalInfo &&
                  proposalInfo._metadata.uiGroups &&
                  proposalInfo._metadata.uiGroups.includes('mapping') && (
                    <>
                      <NavDropdown.Item
                        as={NavLink}
                        to={`/proposals/${proposalName}/samples/review`}
                      >
                        Sample Review
                      </NavDropdown.Item>
                      <NavDropdown.Item
                        as={NavLink}
                        to={`/proposals/${proposalName}/queue`}
                      >
                        Acquisition Queue
                      </NavDropdown.Item>
                    </>
                  )}
              </>
            </NavDropdown>
          )}
        </Nav>

        <Nav>
          <AuthErrorBoundary>
            <Suspense fallback={<span>...</span>}>
              <PersonMenu />
            </Suspense>
          </AuthErrorBoundary>
          <ThemeButton />
          <Logout />
        </Nav>
      </Navbar.Collapse>
    </>
  );
}
