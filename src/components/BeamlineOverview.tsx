import { usePath } from 'hooks/usePath';
import EventList from './Events/EventsList';
import DataCollectionTimes from './Stats/DataCollectionTimes';
import ParameterHistogram from './Stats/ParameterHistogram';
import { Accordion, Col, Container, Row } from 'react-bootstrap';

export default function BeamlineOverview() {
  const beamLineName = usePath('beamLineName');
  return (
    <section>
      <h1>{beamLineName}</h1>
      {/* <DataCollectionTimes beamLineName={beamLineName} /> */}
      <Accordion className="mb-3">
        <Accordion.Item eventKey="0">
          <Accordion.Header>Statistics</Accordion.Header>
          <Accordion.Body>
            <Row>
              <Col xs={12} sm={true}></Col>
              <Col xs={12} sm={true}>
                <ParameterHistogram
                  beamLineName={beamLineName}
                  parameter="stepsizex"
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={true}>
                <ParameterHistogram
                  beamLineName={beamLineName}
                  parameter="energy"
                />
              </Col>
              <Col xs={12} sm={true}>
                <ParameterHistogram
                  beamLineName={beamLineName}
                  parameter="exposuretime"
                />
              </Col>
            </Row>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>

      <EventList beamLineName={beamLineName} />
    </section>
  );
}
