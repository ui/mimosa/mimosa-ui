import { useSuspense } from '@data-client/react';
import { Search, GraphUp } from 'react-bootstrap-icons';

import { EventResource } from 'api/resources/Event';
import StatusBadge from 'components/Events/DataCollections/StatusBadge';
import Table from 'components/Layout/Table';
import { usePaging } from 'hooks/usePaging';
import DataCollectionAttachmentPlot from 'components/Events/DataCollections/DataCollectionAttachmentPlot';
import { ModalButton } from 'components/Common/Modal';
import CreateScalarMapButton from './CreateScalarMap';
import RegenerateMapsButton from './RegenerateMaps';
import EventList from 'components/Events/EventsList';

function DataCollectionModalButton({
  dataCollectionId,
  dataCollectionGroupId,
}: {
  dataCollectionId?: number;
  dataCollectionGroupId?: number;
}) {
  return (
    <ModalButton title="Data Collection" buttonTitle={<Search />} size="xl">
      {() => (
        <EventList
          dataCollectionId={dataCollectionId}
          dataCollectionGroupId={dataCollectionGroupId}
          single
        />
      )}
    </ModalButton>
  );
}

function DataCollectionAttachmentPlotModal({
  dataCollectionId,
}: {
  dataCollectionId: number;
}) {
  return (
    <ModalButton title="Attachment Plot" buttonTitle={<GraphUp />}>
      {() => (
        <DataCollectionAttachmentPlot
          dataCollectionId={dataCollectionId}
          xAxisTitle="Energy (keV)"
          yAxisTitle="Counts"
        />
      )}
    </ModalButton>
  );
}

export default function DataCollectionList({
  blSubSampleId,
}: {
  blSubSampleId: number;
}) {
  const { skip, limit } = usePaging(10, 0);
  const datacollections = useSuspense(EventResource.getList, {
    blSubSampleId,
    skip,
    limit,
  });
  return (
    <>
      <h2>Data Collections</h2>
      <Table
        keyId="id"
        results={datacollections.results}
        paginator={{
          total: datacollections.total,
          skip: datacollections.skip,
          limit: datacollections.limit,
        }}
        columns={[
          {
            label: 'ID',
            key: 'id',
            className: 'text-break',
          },
          {
            label: 'Type',
            key: 'Item.DataCollectionGroup.experimentType',
          },
          {
            label: 'Status',
            key: 'runStatus',
            formatter: (row) => <StatusBadge status={row.Item.runStatus} />,
          },
          {
            label: '',
            key: 'actions',
            className: 'text-nowrap text-end',
            formatter: (row) => (
              <>
                {row.Item.DataCollectionGroup.experimentType ===
                  'Energy scan' && (
                  <DataCollectionAttachmentPlotModal
                    dataCollectionId={row.Item.dataCollectionId}
                  />
                )}
                {row.Item.DataCollectionGroup.experimentType !==
                  'Energy scan' && (
                  <>
                    <CreateScalarMapButton
                      dataCollectionId={row.Item.dataCollectionId}
                    />
                    <RegenerateMapsButton
                      blSampleId={row.blSampleId}
                      dataCollectionId={row.Item.dataCollectionId}
                      noTitle
                    />
                  </>
                )}
                <DataCollectionModalButton
                  dataCollectionId={
                    row.count === 1 ? row.Item.dataCollectionId : undefined
                  }
                  dataCollectionGroupId={
                    row.count > 1
                      ? row.Item.DataCollectionGroup.dataCollectionGroupId
                      : undefined
                  }
                />
              </>
            ),
          },
        ]}
        emptyText="No data collections yet"
      />
    </>
  );
}
