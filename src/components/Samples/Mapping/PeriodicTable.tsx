import { find, range, flatten } from 'lodash';
import classNames from 'classnames';
import { useController } from '@data-client/react';

// @ts-expect-error
import elements from 'data/elements';
import { ModalButton } from 'components/Common/Modal';
import Table from 'components/Layout/Table';
import { MapROIResource } from 'api/resources/MapROI';
import { toPrecision } from 'components/Layout/TableCells';

interface PeriodicElement {
  name?: string;
  symbol?: string;
  edges?: any[];
}

function PeriodicLines(props: {
  onHide: () => void;
  element: PeriodicElement;
  sigmaMca: number;
  blSampleId: number;
}) {
  const { fetch, invalidateAll } = useController();
  const roiLines = new Set([
    'K-L3',
    'L1-M3',
    'L2-M4',
    'L3-M5',
    'M1-N2',
    'M2-N4',
    'M3-N5',
    'M4-N6',
    'M5-N7',
  ]);
  const roiExtra = new Set(['K-M3', 'L2-M4']);

  const lines = flatten(
    props.element && props.element.edges?.map((e) => e.lines)
  );
  const filtered = lines.filter((l) => {
    return roiLines.has(l.iupac) || roiExtra.has(l.iupac);
  });

  const onRowClick = async (row: any) => {
    const eHole = 3.85;
    const fano = 0.114;

    const eev = row.energy * 1000;
    const sigmaMca = props.sigmaMca ?? 45;
    const sig = Math.sqrt(sigmaMca ** 2 + fano * eHole * eev);

    const element = props.element ?? {};
    await fetch(MapROIResource.create, {
      // @ts-expect-error
      blSampleId: props.blSampleId,
      element: element.symbol,
      edge: row.siegbahn,
      startEnergy: Math.round(eev - sig / 2),
      endEnergy: Math.round(eev + sig / 2),
    });

    invalidateAll(MapROIResource.getList);
    props.onHide();
  };

  return (
    <Table
      keyId="iupac"
      results={filtered}
      onRowClick={onRowClick}
      columns={[
        {
          label: 'IUPAC',
          key: 'iupac',
        },
        {
          label: 'Siegbahn',
          key: 'siegbahn',
        },
        {
          label: 'Energy (keV)',
          key: 'energy',
          formatter: toPrecision(3),
        },
      ]}
    />
  );
}

interface PeriodicItemProps {
  blSampleId: number;
  id: number;
  elements: any;
  noEdges: boolean;
  selected?: boolean;
}

function PeriodicItem(props: PeriodicItemProps) {
  const ext = props.id > 144 ? -1 : 0;
  const el = find(props.elements, {
    xpos: (props.id % 18) + 1 + ext,
    ypos: Math.floor(props.id / 18) + 1,
  });

  return (
    <div className="item">
      {el !== undefined && (
        <ModalButton
          title={el.name}
          showClose
          buttonTitle={
            <>
              <span className="number">{el.number}</span>
              <h2>{el.symbol}</h2>
              <p>{el.name}</p>
            </>
          }
          buttonClasses={classNames('periodic-element', `period-${el.period}`, {
            selected: props.selected,
            'no-edges':
              props.noEdges &&
              (!el.edges || (el.edges && el.edges.length === 0)),
          })}
        >
          {({ hideModal }) => (
            <PeriodicLines
              onHide={() => hideModal()}
              element={el}
              sigmaMca={200}
              blSampleId={props.blSampleId}
            />
          )}
        </ModalButton>
      )}
    </div>
  );
}

export default function PeriodicTable(props: { blSampleId: number }) {
  const items = range(18 * 10).map((i) => {
    return (
      <PeriodicItem
        key={i}
        id={i}
        elements={elements}
        noEdges={false}
        blSampleId={props.blSampleId}
      />
    );
  });

  return <div className="periodic-table">{items}</div>;
}
