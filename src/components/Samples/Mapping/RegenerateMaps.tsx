import { useController } from '@data-client/react';
import { ArrowClockwise } from 'react-bootstrap-icons';

import Form from 'components/Common/Form';
import { ModalButton } from 'components/Common/Modal';
import { MapResource } from 'api/resources/Map';
import { RegenerateMapsEndpoint } from 'api/resources/RegenerateMaps';

interface ButtonProps {
  blSampleId: number;
  dataCollectionId?: number;
  noTitle?: boolean;
}

export default function RegenerateMapsButton(props: ButtonProps) {
  return (
    <ModalButton
      title="Regenerate Maps"
      buttonTitle={
        <>
          {props.noTitle ? (
            <ArrowClockwise />
          ) : (
            <>
              <ArrowClockwise className="me-1" />
              Regenerate
            </>
          )}
        </>
      }
      buttonClasses={props.noTitle ? 'ms-1' : 'me-1'}
    >
      {({ hideModal }) => (
        <RegenerateMaps onHide={() => hideModal()} {...props} />
      )}
    </ModalButton>
  );
}

interface Props extends ButtonProps {
  onHide?: () => void;
}

function RegenerateMaps(props: Props) {
  const { dataCollectionId, blSampleId, onHide } = props;
  const { invalidateAll } = useController();

  function success() {
    invalidateAll(MapResource.getList);
    onHide && onHide();
  }

  const uiSchema = {
    blSampleId: { 'ui:classNames': 'hidden-row', 'ui:widget': 'hidden' },
    dataCollectionId: { 'ui:classNames': 'hidden-row', 'ui:widget': 'hidden' },
  };

  return (
    <Form
      schemaName="RegenerateMaps"
      uiSchema={uiSchema}
      resource={RegenerateMapsEndpoint}
      initialFormData={{
        blSampleId,
        dataCollectionId,
      }}
      onSuccess={() => success()}
      buttonTitle="Regenerate Maps"
    />
  );
}
