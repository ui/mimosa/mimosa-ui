import { Suspense, useState, useEffect, useCallback, useRef } from 'react';
import { useSuspense } from '@data-client/react';
import {
  Container,
  Row,
  Col,
  InputGroup,
  Alert,
  Button,
} from 'react-bootstrap';
import {
  useNavigate,
  useSearchParams,
  createSearchParams,
} from 'react-router-dom';

import { SessionContainerResource } from 'api/resources/Container';
import { SampleResource } from 'api/resources/Sample';
import { usePath } from 'hooks/usePath';

import SubSampleList from './SubSampleList';
import SubSampleView from './SubSampleView';
import SampleCanvas from './SampleCanvas';
import { Search, X } from 'react-bootstrap-icons';
import Loading from 'components/Loading';
import { ModalButton } from 'components/Common/Modal';
import SamplesList from '../SamplesList';
import { useSessionInfo } from 'hooks/useSessionInfo';

function SubSamplePanel({
  blSampleId,
  selectedSubSample,
  setSelectedSubSample,
}: {
  blSampleId: number;
  selectedSubSample: number | undefined;
  setSelectedSubSample: (blSubsampleId: number) => void;
}) {
  return (
    <>
      <Suspense fallback={<Loading title="Sub Samples" />}>
        <SubSampleList
          blSampleId={blSampleId}
          selectedSubSample={selectedSubSample}
          selectSubSample={setSelectedSubSample}
        />
      </Suspense>
      {selectedSubSample && (
        <Suspense fallback={<Loading title="Sub Sample" />}>
          <SubSampleView blSubSampleId={selectedSubSample} />
        </Suspense>
      )}
      {!selectedSubSample && <div>Please select a subsample</div>}
    </>
  );
}
interface ISelectSample {
  selectedSample?: number;
  selectSample: (sampleId: number) => void;
}

function SelectSample(props: ISelectSample) {
  const navigate = useNavigate();
  const { selectedSample, selectSample } = props;
  const [searchParams] = useSearchParams();
  // @ts-ignore
  const searchParamsObj = Object.fromEntries([...searchParams]);

  const container = useSuspense(
    SessionContainerResource.get,
    searchParamsObj.sessionId
      ? {
          containerId: searchParamsObj.sessionId,
        }
      : null
  );
  const sessionInfo = useSessionInfo(searchParamsObj.sessionId);

  const clearSessionId = useCallback(() => {
    const { sessionId, ...newSearchParamsObj } = searchParamsObj;
    navigate({
      pathname: '',
      search: createSearchParams({
        ...newSearchParamsObj,
      }).toString(),
    });
  }, [navigate, searchParamsObj]);

  const proposal = usePath('proposal');
  const currentSample = useSuspense(SampleResource.get, {
    ...(proposal ? { proposal } : null),
    blSampleId: `${selectedSample}`,
  });

  return (
    <div className="d-flex">
      <InputGroup className="me-1" style={{ flexWrap: 'nowrap' }}>
        <InputGroup.Text>{currentSample?.name}</InputGroup.Text>
        <ModalButton
          title={`Select Sample ${
            container ? 'from ' + sessionInfo?.session : ''
          }`}
          buttonTitle={<Search />}
        >
          {() => (
            <SamplesList
              embedded
              selectSample={selectSample}
              containerId={container ? `${container?.containerId}` : undefined}
            />
          )}
        </ModalButton>
      </InputGroup>
      {sessionInfo && (
        <InputGroup className="me-1" style={{ flexWrap: 'nowrap' }}>
          <InputGroup.Text>{sessionInfo.session}</InputGroup.Text>
          <Button variant="danger" onClick={() => clearSessionId()}>
            <X />
          </Button>
        </InputGroup>
      )}
    </div>
  );
}

function SampleReviewMain() {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  // @ts-ignore
  const searchParamsObj = Object.fromEntries([...searchParams]);

  const [canvasMounted, setCanvasMount] = useState<boolean>(true);
  const proposal = usePath('proposal');
  const samples = useSuspense(SampleResource.getList, {
    ...(proposal ? { proposal } : null),
    ...(searchParamsObj.sample ? { search: searchParamsObj.sample } : null),
  });
  const [selectedSample, setSelectedSample] = useState<number | undefined>(
    searchParamsObj.blSampleId
      ? parseInt(searchParamsObj.blSampleId)
      : samples.results.length
      ? samples.results[0].blSampleId
      : undefined
  );
  const [selectedSubSample, setSelectedSubSample] = useState<number>();

  useEffect(() => {
    setSelectedSubSample(undefined);
  }, [selectedSample]);

  const selectSample = useCallback(
    (blSampleId: number) => {
      setSelectedSample(blSampleId);
      setCanvasMount(false);
      setTimeout(() => {
        setCanvasMount(true);
      }, 100);
      navigate({
        pathname: '',
        search: createSearchParams({
          ...searchParamsObj,
          blSampleId: `${blSampleId}`,
        }).toString(),
      });
    },
    [setSelectedSample, setCanvasMount, navigate, searchParamsObj]
  );

  useEffect(() => {
    if (samples.results.length === 1)
      selectSample(samples.results[0].blSampleId);
  }, [samples.results]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <div key="header"></div>
      <Container fluid>
        {!selectedSample && <Alert>No samples for this propsal</Alert>}
        {selectedSample && (
          <Row>
            <Col sm={9}>
              {canvasMounted ? (
                <Suspense fallback={<Loading title="Sample Canvas" />}>
                  <SampleCanvas
                    blSampleId={selectedSample}
                    selectSubSample={setSelectedSubSample}
                    selectedSubSample={selectedSubSample}
                    selectSample={
                      <SelectSample
                        selectedSample={selectedSample}
                        selectSample={selectSample}
                      />
                    }
                  />
                </Suspense>
              ) : null}
            </Col>
            <Col sm={3}>
              <SubSamplePanel
                blSampleId={selectedSample}
                selectedSubSample={selectedSubSample}
                setSelectedSubSample={setSelectedSubSample}
              />
            </Col>
          </Row>
        )}
      </Container>
    </>
  );
}

export default function SampleReview() {
  return (
    <Suspense fallback={<Loading title="Samples" />}>
      <SampleReviewMain />
    </Suspense>
  );
}
