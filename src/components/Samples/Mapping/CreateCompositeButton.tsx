import { useRef, useEffect } from 'react';
import { Button, Form, Col, Row } from 'react-bootstrap';
import { Image } from 'react-bootstrap-icons';
import { useController } from '@data-client/react';

import { Map } from 'models/Map';
import { getROIName } from './Maps';
import { CompositeMapResource } from 'api/resources/CompositeMap';
import { useInformativeSubmit } from 'hooks/useInformativeSubmit';
import ParsedError from 'components/ParsedError';
import { ModalButton } from 'components/Common/Modal';

interface ICreateComposite {
  mapIds: number[];
  maps: Map[];
  show?: boolean;
  onHide?: () => void;
}

function CreateComposite({ maps, mapIds, onHide }: ICreateComposite) {
  const controller = useController();
  const alertRef = useRef<HTMLDivElement>(null);
  const rRef = useRef<HTMLSelectElement>(null);
  const gRef = useRef<HTMLSelectElement>(null);
  const bRef = useRef<HTMLSelectElement>(null);

  const roRef = useRef<HTMLInputElement>(null);
  const goRef = useRef<HTMLInputElement>(null);
  const boRef = useRef<HTMLInputElement>(null);

  const selectedMaps = maps
    .filter((map) => mapIds.indexOf(map.xrfFluorescenceMappingId) > -1)
    .sort((a, b) => getROIName(a).localeCompare(getROIName(b)));

  const opts = selectedMaps.map((map) => (
    <option
      key={map.xrfFluorescenceMappingId}
      value={map.xrfFluorescenceMappingId}
    >
      {getROIName(map)}
    </option>
  ));

  const { onSubmit, pending, error, success } = useInformativeSubmit({
    resource: CompositeMapResource,
    alertRef,
    initialFormData: {},
  });

  function createComposite() {
    onSubmit({
      formData: {
        r: rRef.current?.value,
        g: gRef.current?.value,
        b: bRef.current?.value,
        rOpacity: roRef.current && parseFloat(roRef.current?.value) / 100,
        gOpacity: goRef.current && parseFloat(goRef.current?.value) / 100,
        bOpacity: boRef.current && parseFloat(boRef.current?.value) / 100,
      },
    });
  }

  useEffect(() => {
    if (success) {
      controller.invalidateAll(CompositeMapResource.getList);
      onHide && onHide();
    }
  }, [success, onHide, controller]);

  return (
    <>
      <ParsedError error={error} ref={alertRef} />
      <Form>
        <Form.Group as={Row}>
          <Col sm={3} />
          <Col>Map</Col>
          <Col>Opacity</Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label column sm={3}>
            Red
          </Form.Label>
          <Col>
            <Form.Control
              as="select"
              defaultValue={selectedMaps[0].xrfFluorescenceMappingId}
              ref={rRef}
            >
              {opts}
            </Form.Control>
          </Col>
          <Col>
            <Form.Range min="0" max="100" defaultValue="100" ref={roRef} />
          </Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label column sm={3}>
            Green
          </Form.Label>
          <Col>
            <Form.Control
              as="select"
              defaultValue={selectedMaps[1].xrfFluorescenceMappingId}
              ref={gRef}
            >
              {opts}
            </Form.Control>
          </Col>
          <Col>
            <Form.Range min="0" max="100" defaultValue="100" ref={goRef} />
          </Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label column sm={3}>
            Blue
          </Form.Label>
          <Col>
            <Form.Control
              as="select"
              defaultValue={selectedMaps[2].xrfFluorescenceMappingId}
              ref={bRef}
            >
              {opts}
            </Form.Control>
          </Col>
          <Col>
            <Form.Range min="0" max="100" defaultValue="100" ref={boRef} />
          </Col>
        </Form.Group>
      </Form>
      <div className="d-grid">
        <Button
          className="mt-1"
          disabled={pending}
          onClick={() => createComposite()}
        >
          Create
        </Button>
      </div>
    </>
  );
}

export default function CreateCompositeButton(props: ICreateComposite) {
  return (
    <ModalButton
      title="Create Composite Map"
      disabled={props.mapIds.length !== 3}
      buttonTitle={
        <>
          <Image /> Create Composite
        </>
      }
    >
      {({ hideModal }) => (
        <CreateComposite onHide={() => hideModal()} {...props} />
      )}
    </ModalButton>
  );
}
