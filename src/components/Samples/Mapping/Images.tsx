import { useState, useEffect, useMemo, useCallback, useRef } from 'react';
import { useController, useSuspense } from '@data-client/react';
import { Image as KonvaImage } from 'react-konva';
import Konva from 'konva';
import { debounce } from 'lodash';

import { getXHRBlob } from 'api/resources/XHRFile';
import { SampleImage } from 'models/SampleImage';
import config from 'config/config';
import { useSearchParamsObj } from 'hooks/useSearchParamsObj';
import { useHideImages } from 'hooks/useHideImages';
import { SampleImageResource } from 'api/resources/SampleImage';

export async function awaitImage(src: string): Promise<HTMLImageElement> {
  return new Promise((resolve, reject) => {
    const imageElement = new Image();
    imageElement.onload = () => resolve(imageElement);
    imageElement.onerror = () => reject(imageElement);
    imageElement.src = src;
  });
}

// Coordinates in html5 canvas space are x, -y, and not x, y as you
// might expect (!)
export function toCanvasCoordinates({ x, y }: { x: number; y: number }): {
  x: number;
  y: number;
} {
  return {
    x,
    y: -y,
  };
}

function KonvaSampleImage({
  image,
  setLoaded,
  opacity,
  scaleFactor,
  xy,
}: {
  image: SampleImage;
  setLoaded: (id: number, percent: number) => void;
  opacity: number;
  scaleFactor: number;
  xy: { x?: number; y?: number };
}) {
  const { fetch } = useController();
  const [imageElement, setImageElement] = useState<HTMLImageElement>();
  const [loading, setLoading] = useState<boolean>(false);
  const imageRef = useRef<Konva.Image>(null);
  const [visible, setVisible] = useState<boolean>(true);
  const [scale, setScale] = useState<number>(0);
  const { toggleImage } = useHideImages();
  const debouncedSetScale = useCallback(debounce(setScale, 250), [setScale]);

  const imageWidth = image._metadata.width ?? 0;
  const imageHeight = image._metadata.height ?? 0;
  const longestEdge = Math.max(imageWidth, imageHeight);

  useEffect(() => {
    const sizeOnScreen = Math.abs(
      scaleFactor * image.micronsPerPixelX * 1e3 * longestEdge
    );
    let scale = Math.max(
      1,
      Math.min(0.5 * Math.round(longestEdge / sizeOnScreen / 0.5), 8)
    );
    if (scale > 2) scale = Math.round(scale);
    debouncedSetScale(scale);
    if (imageRef.current) {
      setVisible(imageRef.current.isClientRectOnScreen());
    }
  }, [scaleFactor, xy]);

  useEffect(() => {
    if (!scale || !visible) return;
    setLoaded(image.blSampleImageId, 0);
    setLoading(true);
    async function load() {
      const imageBlob = await fetch(getXHRBlob, {
        src:
          scale > 1
            ? `${config.host}${image._metadata.url}?maxSize=${Math.round(
                (1 / scale) * longestEdge
              )}`
            : `${config.host}${image._metadata.url}`,
        // @ts-expect-error
        progress: (percent) => setLoaded(image.blSampleImageId, percent),
      });
      const imageData = await awaitImage(imageBlob);
      setImageElement(imageData);
      setLoaded(image.blSampleImageId, 100);
      setLoading(false);
    }
    load();
  }, [
    image._metadata.url,
    image.blSampleImageId,
    fetch,
    setLoaded,
    scale,
    visible,
  ]);

  if (!imageElement) return null;

  return (
    <KonvaImage
      ref={imageRef}
      key={image.blSampleImageId}
      image={imageElement}
      x={
        image.offsetX -
        (image.micronsPerPixelX * 1e3 * imageElement.width * scale) / 2
      }
      y={
        image.offsetY -
        (image.micronsPerPixelY * -1e3 * imageElement.height * scale) / 2
      }
      scaleX={image.micronsPerPixelX * 1e3 * scale}
      scaleY={image.micronsPerPixelY * -1e3 * scale}
      opacity={loading ? 0 : opacity}
      visible={visible}
      onDblClick={() => toggleImage(image.blSampleImageId)}
    />
  );
}

export default function Images({
  blSampleId,
  setLoadingMessage,
  scaleFactor,
  xy,
}: {
  blSampleId: number;
  setLoadingMessage: (message: string) => void;
  scaleFactor: number;
  xy: { x?: number; y?: number };
}) {
  const images = useSuspense(SampleImageResource.getList, {
    blSampleId,
    limit: 9999,
  });
  const { hiddenImages } = useSearchParamsObj();
  const hiddenImagesArr = hiddenImages
    ? hiddenImages
        .split(',')
        .map((blSampleImageId) => parseInt(blSampleImageId))
    : [];
  const setLoadedCount = useState<Record<number, number>>({})[1];
  const filteredImages = useMemo(() => {
    setLoadedCount({});
    return images.results.filter(() => true);
  }, [images, setLoadedCount]);

  const setLoaded = useCallback(
    (blSampleImageId: number, percent: number, revoke?: boolean) => {
      setLoadedCount((prevCount) => {
        const newCount = { ...prevCount };
        newCount[blSampleImageId] = percent;
        const finished = Object.entries(newCount)
          .filter(([key, value]) => value === 100)
          .map(([key, value]) => key);
        const loading = Object.entries(newCount)
          .filter(([key, value]) => value < 100)
          .map(([key, value]) => `${value}%`)
          .join(', ');
        setLoadingMessage(
          filteredImages.length === finished.length
            ? ''
            : `Loaded ${finished.length}/${filteredImages.length} images (${loading})`
        );
        return newCount;
      });
    },

    [setLoadedCount, filteredImages.length, setLoadingMessage]
  );

  return (
    <>
      {filteredImages.map((image) => (
        <KonvaSampleImage
          key={image.blSampleImageId}
          image={image}
          setLoaded={setLoaded}
          opacity={
            hiddenImagesArr.indexOf(image.blSampleImageId) === -1 ? 1 : 0
          }
          scaleFactor={scaleFactor}
          xy={xy}
        />
      ))}
    </>
  );
}
