import { useController } from '@data-client/react';
import { Map } from 'react-bootstrap-icons';

import Form from 'components/Common/Form';
import { ModalButton } from 'components/Common/Modal';
import { MapResource } from 'api/resources/Map';
import { ScalarMapEndpoint } from 'api/resources/ScalarMap';

interface ButtonProps {
  dataCollectionId: number;
}

export default function CreateScalarMapButton(props: ButtonProps) {
  return (
    <ModalButton title="Add Scalar Map" buttonTitle={<Map />}>
      {({ hideModal }) => (
        <CreateScalarMap onHide={() => hideModal()} {...props} />
      )}
    </ModalButton>
  );
}

interface Props extends ButtonProps {
  onHide?: () => void;
}

function CreateScalarMap(props: Props) {
  const { dataCollectionId, onHide } = props;
  const { invalidateAll } = useController();

  function success() {
    invalidateAll(MapResource.getList);
    onHide && onHide();
  }

  const uiSchema = {
    dataCollectionId: { 'ui:classNames': 'hidden-row', 'ui:widget': 'hidden' },
    scalar: {
      'ui:widget': 'remoteSelectFromH5',
      'ui:options': {
        source: 'h5',
        shape: 1,
        dataCollectionId: props.dataCollectionId,
      },
    },
  };

  return (
    <Form
      schemaName="ScalarMapCreate"
      uiSchema={uiSchema}
      resource={ScalarMapEndpoint}
      initialFormData={{
        dataCollectionId,
      }}
      onSuccess={() => success()}
    />
  );
}
