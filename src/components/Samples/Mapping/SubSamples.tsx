import React from 'react';
import {
  Rect,
  Text,
  Line,
  Arrow,
  // Group
} from 'react-konva';

import { SubSample } from 'models/SubSample';
import { DataCollection, Event } from 'models/Event';
import { range } from 'lodash';
import { getColors } from 'utils/colours';

interface ICross {
  x: number | undefined;
  y: number | undefined;
  strokeWidth: number;
  fill: string;
}

function Cross({ x, y, strokeWidth, fill }: ICross) {
  const size = 15 * strokeWidth;
  if (!x || !y) return null;
  return (
    // <Group
    //   x={x}
    //   y={y}
    //   strokeWidth={strokeWidth}
    //   scaleX={strokeWidth}
    //   scaleY={strokeWidth}
    //   fill={fill}
    // >
    <>
      <Line
        strokeWidth={strokeWidth}
        stroke={fill}
        points={[x - size, y, x + size, y]}
      />
      <Line
        strokeWidth={strokeWidth}
        points={[x, y - size, x, y + size]}
        stroke={fill}
      />
    </>
    // </Group>
  );
}

const fontSize = 16;

export default function SubSamples({
  subsamples,
  subSampleDataCollections,
  scaleFactor,
  selectedSubSample,
  showPOI,
}: {
  subsamples: SubSample[];
  subSampleDataCollections: Event[];
  scaleFactor: number;
  selectedSubSample: number | undefined;
  showPOI: boolean;
}) {
  return (
    <>
      {subsamples.map((subsample) => {
        const strokeWidth = Math.max(1 / scaleFactor, 1);
        const fontScale = strokeWidth;
        const textOffset = 5 * strokeWidth;
        if (subsample.type === 'roi') {
          const width =
            (subsample.Position1 &&
              subsample.Position2 &&
              subsample.Position2?.posX - subsample.Position1?.posX) ||
            100000;
          const height =
            (subsample.Position1 &&
              subsample.Position2 &&
              subsample.Position2?.posY - subsample.Position1?.posY) ||
            100000;

          return (
            <React.Fragment key={subsample.blSubSampleId}>
              <Rect
                key="region"
                x={subsample.Position1?.posX}
                y={subsample.Position1?.posY}
                width={width}
                height={height}
                stroke={
                  subsample.blSubSampleId === selectedSubSample
                    ? 'red'
                    : 'purple'
                }
                strokeWidth={strokeWidth}
                listening={false}
              />
              <Text
                key="text"
                text={`${subsample.blSubSampleId}`}
                stroke={
                  subsample.blSubSampleId === selectedSubSample
                    ? 'red'
                    : 'purple'
                }
                fontSize={fontSize}
                scaleX={fontScale}
                scaleY={fontScale}
                x={(subsample.Position2?.posX || 0) + textOffset}
                y={(subsample.Position2?.posY || 0) + textOffset}
              />
            </React.Fragment>
          );
        }

        if (subsample.type === 'poi' && showPOI) {
          return (
            <React.Fragment key={subsample.blSubSampleId}>
              <Cross
                key="point"
                x={subsample.Position1?.posX}
                y={subsample.Position1?.posY}
                strokeWidth={strokeWidth}
                fill={
                  subsample.blSubSampleId === selectedSubSample
                    ? 'red'
                    : 'green'
                }
              />
              <Text
                key="text"
                text={`${subsample.blSubSampleId}`}
                stroke={
                  subsample.blSubSampleId === selectedSubSample
                    ? 'red'
                    : 'green'
                }
                fontSize={fontSize}
                scaleX={fontScale}
                scaleY={fontScale}
                x={(subsample.Position1?.posX || 0) + textOffset}
                y={(subsample.Position1?.posY || 0) + textOffset}
              />
            </React.Fragment>
          );
        }

        if (subsample.type === 'loi') {
          const dcs = subSampleDataCollections.filter((event) => {
            const item = event.Item as DataCollection;
            return item.BLSubSample?.blSubSampleId === subsample.blSubSampleId;
          });
          const dcColors = getColors(dcs.length);
          return (
            <React.Fragment key={subsample.blSubSampleId}>
              <Arrow
                key="line"
                points={[
                  subsample.Position1?.posX || 0,
                  subsample.Position1?.posY || 0,
                  subsample.Position2?.posX || 10,
                  subsample.Position2?.posY || 10,
                ]}
                stroke={
                  subsample.blSubSampleId === selectedSubSample
                    ? 'red'
                    : 'purple'
                }
                fill={
                  subsample.blSubSampleId === selectedSubSample
                    ? 'red'
                    : 'purple'
                }
                pointerWidth={10 * fontScale}
                pointerLength={10 * fontScale}
                strokeWidth={strokeWidth}
                listening={false}
              />
              <Text
                key="text"
                text={`${subsample.blSubSampleId}`}
                stroke={
                  subsample.blSubSampleId === selectedSubSample
                    ? 'red'
                    : 'purple'
                }
                fontSize={fontSize}
                scaleX={fontScale}
                scaleY={fontScale}
                x={(subsample.Position2?.posX || 0) + textOffset}
                y={(subsample.Position2?.posY || 0) + textOffset}
              />
              {dcs.map((dc, dcCount) => {
                const item = dc.Item as DataCollection;
                if (!item.GridInfo?.length) return null;
                return (
                  <>
                    {range(item.GridInfo?.[0].steps_x || 1).map((step) => (
                      <Cross
                        key={`${dc.id}-${step}`}
                        x={
                          (subsample.Position1?.posX || 0) +
                          ((item.GridInfo?.[0].dx_mm || 0) * step) / 1e-6
                        }
                        y={
                          (subsample.Position1?.posY || 0) +
                          ((item.GridInfo?.[0].dy_mm || 0) * step) / 1e-6
                        }
                        strokeWidth={strokeWidth}
                        fill={dcColors[dcCount]}
                      />
                    ))}
                  </>
                );
              })}
            </React.Fragment>
          );
        }

        return null;
      })}
    </>
  );
}
