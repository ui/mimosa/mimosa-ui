import { useSuspense } from '@data-client/react';

import { SubSampleResource } from 'api/resources/SubSample';
import Table from 'components/Layout/Table';
import { tagCell } from 'components/Layout/TableCells';
import { DCTypes } from 'components/Samples/SampleStatus';
import { SubSample } from 'models/SubSample';
import { usePaging } from 'hooks/usePaging';
import SubSampleType from 'components/Samples/SubSampleType';
import ROIManager from './ROIManager';
import RegenerateMapsButton from './RegenerateMaps';

export default function SubSampleList({
  blSampleId,
  selectedSubSample,
  selectSubSample,
}: {
  blSampleId: number;
  selectedSubSample: number | undefined;
  selectSubSample: (blSubSampleId: number) => void;
}) {
  const { skip, limit } = usePaging(10, 0, 'subsample');
  const subsamples = useSuspense(SubSampleResource.getList, {
    blSampleId,
    skip,
    limit,
  });

  const onRowClick = (row: SubSample) => {
    selectSubSample(row.blSubSampleId);
  };

  return (
    <>
      <div className="text-end">
        <RegenerateMapsButton blSampleId={blSampleId} />
        <ROIManager blSampleId={blSampleId} />
      </div>
      <Table
        urlPrefix="subsample"
        keyId="blSubSampleId"
        results={subsamples.results}
        onRowClick={onRowClick}
        rowClasses={(row: SubSample) =>
          row.blSubSampleId === selectedSubSample ? 'table-active' : undefined
        }
        paginator={{
          total: subsamples.total,
          skip: subsamples.skip,
          limit: subsamples.limit,
        }}
        columns={[
          { label: '#', key: 'blSubSampleId', className: 'text-break' },
          {
            label: 'Type',
            key: 'type',
            className: 'text-nowrap',
            formatter: (row) => <SubSampleType type={row.type} />,
          },
          {
            label: 'Tags',
            key: 'extraMetadata',
            formatter: tagCell,
          },
          {
            label: 'Data',
            key: '_metadata.datacollections',
            className: 'text-nowrap',
          },
          {
            label: 'DC Types',
            key: '_metadata.types',
            formatter: (row) => <DCTypes {...row} />,
            className: 'text-nowrap',
          },
        ]}
        emptyText="No sub samples yet"
      />
    </>
  );
}
