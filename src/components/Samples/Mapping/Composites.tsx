import { useState, useEffect, useCallback, useMemo } from 'react';
import { useController, useSuspense } from '@data-client/react';
import { Image as KonvaImage } from 'react-konva';
import { getXHRBlob } from 'api/resources/XHRFile';
import { SubSample } from 'models/SubSample';
import { awaitImage } from './Images';
import config from 'config/config';
import { CompositeMap } from 'models/CompositeMap';
import { CompositeMapResource } from 'api/resources/CompositeMap';

function KonvaCompositeMapImage({
  map,
  subsamples,
  mapOpacity,
  setLoaded,
}: {
  map: CompositeMap;
  subsamples: SubSample[];
  mapOpacity: number;
  setLoaded: (id: number) => void;
}) {
  const { fetch } = useController();
  const [imageElement, setImageElement] = useState<HTMLImageElement>();

  useEffect(() => {
    async function load() {
      const imageData = await fetch(getXHRBlob, {
        src: `${config.host}${map._metadata.url}`,
      });
      const image = await awaitImage(imageData);
      setImageElement(image);
      setLoaded(map.xfeFluorescenceCompositeId);
    }
    load();
  }, [
    map.rOpacity,
    map.gOpacity,
    map.bOpacity,
    fetch,
    map._metadata.url,
    map.xfeFluorescenceCompositeId,
    setLoaded,
  ]);

  const subsample = subsamples.filter(
    (subsample) => subsample.blSubSampleId === map._metadata.blSubSampleId
  )[0];
  if (!subsample) return null;

  const width =
    (subsample.Position1 &&
      subsample.Position2 &&
      subsample.Position2?.posX - subsample.Position1?.posX) ||
    100000;
  const height =
    (subsample.Position1 &&
      subsample.Position2 &&
      subsample.Position2?.posY - subsample.Position1?.posY) ||
    100000;

  return (
    <KonvaImage
      key={map.xfeFluorescenceCompositeId}
      image={imageElement}
      x={subsample.Position1?.posX}
      y={subsample.Position1?.posY}
      scaleX={width / map.XRFFluorescenceMapping.GridInfo.steps_x}
      scaleY={height / map.XRFFluorescenceMapping.GridInfo.steps_y}
      opacity={mapOpacity / 100}
      attrs={{
        xfeFluorescenceCompositeId: map.xfeFluorescenceCompositeId,
      }}
    />
  );
}

export default function Maps({
  blSampleId,
  subsamples,
  mapOpacity,
  setLoadingMessage,
}: {
  blSampleId: number;
  subsamples: SubSample[];
  mapOpacity: number;
  setLoadingMessage: (message: string) => void;
}) {
  const composites = useSuspense(CompositeMapResource.getList, {
    blSampleId,
    limit: 9999,
  });

  const setLoadedCount = useState<number[]>([])[1];
  const filteredMaps = useMemo(() => {
    setLoadedCount([]);
    const mapForSubSample: Record<string, number> = {};
    return composites.results
      .slice()
      .reverse()
      .filter((map) => {
        if (
          map.opacity &&
          map._metadata?.blSubSampleId &&
          !(map._metadata.blSubSampleId in mapForSubSample)
        ) {
          mapForSubSample[map._metadata.blSubSampleId] =
            map.xfeFluorescenceCompositeId;
          return true;
        }
        return false;
      })
      .reverse();
  }, [composites, setLoadedCount]);

  const setLoaded = useCallback(
    (xfeFluorescenceCompositeId: number) => {
      setLoadedCount((prevCount) => {
        const newCount = [...prevCount];
        if (newCount.indexOf(xfeFluorescenceCompositeId) === -1)
          newCount.push(xfeFluorescenceCompositeId);

        setLoadingMessage(
          filteredMaps.length === newCount.length
            ? ''
            : `Loaded ${newCount.length}/${filteredMaps.length} maps`
        );
        return newCount;
      });
    },

    [setLoadedCount, filteredMaps.length, setLoadingMessage]
  );

  return (
    <>
      {filteredMaps.map((map) => (
        <KonvaCompositeMapImage
          key={map.xfeFluorescenceCompositeId}
          map={map}
          subsamples={subsamples}
          mapOpacity={mapOpacity}
          setLoaded={setLoaded}
        />
      ))}
    </>
  );
}
