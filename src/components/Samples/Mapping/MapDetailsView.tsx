import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import { Map } from 'models/Map';
import { useController, useSuspense } from '@data-client/react';
import { MapHistogramResource } from 'api/resources/MapHistogram';
import { Gear } from 'react-bootstrap-icons';
import { Suspense, forwardRef } from 'react';
import Loading from 'components/Loading';
import PlotEnhancer from 'components/Stats/PlotEnhancer';
import { Form } from 'react-bootstrap';
import { MapResource } from 'api/resources/Map';

function MapDetailsInner(map: Map) {
  const controller = useController();
  const histogram = useSuspense(MapHistogramResource.get, {
    xrfFluorescenceMappingId: map.xrfFluorescenceMappingId,
  });

  function onChange(property: string, value: any) {
    controller.fetch(
      MapResource.partialUpdate,
      { xrfFluorescenceMappingId: map.xrfFluorescenceMappingId },
      { [property]: value }
    );
  }

  function onSelected(min: number | null, max: number | null) {
    controller.fetch(
      MapResource.partialUpdate,
      { xrfFluorescenceMappingId: map.xrfFluorescenceMappingId },
      // @ts-expect-error
      { min, max }
    );
  }

  return (
    <>
      <Form.Group>
        <Form.Label>Color Map</Form.Label>
        <Form.Control
          as="select"
          onChange={(e) => onChange('colourMap', e.target.value)}
          value={map.colourMap}
        >
          <option>viridis</option>
          <option>jet</option>
          <option>binary</option>
          <option>gray</option>
        </Form.Control>
      </Form.Group>

      <Form.Group>
        <Form.Label>Scaling</Form.Label>
        <Form.Control
          as="select"
          onChange={(e) => onChange('scale', e.target.value)}
          value={map.scale}
        >
          <option>linear</option>
          <option>logarithmic</option>
        </Form.Control>
      </Form.Group>
      <div style={{ width: 250 }}>
        <PlotEnhancer
          style={{ height: 250 }}
          layout={{
            xaxis: { title: 'Bin' },
            yaxis: { title: 'Count' },
            dragmode: 'select',
            selectdirection: 'h',
            shapes:
              map.min && map.max
                ? [
                    {
                      type: 'rect',
                      xref: 'x',
                      yref: 'paper',
                      x0: map.min,
                      x1: map.max,
                      fillcolor: 'cyan',
                      opacity: 0.2,
                      y0: 0,
                      y1: 1,
                      line: {
                        width: 0,
                      },
                    },
                  ]
                : undefined,
          }}
          onSelected={(e) => e.range && onSelected(e.range.x[0], e.range.x[1])}
          data={[
            {
              type: 'bar',
              x: histogram.bins,
              y: histogram.hist,
              width: histogram.width,
            },
          ]}
        />
      </div>
      <div className="d-grid mt-1">
        <Button
          size="sm"
          onClick={() => onSelected(null, null)}
          disabled={map.min === null && map.max === null}
        >
          Reset Selection
        </Button>
      </div>
    </>
  );
}

const MapDetailsPopover = forwardRef(({ map, ...rest }: { map: Map }, ref) => {
  return (
    <Popover
      id="popover-basic"
      // @ts-expect-error
      ref={ref}
      {...rest}
    >
      <Popover.Header as="h3">Map Details</Popover.Header>
      <Popover.Body>
        <Suspense fallback={<Loading />}>
          <MapDetailsInner {...map} />
        </Suspense>
      </Popover.Body>
    </Popover>
  );
});

MapDetailsPopover.displayName = 'MapDetailsPopover';

export default function MapDetailsView(map: Map) {
  return (
    <OverlayTrigger
      trigger="click"
      placement="left"
      rootClose
      overlay={<MapDetailsPopover map={map} />}
    >
      <Button size="sm" className="me-1">
        <Gear />
      </Button>
    </OverlayTrigger>
  );
}
