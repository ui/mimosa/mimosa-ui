import { useState } from 'react';
import { Line, Text } from 'react-konva';
import { formatEng } from 'utils/numbers';

export function useMeasure(enabled: boolean) {
  const [measuring, setMeasuring] = useState<boolean>(false);
  const [measureStart, setMeasureStart] = useState<number[]>([0, 0]);
  const [measureEnd, setMeasureEnd] = useState<number[]>([0, 0]);

  function startMeasure(event: any) {
    if (!enabled) return;
    const stage = event.target.getStage();
    const oldScale = stage.scaleX();
    const { x: pointerX, y: pointerY } = stage.getPointerPosition();
    const mousePointTo = {
      x: (pointerX - stage.x()) / oldScale,
      y: (pointerY - stage.y()) / oldScale,
    };

    setMeasureStart([mousePointTo.x, mousePointTo.y]);
    setMeasureEnd([mousePointTo.x, mousePointTo.y]);
    setMeasuring(true);
  }

  function doMeasure(event: any) {
    if (!enabled) return;
    const stage = event.target.getStage();
    const oldScale = stage.scaleX();
    const { x: pointerX, y: pointerY } = stage.getPointerPosition();
    const mousePointTo = {
      x: (pointerX - stage.x()) / oldScale,
      y: (pointerY - stage.y()) / oldScale,
    };

    if (measuring) {
      setMeasureEnd([mousePointTo.x, mousePointTo.y]);
    }
  }

  function stopMeasure() {
    if (!enabled) return;
    setMeasuring(false);
  }

  return {
    setMeasuring,
    startMeasure,
    doMeasure,
    stopMeasure,
    measureStart,
    measureEnd,
    measuring,
  };
}

interface IMeasure {
  measureStart: number[];
  measureEnd: number[];
  measuring: boolean;
  scaleFactor: number;
}

export default function Measure(props: IMeasure) {
  const { measuring, measureStart, measureEnd, scaleFactor } = props;
  const strokeWidth = Math.max(1 / scaleFactor, 1);
  const fontScale = strokeWidth;
  const textOffset = 10 * strokeWidth;
  const fontSize = 14;

  const length = Math.sqrt(
    Math.pow(measureEnd[0] - measureStart[0], 2) +
      Math.pow(measureEnd[1] - measureStart[1], 2)
  );

  const size = formatEng(length * 1e-9);

  return (
    <>
      {measuring && (
        <>
          <Line
            key="measure"
            points={[
              measureStart[0],
              measureStart[1],
              measureEnd[0],
              measureEnd[1],
            ]}
            stroke="orange"
            strokeWidth={1 / scaleFactor}
          />
          {length > 0 && (
            <Text
              key="text"
              text={`${size.scalar.toFixed(1)}${size.prefix}m`}
              stroke="orange"
              fontSize={fontSize}
              scaleX={fontScale}
              scaleY={fontScale}
              x={measureEnd[0] + textOffset}
              y={measureEnd[1] - textOffset}
            />
          )}
        </>
      )}
    </>
  );
}
