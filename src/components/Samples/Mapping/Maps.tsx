import { useState, useEffect, useCallback, useMemo } from 'react';
import { useController, useSuspense } from '@data-client/react';
import { Image as KonvaImage } from 'react-konva';
import { getXHRBlob } from 'api/resources/XHRFile';
import { SubSample } from 'models/SubSample';
import { Map } from 'models/Map';
import { awaitImage } from './Images';
import { KonvaEventObject } from 'konva/lib/Node';
import config from 'config/config';
import { MapResource } from 'api/resources/Map';

export function getROIName(map: any): string {
  return map.XRFFluorescenceMappingROI.scalar
    ? map.XRFFluorescenceMappingROI.scalar
    : `${map.XRFFluorescenceMappingROI.element}-${map.XRFFluorescenceMappingROI.edge}`;
}

export interface SelectPoint {
  dataCollectionId: number;
  selectedPoint: number;
  blSubSampleId: number;
}

function KonvaMapImage({
  map,
  subsamples,
  mapOpacity,
  selectPoint,
  setLoaded,
}: {
  map: Map;
  subsamples: SubSample[];
  mapOpacity: number;
  selectPoint?: (point: SelectPoint) => void;
  setLoaded: (id: number) => void;
}) {
  const { fetch } = useController();
  const [imageElement, setImageElement] = useState<HTMLImageElement>();

  const setCursor = useCallback(
    (evt: KonvaEventObject<MouseEvent>, pointerType: string = 'default') => {
      if (evt.target.parent?.parent) {
        // @ts-expect-error
        evt.target.parent.parent.content.style.cursor = pointerType;
      }
    },
    []
  );

  const onClick = useCallback(
    (evt: any) => {
      const rel = evt.target.getRelativePointerPosition();
      const x = Math.floor(rel.x);
      const y = Math.floor(rel.y);

      // point is 1-offset
      const point =
        x + y * (map.GridInfo.steps_x ? map.GridInfo.steps_x : 1) + 1;
      console.log(
        'dataCollectionId',
        map._metadata.dataCollectionId,
        'point',
        point
      );
      if (selectPoint && map._metadata.dataCollectionId)
        selectPoint({
          dataCollectionId: map._metadata.dataCollectionId,
          selectedPoint: point,
          blSubSampleId: map._metadata.blSubSampleId || 0,
        });
    },
    [
      selectPoint,
      map.GridInfo.steps_x,
      map._metadata.dataCollectionId,
      map._metadata.blSubSampleId,
    ]
  );

  useEffect(() => {
    async function load() {
      const imageData = await fetch(getXHRBlob, {
        src: `${config.host}${map._metadata.url}`,
      });
      const image = await awaitImage(imageData);
      setImageElement(image);
      setLoaded(map.xrfFluorescenceMappingId);
    }
    load();
  }, [
    map.colourMap,
    map.max,
    map.min,
    map.scale,
    fetch,
    map._metadata.url,
    map.xrfFluorescenceMappingId,
    setLoaded,
  ]);

  const subsample = subsamples.filter(
    (subsample) => subsample.blSubSampleId === map._metadata.blSubSampleId
  )[0];
  if (!subsample) return null;

  const width =
    (subsample.Position1 &&
      subsample.Position2 &&
      subsample.Position2?.posX - subsample.Position1?.posX) ||
    100000;
  const height =
    (subsample.Position1 &&
      subsample.Position2 &&
      subsample.Position2?.posY - subsample.Position1?.posY) ||
    100000;

  return (
    <KonvaImage
      key={map.xrfFluorescenceMappingId}
      image={imageElement}
      x={subsample.Position1?.posX}
      y={subsample.Position1?.posY}
      scaleX={width / map.GridInfo.steps_x}
      scaleY={height / map.GridInfo.steps_y}
      opacity={mapOpacity / 100}
      onClick={onClick}
      onTouchStart={onClick}
      attrs={{
        xrfFluorescenceMappingId: map.xrfFluorescenceMappingId,
      }}
      onMouseEnter={(evt) => setCursor(evt, 'pointer')}
      onMouseLeave={(evt) => setCursor(evt)}
    />
  );
}

export default function Maps({
  blSampleId,
  subsamples,
  mapOpacity,
  setLoadingMessage,
  selectPoint,
}: {
  blSampleId: number;
  subsamples: SubSample[];
  mapOpacity: number;
  setLoadingMessage: (message: string) => void;
  selectPoint?: (point: SelectPoint) => void;
}) {
  const maps = useSuspense(MapResource.getList, {
    blSampleId,
    limit: 9999,
  });
  const setLoadedCount = useState<number[]>([])[1];
  const filteredMaps = useMemo(() => {
    setLoadedCount([]);
    const mapForSubSample: Record<string, number> = {};
    return maps.results
      .slice()
      .reverse()
      .filter((map) => {
        if (
          map.opacity &&
          map._metadata?.blSubSampleId &&
          !(map._metadata.blSubSampleId in mapForSubSample)
        ) {
          mapForSubSample[map._metadata.blSubSampleId] =
            map.xrfFluorescenceMappingId;
          return true;
        }
        return false;
      })
      .reverse();
  }, [maps, setLoadedCount]);

  const setLoaded = useCallback(
    (xrfFluorescenceMappingId: number) => {
      setLoadedCount((prevCount) => {
        const newCount = [...prevCount];
        if (newCount.indexOf(xrfFluorescenceMappingId) === -1)
          newCount.push(xrfFluorescenceMappingId);

        setLoadingMessage(
          filteredMaps.length === newCount.length
            ? ''
            : `Loaded ${newCount.length}/${filteredMaps.length} maps`
        );
        return newCount;
      });
    },

    [setLoadedCount, filteredMaps.length, setLoadingMessage]
  );

  return (
    <>
      {filteredMaps.map((map) => (
        <KonvaMapImage
          key={map.xrfFluorescenceMappingId}
          map={map}
          subsamples={subsamples}
          mapOpacity={mapOpacity}
          setLoaded={setLoaded}
          selectPoint={selectPoint}
        />
      ))}
    </>
  );
}
