import { Suspense } from 'react';
import { useSuspense, useController } from '@data-client/react';
import { GraphUp, Plus, Trash } from 'react-bootstrap-icons';
import { Button } from 'react-bootstrap';

import { MapROIResource } from 'api/resources/MapROI';
import Table from 'components/Layout/Table';
import { ModalButton } from 'components/Common/Modal';
import Form from 'components/Common/Form';
import PeriodicTable from './PeriodicTable';
import Confirm from 'components/Common/Confirm';

interface IROIManager {
  blSampleId: number;
}

interface IAddROI extends IROIManager {
  onHide: () => void;
}

function AddROI(props: IAddROI) {
  const { blSampleId, onHide } = props;
  const { invalidateAll } = useController();

  function success() {
    invalidateAll(MapROIResource.getList);
    onHide && onHide();
  }

  const uiSchema = {
    blSampleId: { 'ui:classNames': 'hidden-row', 'ui:widget': 'hidden' },
  };

  return (
    <Form
      schemaName="MapROICreate"
      uiSchema={uiSchema}
      resource={MapROIResource}
      initialFormData={{
        blSampleId,
      }}
      onSuccess={() => success()}
    />
  );
}

function AddROIButton(props: IROIManager) {
  return (
    <ModalButton
      title="Add ROI"
      showClose
      buttonTitle={
        <>
          <Plus className="me-1" />
          Add
        </>
      }
    >
      {({ hideModal }) => <AddROI onHide={() => hideModal()} {...props} />}
    </ModalButton>
  );
}

function ROIList(props: IROIManager) {
  const { blSampleId } = props;
  const controller = useController();
  const rois = useSuspense(MapROIResource.getList, {
    blSampleId,
  });

  function deleteMapROI(xrfFluorescenceMappingROIId: number) {
    return controller.fetch(MapROIResource.delete, {
      xrfFluorescenceMappingROIId,
    });
  }

  return (
    <>
      <PeriodicTable blSampleId={blSampleId} />
      <div className="text-end mt-2">
        <AddROIButton blSampleId={blSampleId} />
      </div>
      <Table
        keyId="xrfFluorescenceMappingROIId"
        results={rois.results}
        paginator={{
          total: rois.total,
          skip: rois.skip,
          limit: rois.limit,
        }}
        columns={[
          {
            label: '#',
            key: 'xrfFluorescenceMappingROIId',
            className: 'text-break',
          },
          {
            label: 'Name',
            key: 'type',
            className: 'text-nowrap',
            formatter: (row) => `${row.element}-${row.edge}`,
          },
          {
            label: 'Start (eV)',
            key: 'startEnergy',
            className: 'text-nowrap',
          },
          {
            label: 'End (eV)',
            key: 'endEnergy',
            className: 'text-nowrap',
          },
          {
            label: '# Maps',
            key: '_metadata.maps',
            className: 'text-nowrap',
          },
          {
            label: '',
            key: 'opacity',
            className: 'text-nowrap text-end',
            formatter: (row) => (
              <>
                {row._metadata.maps === 0 && (
                  <Confirm
                    action={() => deleteMapROI(row.xrfFluorescenceMappingROIId)}
                    message="Are you sure you want to remove this map ROI?"
                    title="Delete Map ROI"
                  >
                    {({ showConfirm }) => (
                      <Button
                        variant="danger"
                        size="sm"
                        onClick={() => showConfirm()}
                      >
                        <Trash />
                      </Button>
                    )}
                  </Confirm>
                )}
              </>
            ),
          },
        ]}
        emptyText="No ROIs yet"
      />
    </>
  );
}

interface IROIManager {
  blSampleId: number;
}

function ROIManager(props: IROIManager) {
  const { blSampleId } = props;
  return (
    <Suspense>
      <ROIList blSampleId={blSampleId} />
    </Suspense>
  );
}

export default function ROIManagerButton(props: IROIManager) {
  return (
    <ModalButton
      title="Spectrum ROI Manager"
      showClose
      buttonTitle={
        <>
          <GraphUp className="me-1" />
          Spectrum ROIs
        </>
      }
    >
      {() => <ROIManager {...props} />}
    </ModalButton>
  );
}
