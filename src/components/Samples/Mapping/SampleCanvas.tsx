import { useState, useRef, useEffect, useCallback, Suspense } from 'react';
import { useSuspense } from '@data-client/react';
import { Stage, Layer, Rect, Text } from 'react-konva';

import Konva from 'konva';
import {
  Button,
  Container,
  Form,
  Navbar,
  ToggleButton,
  Spinner,
} from 'react-bootstrap';
import {
  ArrowsMove,
  Bullseye,
  ZoomOut,
  ZoomIn,
  Rulers,
} from 'react-bootstrap-icons';

import { SubSampleResource } from 'api/resources/SubSample';
import { EventResource } from 'api/resources/Event';
import { SubSample } from 'models/SubSample';
import { Event } from 'models/Event';
import Images from './Images';
import Maps, { SelectPoint } from './Maps';
import Composites from './Composites';

import SubSamples from './SubSamples';
import DataPointModal from './DataPointModal';
import SelectImages from './SelectImages';
import Measure, { useMeasure } from './Measure';

function KonvaLoading({
  bounds,
  scaleFactor,
  message,
  setPending,
  hidden,
}: {
  bounds: number[];
  scaleFactor: number;
  message?: string;
  setPending?: (pending: boolean) => void;
  hidden?: boolean;
}) {
  useEffect(() => {
    setPending?.(true);
    return () => {
      setPending?.(false);
    };
  }, []);
  if (hidden) return null;

  const strokeWidth = Math.max(1 / scaleFactor, 1);
  const fontScale = strokeWidth;
  return (
    <Text
      key="text"
      text={`Loading ${message}`}
      fontSize={16}
      scaleX={fontScale}
      scaleY={fontScale}
      x={bounds[0]}
      y={bounds[1]}
    />
  );
}

function SampleCanvasMain({
  blSampleId,
  subsamples,
  subSampleDataCollections,
  selectedSubSample,
  showPOI,
  mapOpacity,
  enableCursor,
  enableMeasure,
  selectSubSample,
}: {
  blSampleId: number;
  subsamples: SubSample[];
  subSampleDataCollections: Event[];
  showPOI: boolean;
  mapOpacity: number;
  enableCursor: boolean;
  enableMeasure: boolean;
  selectedSubSample: number | undefined;
  selectSubSample: (blSubSampleId: number) => void;
}) {
  const wrapRef = useRef<HTMLDivElement>(null);
  const [center, setCenter] = useState<number[]>([0, 0, 0, 0]);
  const [bounds, setBounds] = useState<number[]>([0, 0, 0, 0]);
  const [scaleFactor, setScaleFactor] = useState<number>(1);
  const [xy, setXY] = useState<{ x?: number; y?: number }>({});
  const [parentSize, setParentSize] = useState<number[]>([1000, 800]);
  const [imageLoadingMessage, setImageLoadingMessage] = useState<
    string | JSX.Element
  >('');
  const [mapLoadingMessage, setMapLoadingMessage] = useState<
    string | JSX.Element
  >('');

  const [showModal, setShowModal] = useState<boolean>(false);
  const [selectedPoint, setSelectedPoint] = useState<SelectPoint | null>(null);
  const {
    startMeasure,
    doMeasure,
    stopMeasure,
    measureStart,
    measureEnd,
    measuring,
  } = useMeasure(enableMeasure);

  const stageRef = useRef<Konva.Stage>(null);

  useEffect(() => {
    if (wrapRef.current) {
      setParentSize([
        wrapRef.current.clientWidth,
        wrapRef.current.clientHeight,
      ]);
    }
  }, [wrapRef]);

  const zoomStage = useCallback(
    (event: any) => {
      event.evt.preventDefault();
      const delta =
        event.evt.deltaMode === 1 ? event.evt.deltaY * 18 : event.evt.deltaY;
      if (stageRef.current !== null) {
        const stage = stageRef.current;
        const oldScale = stage.scaleX();
        // @ts-expect-error
        const { x: pointerX, y: pointerY } = stage.getPointerPosition();
        const mousePointTo = {
          x: (pointerX - stage.x()) / oldScale,
          y: (pointerY - stage.y()) / oldScale,
        };
        const newScale = (-1 * delta) / (400 / oldScale) + oldScale;
        stage.scale({ x: newScale, y: newScale });
        setScaleFactor(newScale);
        const newPos = {
          x: pointerX - mousePointTo.x * newScale,
          y: pointerY - mousePointTo.y * newScale,
        };
        stage.position(newPos);
        stage.batchDraw();
      }
    },
    [stageRef]
  );

  const zoom = useCallback(
    (out: boolean = false) => {
      if (stageRef.current !== null) {
        const stage = stageRef.current;
        const oldScale = stage.scaleX();
        const newScale = (out ? -50 : 50) / (400 / oldScale) + oldScale;
        stage.scale({ x: newScale, y: newScale });
        setScaleFactor(newScale);
        stage.batchDraw();
      }
    },
    [stageRef]
  );

  const centerStage = useCallback(() => {
    const minXs = subsamples.map((subsample) => subsample.Position1?.posX || 0);
    const minYs = subsamples.map((subsample) => subsample.Position1?.posY || 0);

    const maxXs = subsamples.map(
      (subsample) =>
        (subsample.Position2?.posX
          ? subsample.Position2?.posX
          : subsample.Position1?.posX) || 0
    );
    const maxYs = subsamples.map(
      (subsample) =>
        (subsample.Position2?.posY
          ? subsample.Position2?.posY
          : subsample.Position1?.posY) || 0
    );

    const minX = minXs.length ? Math.min(...minXs) : 0;
    const minY = minYs.length ? Math.min(...minYs) : 0;
    const maxX = maxXs.length ? Math.max(...maxXs) : 0;
    const maxY = maxYs.length ? Math.max(...maxYs) : 0;

    console.log('constraints', minX, minY, maxX, maxY);
    const scaleX = maxX - minX > 0 ? 1024 / (maxX - minX) : 1;
    const scaleY = maxY - minY > 0 ? 1024 / (maxY - minY) : 1;
    const scale = scaleX < scaleY ? scaleX : scaleY;

    const cenX = Math.round((maxX - minX) / 2 + minX);
    const cenY = Math.round((maxY - minY) / 2 + minY);

    console.log('cen', cenX, cenY, 'scale', scaleX, scaleY);
    setCenter([cenX, cenY]);
    setBounds([minX, minY, maxX - minX, maxY - minY]);
    setScaleFactor(scale);

    const stage = stageRef.current;
    if (stage) {
      stage.scale({ x: scale, y: scale });
      stage.position({
        x: -cenX * scale + parentSize[0] / 2,
        y: -cenY * scale + parentSize[1] / 2,
      });
      stage.batchDraw();
    }
  }, [subsamples, parentSize]);

  useEffect(() => {
    centerStage();
  }, [centerStage]);

  const showBounds = false;
  const showCenter = false;

  const selectPoint = useCallback(
    (point: SelectPoint) => {
      if (enableCursor) {
        setSelectedPoint(point);
        setShowModal(true);
      } else {
        selectSubSample(point.blSubSampleId);
      }
    },
    [setSelectedPoint, enableCursor, selectSubSample]
  );

  const onDragMove = useCallback(() => {
    // @ts-expect-error
    setXY({ x: stageRef.current?.x, y: stageRef.current?.y });
  }, []);

  const [lastSeries, setLastSeries] = useState<string>();

  return (
    <div className="stage-wrapper position-relative" ref={wrapRef}>
      {selectedPoint && (
        <DataPointModal
          dataCollectionId={selectedPoint.dataCollectionId}
          selectedPoint={selectedPoint.selectedPoint}
          show={showModal}
          onHide={() => setShowModal(false)}
          setLastSeries={setLastSeries}
          lastSeries={lastSeries}
        />
      )}
      <div
        className="position-absolute loading-message"
        style={{ right: 10, top: 10, zIndex: 100 }}
      >
        {imageLoadingMessage && (
          <span
            className="p-1 rounded me-1 opacity-75"
            style={{ backgroundColor: '#fff', color: '#000' }}
          >
            {imageLoadingMessage}
          </span>
        )}
        {mapLoadingMessage && (
          <span
            className="p-1 rounded me-1 opacity-75"
            style={{ backgroundColor: '#fff', color: '#000' }}
          >
            {mapLoadingMessage}
          </span>
        )}
        <Button onClick={() => zoom(true)} className="me-1">
          <ZoomOut />
        </Button>
        <Button onClick={() => zoom()} className="me-1">
          <ZoomIn />
        </Button>
        <Button onClick={() => centerStage()}>
          <ArrowsMove />
        </Button>
      </div>
      <Stage
        key="canvas"
        onDragMove={onDragMove}
        width={parentSize[0]}
        height={parentSize[1]}
        draggable={!enableMeasure}
        onMouseDown={startMeasure}
        onMouseMove={doMeasure}
        onMouseUp={stopMeasure}
        onWheel={zoomStage}
        ref={stageRef}
        style={{ border: '1px solid #ccc' }}
      >
        <Layer imageSmoothingEnabled={false}>
          <Suspense
            fallback={
              <KonvaLoading
                bounds={bounds}
                scaleFactor={scaleFactor}
                message="Images"
                hidden
                setPending={(pending) =>
                  setMapLoadingMessage(
                    pending ? (
                      <span>
                        <Spinner
                          size="sm"
                          animation="border"
                          role="status"
                          className="me-1"
                        />
                        Loading Image List
                      </span>
                    ) : (
                      ''
                    )
                  )
                }
              />
            }
          >
            <Images
              blSampleId={blSampleId}
              setLoadingMessage={setImageLoadingMessage}
              xy={xy}
              scaleFactor={scaleFactor}
            />
          </Suspense>
          {showBounds && bounds && bounds[0] > 0 && (
            <Rect
              key="bounds"
              x={bounds[0]}
              y={bounds[1]}
              width={bounds[2]}
              height={bounds[3]}
              stroke="blue"
              strokeWidth={1 / scaleFactor}
            ></Rect>
          )}

          {showCenter && center && center[0] > 0 && (
            <Rect
              key="bounds-center"
              x={center[0]}
              y={center[1]}
              width={100}
              height={100}
              stroke="yellow"
              strokeWidth={1 / scaleFactor}
            ></Rect>
          )}

          <Suspense
            fallback={
              <KonvaLoading
                bounds={bounds}
                scaleFactor={scaleFactor}
                message="Maps"
                hidden
                setPending={(pending) =>
                  setMapLoadingMessage(
                    pending ? (
                      <span>
                        <Spinner
                          size="sm"
                          animation="border"
                          role="status"
                          className="me-1"
                        />
                        Loading Map List
                      </span>
                    ) : (
                      ''
                    )
                  )
                }
              />
            }
          >
            <Maps
              blSampleId={blSampleId}
              subsamples={subsamples}
              setLoadingMessage={setMapLoadingMessage}
              mapOpacity={mapOpacity}
              selectPoint={selectPoint}
            />
          </Suspense>

          <Suspense>
            <Composites
              blSampleId={blSampleId}
              subsamples={subsamples}
              setLoadingMessage={setMapLoadingMessage}
              mapOpacity={mapOpacity}
            />
          </Suspense>

          <SubSamples
            subsamples={subsamples}
            subSampleDataCollections={subSampleDataCollections}
            scaleFactor={scaleFactor}
            selectedSubSample={selectedSubSample}
            showPOI={showPOI}
          />
          <Measure
            measuring={measuring}
            measureStart={measureStart}
            measureEnd={measureEnd}
            scaleFactor={scaleFactor}
          />
        </Layer>
      </Stage>
    </div>
  );
}

export default function SampleCanvas({
  blSampleId,
  selectSample,
  selectedSubSample,
  selectSubSample,
}: {
  blSampleId: number;
  selectSample: JSX.Element;
  selectedSubSample: number | undefined;
  selectSubSample: (blSubSampleId: number) => void;
}) {
  const subsamples = useSuspense(SubSampleResource.getList, {
    blSampleId,
    limit: 9999,
  });
  const subSampleDataCollections = useSuspense(EventResource.getList, {
    blSampleId,
    blSubSampleType: 'loi',
    limit: 9999,
  });

  const [showPOI, setShowPOI] = useState<boolean>(true);
  const [mapOpacity, setMapOpacity] = useState<number>(100);
  const [enableCursor, setEnableCursor] = useState<boolean>(false);
  const [enableMeasure, setEnableMeasure] = useState<boolean>(false);

  return (
    <div>
      <Navbar bg="secondary" key="nav" className="samplereview-navbar">
        <Container fluid>
          <Form className="d-flex align-items-center">
            {selectSample}

            <SelectImages blSampleId={blSampleId} />

            <ToggleButton
              id="cursor"
              title="Enable Cursor (Click to show point)"
              value="1"
              type="checkbox"
              className="ms-1"
              checked={enableCursor}
              onClick={() => setEnableCursor(!enableCursor)}
            >
              <Bullseye />
            </ToggleButton>
            <ToggleButton
              id="measure"
              title="Enable Measure"
              value="1"
              type="checkbox"
              className="ms-1"
              checked={enableMeasure}
              onClick={() => setEnableMeasure(!enableMeasure)}
            >
              <Rulers />
            </ToggleButton>
            <Form.Range
              className="ms-2"
              min={0}
              max={100}
              defaultValue={mapOpacity}
              onChange={(evt) => setMapOpacity(parseInt(evt.target.value))}
            />
            <Form.Label className="ms-2 text-nowrap">Map Opacity</Form.Label>
            <Form.Check
              style={{ flex: 1 }}
              className="ms-2 text-nowrap"
              type="switch"
              label="Hide POIs"
              onChange={(e) => setShowPOI(!e.target.checked)}
            />
          </Form>
        </Container>
      </Navbar>
      <SampleCanvasMain
        blSampleId={blSampleId}
        subsamples={subsamples.results}
        subSampleDataCollections={subSampleDataCollections.results}
        showPOI={showPOI}
        mapOpacity={mapOpacity}
        enableCursor={enableCursor}
        enableMeasure={enableMeasure}
        selectedSubSample={selectedSubSample}
        selectSubSample={selectSubSample}
      />
    </div>
  );
}
