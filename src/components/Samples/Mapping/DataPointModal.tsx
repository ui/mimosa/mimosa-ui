import { Suspense } from 'react';
import { useSuspense } from '@data-client/react';

import DataViewer from 'components/Events/DataCollections/Mapping/DataViewer';
import { EventResource } from 'api/resources/Event';
import type { DataCollection } from 'models/Event';
import Metadata from 'components/Events/Metadata';
import Modal from 'components/Common/Modal';

interface IDataPointModal {
  dataCollectionId: number;
  selectedPoint: number;
  show?: boolean;
  onHide?: () => void;
  lastSeries?: string;
  setLastSeries: (series: string) => void;
}

function DataPointModalMain({
  dataCollectionId,
  selectedPoint,
  show,
  onHide,
  lastSeries,
  setLastSeries,
}: IDataPointModal) {
  const events = useSuspense(EventResource.getList, {
    dataCollectionId: dataCollectionId,
  });
  const dataCollection = events.results[0].Item as DataCollection;

  return (
    <Modal
      show={show || false}
      onHide={() => onHide && onHide()}
      title="Data Viewer"
    >
      <Metadata
        properties={[
          { title: 'Point', content: selectedPoint },
          {
            title: 'File',
            content: `${dataCollection.imageDirectory}/${dataCollection.fileTemplate}`,
          },
        ]}
      />
      <Suspense fallback="Loading...">
        {dataCollection && (
          <div style={{ minHeight: '70vh', display: 'flex' }}>
            <DataViewer
              dataCollection={dataCollection}
              selectedPoint={selectedPoint}
              lastSeries={lastSeries}
              setLastSeries={setLastSeries}
            />
          </div>
        )}
      </Suspense>
    </Modal>
  );
}

export default function DataPointModal(props: IDataPointModal) {
  return (
    <Suspense>
      <DataPointModalMain {...props} />
    </Suspense>
  );
}
