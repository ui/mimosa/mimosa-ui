import { useController, useSuspense } from '@data-client/react';
import classNames from 'classnames';
import { Eye, EyeSlash, Trash } from 'react-bootstrap-icons';
import { Button } from 'react-bootstrap';

import Table from 'components/Layout/Table';
import { CompositeMapResource } from 'api/resources/CompositeMap';
import { usePaging } from 'hooks/usePaging';

export default function CompositeList({
  blSubSampleId,
}: {
  blSubSampleId: number;
}) {
  const controller = useController();
  const { skip: compositeSkip, limit: compositeLimit } = usePaging(
    10,
    0,
    'composite'
  );

  const composites = useSuspense(CompositeMapResource.getList, {
    blSubSampleId,
    skip: compositeSkip,
    limit: compositeLimit,
  });

  function toggleCompositeOpacity(
    xfeFluorescenceCompositeId: number,
    opacity: boolean
  ) {
    return controller.fetch(
      CompositeMapResource.partialUpdate,
      { xfeFluorescenceCompositeId },
      { opacity: opacity ? 1 : 0 }
    );
  }

  function deleteComposite(xfeFluorescenceCompositeId: number) {
    return controller.fetch(CompositeMapResource.delete, {
      xfeFluorescenceCompositeId,
    });
  }
  return (
    <>
      <h2>Composite Maps</h2>
      <Table
        urlPrefix="composite"
        keyId="xfeFluorescenceCompositeId"
        results={composites.results}
        paginator={{
          total: composites.total,
          skip: composites.skip,
          limit: composites.limit,
        }}
        columns={[
          {
            label: 'ID',
            key: 'xfeFluorescenceCompositeId',
            className: 'text-break',
          },
          {
            label: 'R',
            key: 'r',
            className: 'text-break',
          },
          {
            label: 'R ROI',
            key: '_metadata.rROI',
            className: 'text-break',
          },
          {
            label: 'G',
            key: 'g',
            className: 'text-break',
          },
          {
            label: 'G ROI',
            key: '_metadata.gROI',
            className: 'text-break',
          },
          {
            label: 'B',
            key: 'b',
            className: 'text-break',
          },
          {
            label: 'B ROI',
            key: '_metadata.bROI',
            className: 'text-break',
          },
          {
            label: '',
            key: 'opacity',
            className: 'text-nowrap text-end',
            formatter: (row) => (
              <>
                <Button
                  size="sm"
                  onClick={() =>
                    toggleCompositeOpacity(
                      row.xfeFluorescenceCompositeId,
                      !row.opacity
                    )
                  }
                  className={classNames(
                    'me-1',
                    { 'btn-primary': row.opacity },
                    { 'btn-light': !row.opacity }
                  )}
                >
                  {row.opacity ? <Eye /> : <EyeSlash />}
                </Button>
                <Button
                  variant="danger"
                  size="sm"
                  onClick={() =>
                    deleteComposite(row.xfeFluorescenceCompositeId)
                  }
                >
                  <Trash />
                </Button>
              </>
            ),
          },
        ]}
        emptyText="No composites yet"
      />
    </>
  );
}
