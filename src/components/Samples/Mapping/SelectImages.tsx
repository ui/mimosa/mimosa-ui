import classNames from 'classnames';
import { Button, Container, Dropdown, Row, Col, Alert } from 'react-bootstrap';
import { EyeSlash, EyeFill, InfoCircle } from 'react-bootstrap-icons';

import { useHideImages } from 'hooks/useHideImages';
import { useSuspense } from '@data-client/react';
import { SampleImageResource } from 'api/resources/SampleImage';

export default function SelectImages({ blSampleId }: { blSampleId: number }) {
  const { hiddenImages, toggleImage, showAll } = useHideImages();
  const images = useSuspense(SampleImageResource.getList, {
    blSampleId,
    limit: 9999,
  });

  return (
    <Dropdown>
      <Dropdown.Toggle id="images-dropdown" className="ms-1">
        Image Visibility
      </Dropdown.Toggle>
      <Dropdown.Menu
        style={{ width: 250, maxHeight: '30vh', overflowY: 'scroll' }}
      >
        <Container>
          <Alert variant="info" style={{ fontSize: 12, padding: '0.5rem' }}>
            <InfoCircle className="me-1" />
            Double click an image to toggle its visibility
          </Alert>
          <Row className="mb-1">
            <Col xs={9}>Show All Images</Col>
            <Col xs={3} className="text-end">
              <Button
                size="sm"
                onClick={() => showAll()}
                disabled={hiddenImages.length === 0}
              >
                <EyeFill />
              </Button>
            </Col>
          </Row>
          {images.results.map((image) => (
            <Row key={image.blSampleImageId} className="mb-1">
              <Col xs={4}>{image.offsetX}</Col>
              <Col xs={5}>{image.offsetY}</Col>
              <Col xs={3} className="text-end">
                <Button
                  onClick={() => toggleImage(image.blSampleImageId)}
                  size="sm"
                  className={classNames(
                    {
                      'btn-primary':
                        hiddenImages.indexOf(image.blSampleImageId) === -1,
                    },
                    {
                      'btn-light':
                        hiddenImages.indexOf(image.blSampleImageId) > -1,
                    }
                  )}
                >
                  {hiddenImages.indexOf(image.blSampleImageId) > -1 ? (
                    <EyeSlash />
                  ) : (
                    <EyeFill />
                  )}
                </Button>
              </Col>
            </Row>
          ))}
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
}
