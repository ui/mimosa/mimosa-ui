import { useState } from 'react';
import { useController, useSuspense } from '@data-client/react';
import classNames from 'classnames';
import { Eye, EyeSlash, Trash } from 'react-bootstrap-icons';
import { Button, Form } from 'react-bootstrap';

import { MapResource } from 'api/resources/Map';
import Table from 'components/Layout/Table';
import { getROIName } from './Maps';
import { usePaging } from 'hooks/usePaging';
import MapDetailsView from './MapDetailsView';
import CreateCompositeButton from './CreateCompositeButton';
import Confirm from 'components/Common/Confirm';

export default function MapList({ blSubSampleId }: { blSubSampleId: number }) {
  const controller = useController();
  const [mapSelection, setMapSelection] = useState<number[]>([]);
  const { skip: mapSkip, limit: mapLimit } = usePaging(10, 0, 'map');
  const maps = useSuspense(MapResource.getList, {
    blSubSampleId,
    skip: mapSkip,
    limit: mapLimit,
  });

  function toggleOpacity(xrfFluorescenceMappingId: number, opacity: boolean) {
    return controller.fetch(
      MapResource.partialUpdate,
      { xrfFluorescenceMappingId },
      { opacity: opacity ? 1 : 0 }
    );
  }

  function updateMapSelection(
    xrfFluorescenceMappingId: number,
    selected: boolean
  ) {
    console.log('update selection', xrfFluorescenceMappingId, selected);
    if (!selected) {
      setMapSelection((prevSelection) => {
        const newSelection = [...prevSelection];
        newSelection.splice(newSelection.indexOf(xrfFluorescenceMappingId), 1);
        return newSelection;
      });
    } else {
      setMapSelection((prevSelection) => {
        const newSelection = [...prevSelection];
        newSelection.push(xrfFluorescenceMappingId);
        return newSelection;
      });
    }
  }

  function deleteMap(xrfFluorescenceMappingId: number) {
    return controller.fetch(MapResource.delete, {
      xrfFluorescenceMappingId,
    });
  }

  return (
    <>
      <h2>Maps</h2>
      <div className="text-end">
        <CreateCompositeButton mapIds={mapSelection} maps={maps.results} />
      </div>
      <Table
        urlPrefix="map"
        keyId="xrfFluorescenceMappingId"
        results={maps.results}
        paginator={{
          total: maps.total,
          skip: maps.skip,
          limit: maps.limit,
        }}
        columns={[
          {
            label: 'ID',
            key: 'xrfFluorescenceMappingId',
            className: 'text-break',
          },
          {
            label: 'ROI',
            key: 'XRFFluorescenceMappingROI.edge',
            formatter: (row) => getROIName(row),
            className: 'text-break',
          },
          {
            label: 'Px',
            key: 'GridInfo.steps_x',
            className: 'text-nowrap',
          },
          {
            label: 'Py',
            key: 'GridInfo.steps_y',
            className: 'text-nowrap',
          },
          {
            label: '',
            key: 'opacity',
            className: 'text-nowrap text-end',
            formatter: (row) => (
              <>
                <Form.Check
                  inline
                  defaultChecked={
                    mapSelection.indexOf(row.xrfFluorescenceMappingId) > -1
                  }
                  onChange={(e) =>
                    updateMapSelection(
                      row.xrfFluorescenceMappingId,
                      e.target.checked
                    )
                  }
                />
                <MapDetailsView {...row} />
                <Button
                  size="sm"
                  onClick={() =>
                    toggleOpacity(row.xrfFluorescenceMappingId, !row.opacity)
                  }
                  className={classNames(
                    { 'btn-primary': row.opacity },
                    { 'btn-light': !row.opacity }
                  )}
                >
                  {row.opacity ? <Eye /> : <EyeSlash />}
                </Button>
                <Confirm
                  action={() => deleteMap(row.xrfFluorescenceMappingId)}
                  message="Are you sure you want to remove this map?"
                  title="Delete Map"
                >
                  {({ showConfirm }) => (
                    <Button
                      className="ms-1"
                      variant="danger"
                      size="sm"
                      onClick={() => showConfirm()}
                    >
                      <Trash />
                    </Button>
                  )}
                </Confirm>
              </>
            ),
          },
        ]}
        emptyText="No maps yet"
      />
    </>
  );
}
