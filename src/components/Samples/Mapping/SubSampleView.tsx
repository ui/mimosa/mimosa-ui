import { Suspense } from 'react';

import DataCollectionList from './DataCollectionList';
import MapList from './MapList';
import CompositeList from './CompositeList';
import Loading from 'components/Loading';

export default function SubSampleView({
  blSubSampleId,
}: {
  blSubSampleId: number;
}) {
  return (
    <>
      <Suspense fallback={<Loading title="Data Collections" />}>
        <DataCollectionList blSubSampleId={blSubSampleId} />
      </Suspense>
      <Suspense fallback={<Loading title="Maps" />}>
        <MapList blSubSampleId={blSubSampleId} />
      </Suspense>
      <Suspense fallback={<Loading title="Composites" />}>
        <CompositeList blSubSampleId={blSubSampleId} />
      </Suspense>
    </>
  );
}
