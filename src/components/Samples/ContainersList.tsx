import { useSuspense } from '@data-client/react';
import { useNavigate } from 'react-router-dom';

import Table from 'components/Layout/Table';
import Search from 'components/Layout/Search';
import { ContainerResource } from 'api/resources/Container';
import { Container } from 'models/Container';
import { usePath } from 'hooks/usePath';
import { usePaging } from 'hooks/usePaging';
import { useSearch } from 'hooks/useSearch';
import { Badge } from 'react-bootstrap';

function AssignedStatus(container: Container) {
  return container._metadata.assigned ? (
    <Badge bg="danger">Assigned</Badge>
  ) : null;
}

interface IContainersList {
  proteinId?: string;
  focusSearch?: boolean;
}

export default function ContainerList({ focusSearch }: IContainersList) {
  const { skip, limit } = usePaging(10);
  const search = useSearch();
  const navigate = useNavigate();
  const proposal = usePath('proposal');
  const containers = useSuspense(ContainerResource.getList, {
    skip,
    limit,
    ...(proposal ? { proposal } : {}),
    ...(search ? { search } : null),
  });

  const onRowClick = (row: Container) => {
    navigate(`/proposals/${proposal}/samples/containers/${row.containerId}`);
  };

  return (
    <section>
      <h1>Containers</h1>
      <Search focus={focusSearch} />
      <Table
        responsive
        titleColumn="code"
        keyId="containerId"
        results={containers.results}
        onRowClick={onRowClick}
        paginator={{
          total: containers.total,
          skip: containers.skip,
          limit: containers.limit,
        }}
        columns={[
          { label: 'Code', key: 'code' },
          { label: 'Capacity', key: 'capacity' },
          { label: '# Samples', key: '_metadata.samples' },
          { label: 'Status', key: 'containerStatus' },
          {
            label: 'Location',
            key: 'beamlineLocation',
          },
          {
            label: 'Assigned',
            key: '_metadata.assigned',
            formatter: (row) => <AssignedStatus {...row} />,
          },
        ]}
        emptyText="No containers yet"
      />
    </section>
  );
}
