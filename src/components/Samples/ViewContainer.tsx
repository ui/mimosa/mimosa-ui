import { useController, useSuspense } from '@data-client/react';
import { useParams } from 'react-router-dom';
import { set } from 'lodash';
import { Row, Col, Button, InputGroup } from 'react-bootstrap';

import NetworkErrorPage from 'components/NetworkErrorPage';
import { useSchema } from 'hooks/useSpec';
import { ContainerResource } from 'api/resources/Container';
import { ContainerHistoryResource } from 'api/resources/ContainerHistory';
import SamplesList from './SamplesList';

import InlineEditable from 'components/RJSF/InlineEditable';
import { hiddenField } from 'components/RJSF/uiSchemaHelpers';
import ContainerHistoryList from './ContainersHistoryList';
import { Suspense } from 'react';
import Loading from 'components/Loading';

function UnassignButton({ containerId }: { containerId: string }) {
  const controller = useController();

  function unAssign() {
    const obj = {
      beamlineLocation: null,
      sampleChangerLocation: null,
    };
    controller
      .fetch(
        ContainerResource.partialUpdate,
        { containerId },
        // @ts-expect-error
        obj
      )
      .then(() => {
        controller.invalidateAll(ContainerHistoryResource.getList);
      });
  }

  return (
    <InputGroup>
      <Button onClick={() => unAssign()}>Unassign</Button>
    </InputGroup>
  );
}

function ViewContainerMain() {
  const { containerId } = useParams();
  const controller = useController();

  const container = useSuspense(
    ContainerResource.get,
    containerId
      ? {
          containerId,
        }
      : null
  );

  const schema = useSchema('Container', '');
  const uiSchema = {
    dewarId: hiddenField,
    blSampleId: hiddenField,
    containerId: hiddenField,
    sampleChangerLocation: hiddenField,

    Dewar: hiddenField,
    _metadata: hiddenField,
  };

  if (!containerId) return <div>No container id provided</div>;
  if (!container) return <div>No such container</div>;

  function onChange({ field, value }: { field: string; value: any }) {
    const obj = {};
    set(obj, field, value);
    return controller.fetch(
      ContainerResource.partialUpdate,
      // @ts-expect-error
      { containerId },
      obj
    );
  }

  const editable = ['root_code', 'root_capacity'];

  const extraComponents = {
    root_beamlineLocation: container?._metadata.assigned ? (
      <UnassignButton containerId={containerId} />
    ) : null,
  };

  return (
    <div>
      <h1>View Container</h1>
      <section>
        <Row>
          <Col sm={8}>
            <InlineEditable
              editable={editable}
              schema={schema}
              uiSchema={uiSchema}
              formData={container}
              onChange={onChange}
              extraComponents={extraComponents}
            />
          </Col>
          <Col sm={4}>
            <Suspense fallback={<Loading title="Container History" />}>
              <ContainerHistoryList containerId={containerId} />
            </Suspense>
          </Col>
        </Row>
      </section>
      <section>
        <Suspense fallback={<Loading title="Samples" />}>
          <SamplesList containerId={containerId} />
        </Suspense>
      </section>
    </div>
  );
}

export default function ViewContainer() {
  return (
    <NetworkErrorPage>
      <ViewContainerMain />
    </NetworkErrorPage>
  );
}
