import { ContainerResource } from 'api/resources/Container';
import { usePath } from 'hooks/usePath';
import { useRef, useState } from 'react';
import { Button, Form, Spinner } from 'react-bootstrap';
import { useController, useSuspense } from '@data-client/react';

interface IAssignContainer {
  beamLineName: string;
  show?: boolean;
  onHide?: () => void;
}

export default function AssignContainer({
  beamLineName,
  onHide,
}: IAssignContainer) {
  const [pending, setPending] = useState<boolean>(false);
  const controller = useController();
  const proposal = usePath('proposal');
  const selectRef = useRef<HTMLSelectElement>(null);
  const containers = useSuspense(ContainerResource.getList, {
    skip: 0,
    limit: 9999,
    ...(proposal ? { proposal } : {}),
  });

  async function assign() {
    if (!selectRef.current) return;
    const obj = {
      beamlineLocation: beamLineName,
      sampleChangerLocation: 1,
    };
    setPending(true);
    await controller.fetch(
      ContainerResource.partialUpdate,
      { containerId: selectRef.current?.value },
      // @ts-expect-error
      obj
    );
    setPending(false);
    onHide && onHide();
  }

  return (
    <Form>
      <Form.Label>Container:</Form.Label>
      <Form.Control as="select" ref={selectRef}>
        {containers.results.map((container) => (
          <option
            key={container.containerId}
            value={container.containerId}
            disabled={container._metadata.assigned}
          >
            {container.code}
          </option>
        ))}
      </Form.Control>
      <div className="d-grid">
        <Button onClick={() => assign()} disabled={pending} className="mt-1">
          {pending && (
            <Spinner
              size="sm"
              animation="border"
              role="status"
              className="me-1"
            >
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          )}
          Assign
        </Button>
      </div>
    </Form>
  );
}
