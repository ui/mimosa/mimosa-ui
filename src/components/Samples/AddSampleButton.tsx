import { Button, Spinner } from 'react-bootstrap';
import { Plus } from 'react-bootstrap-icons';
import { useController } from '@data-client/react';
import { SessionContainerResource } from 'api/resources/Container';
import { useNavigate } from 'react-router-dom';
import { usePath } from 'hooks/usePath';
import { useState } from 'react';

export default function AddSampleButton({ sessionId }: { sessionId: string }) {
  const [pending, setPending] = useState<boolean>(false);
  const { fetch } = useController();
  const navigate = useNavigate();
  const proposal = usePath('proposal');
  async function gotoContainer() {
    setPending(true);
    const container = await fetch(SessionContainerResource.get, {
      containerId: sessionId,
    });
    setPending(false);
    navigate(
      `/proposals/${proposal}/samples/containers/${container.containerId}`
    );
  }

  return (
    <Button
      size="sm"
      onClick={gotoContainer}
      className="me-1"
      disabled={pending}
    >
      {pending && (
        <Spinner size="sm" animation="border" role="status" className="me-1">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      )}
      {!pending && <Plus />}
      Samples
    </Button>
  );
}
