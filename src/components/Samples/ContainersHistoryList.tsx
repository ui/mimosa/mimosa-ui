import { useSuspense } from '@data-client/react';

import Table from 'components/Layout/Table';
import { ContainerHistoryResource } from 'api/resources/ContainerHistory';
import { usePaging } from 'hooks/usePaging';

interface IContainerHistoryList {
  containerId: string;
}

export default function ContainerHistoryList({
  containerId,
}: IContainerHistoryList) {
  const { skip, limit } = usePaging(5, 0, 'history');
  const containerHistory = useSuspense(ContainerHistoryResource.getList, {
    skip,
    limit,
    containerId: containerId,
  });

  return (
    <section>
      <h2>Container History</h2>
      <Table
        urlPrefix="history"
        keyId="containerHistoryId"
        results={containerHistory.results}
        paginator={{
          total: containerHistory.total,
          skip: containerHistory.skip,
          limit: containerHistory.limit,
        }}
        columns={[
          { label: 'Timestamp', key: 'blTimeStamp' },
          {
            label: 'Status',
            key: 'status',
          },
          {
            label: 'Beamline',
            key: 'beamlineName',
          },
          {
            label: 'Location',
            key: 'location',
          },
        ]}
        emptyText="No container history yet"
      />
    </section>
  );
}
