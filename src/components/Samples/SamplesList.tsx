import { useSuspense } from '@data-client/react';
import { useNavigate, useSearchParams, Link } from 'react-router-dom';
import { Image, Plus } from 'react-bootstrap-icons';

import Table from 'components/Layout/Table';
import { tagCell } from 'components/Layout/TableCells';
import Search from 'components/Layout/Search';
import { SampleResource } from 'api/resources/Sample';
import { Sample } from 'models/Sample';
import { usePath } from 'hooks/usePath';
import { usePaging } from 'hooks/usePaging';
import { useSearch } from 'hooks/useSearch';
import Filter from 'components/Filter';
import { useSchema } from 'hooks/useSpec';
import SampleStatus, { DCTypes } from './SampleStatus';
import useComponentsTitle from 'hooks/useComponentsTitle';
import { useProposalInfo } from 'hooks/useProposalInfo';
import CreateSample from './CreateSample';
import { ModalButton } from 'components/Common/Modal';

function SampleStatusFilter({ urlKey }: { urlKey: string }) {
  const schema = useSchema('SampleStatus', 'Sample Status');
  if (!schema.enum) return null;
  const filterTypes = schema.enum.map(
    // @ts-expect-error
    (status: string) => ({
      filterKey: status,
      filterValue: status,
    })
  );

  return <Filter urlKey={urlKey} filters={filterTypes} />;
}

interface ISamplesList {
  proteinId?: string;
  focusSearch?: boolean;
  containerId?: string;
  selectSample?: (blSampleId: number) => void;
  embedded?: boolean;
}

export default function SamplesList({
  proteinId,
  focusSearch,
  containerId,
  selectSample,
  embedded,
}: ISamplesList) {
  const [searchParams] = useSearchParams();
  const { skip, limit } = usePaging(10, 0, 'sample');
  const search = useSearch();
  const navigate = useNavigate();
  const proposal = usePath('proposal');
  const proposalInfo = useProposalInfo();
  const componentsTitle = useComponentsTitle();
  const status = searchParams.get('sampleStatus');
  const samples = useSuspense(SampleResource.getList, {
    skip,
    limit,
    ...(proposal ? { proposal } : {}),
    ...(proteinId ? { proteinId } : {}),
    ...(containerId ? { containerId } : {}),
    ...(search ? { search } : null),
    ...(status ? { status } : null),
  });

  const onRowClick = (row: Sample) => {
    if (selectSample) {
      selectSample(row.blSampleId);
      return;
    }

    navigate(`/proposals/${proposal}/samples/${row.blSampleId}`);
  };

  return (
    <section>
      {!embedded && <h1>Samples</h1>}
      {!embedded && containerId && (
        <>
          <ModalButton
            title="Create Sample"
            buttonTitle={
              <>
                <Plus />
                Create
              </>
            }
          >
            {({ hideModal }) => (
              <CreateSample
                containerId={parseInt(containerId)}
                onHide={() => hideModal()}
              />
            )}
          </ModalButton>
        </>
      )}
      <SampleStatusFilter urlKey="sampleStatus" />
      <Search focus={focusSearch} skipKey="sampleskip" />
      <Table
        responsive
        titleColumn="name"
        urlPrefix="sample"
        keyId="blSampleId"
        results={samples.results}
        onRowClick={onRowClick}
        paginator={{
          total: samples.total,
          skip: samples.skip,
          limit: samples.limit,
        }}
        columns={[
          ...(containerId
            ? [
                {
                  label: 'Location',
                  key: 'location',
                },
              ]
            : []),
          { label: 'Name', key: 'name' },
          {
            label: componentsTitle.slice(0, -1),
            key: 'Crystal.Protein.acronym',
          },
          {
            label: 'Tags',
            key: 'extraMetadata',
            formatter: tagCell,
          },
          { label: 'Sub Samples', key: '_metadata.subsamples' },
          ...(embedded ? [] : [{ label: 'Container', key: 'Container.code' }]),
          {
            label: '# DCs',
            key: '_metadata.datacollections',
          },
          {
            label: 'DC Types',
            key: '_metadata.types',
            formatter: (row) => <DCTypes {...row} />,
          },
          {
            label: 'Status',
            key: 'sampleStatus',
            formatter: (row) => <SampleStatus {...row} />,
          },
          ...(!embedded &&
          proposalInfo &&
          proposalInfo._metadata.uiGroups &&
          proposalInfo._metadata.uiGroups.includes('mapping')
            ? [
                {
                  label: '',
                  key: 'review',
                  formatter: (row: Sample) => (
                    <Link
                      className="btn btn-primary btn-sm"
                      role="button"
                      title="Sample Review"
                      to={`/proposals/${proposal}/samples/review?blSampleId=${row.blSampleId}`}
                    >
                      <Image />
                    </Link>
                  ),
                  className: 'text-end',
                },
              ]
            : []),
        ]}
        emptyText="No samples yet"
      />
    </section>
  );
}
