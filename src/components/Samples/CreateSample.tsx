import Form from 'components/Common/Form';
import { SampleResource } from 'api/resources/Sample';
import { useProposalInfo } from '../../hooks/useProposalInfo';
import useComponentsTitle from 'hooks/useComponentsTitle';
import { hiddenField } from 'components/RJSF/uiSchemaHelpers';
import { ProteinResource } from 'api/resources/Protein';
import { useController, useSuspense } from '@data-client/react';

interface ICreateSample {
  containerId: number;
  show?: boolean;
  onHide?: () => void;
}

export default function CreateSample({ containerId, onHide }: ICreateSample) {
  const { invalidateAll } = useController();
  const componentsTitle = useComponentsTitle();
  const proposal = useProposalInfo();

  const samples = useSuspense(SampleResource.getList, {
    containerId,
  });

  function success() {
    invalidateAll(SampleResource.getList);
    onHide && onHide();
  }

  const uiSchema = {
    proposalId: hiddenField,
    containerId: hiddenField,
    location: { 'ui:readonly': true },
    Crystal: {
      'ui:title': 'Parent',
      crystalId: hiddenField,
      proteinId: {
        'ui:title': componentsTitle.slice(0, -1),
        'ui:options': {
          resource: ProteinResource,
          params: {
            proposalId: proposal?.proposalId,
          },
          key: 'acronym',
          value: 'proteinId',
        },
        'ui:widget': 'remoteSelect',
      },
    },
  };

  return (
    <Form
      schemaName="SampleCreate"
      uiSchema={uiSchema}
      resource={SampleResource}
      initialFormData={{
        proposalId: proposal && proposal.proposalId,
        location: samples.total + 1,
        containerId,
      }}
      onSuccess={() => success()}
    />
  );
}
