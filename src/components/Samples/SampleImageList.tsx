import { useSuspense } from '@data-client/react';
import { Suspense } from 'react';
import { Download } from 'react-bootstrap-icons';

import { SampleImageResource } from 'api/resources/SampleImage';
import FileNameButton from 'components/Common/FileNameButton';
import Table from 'components/Layout/Table';
import { imageCell } from 'components/Layout/TableCells';
import config from 'config/config';
import { usePaging } from 'hooks/usePaging';
import { useSign } from 'hooks/useSign';
import { SampleImage } from 'models/SampleImage';

function ActionsCell(row: SampleImage) {
  const { signHandler } = useSign();
  return (
    <>
      <FileNameButton filename={row.imageFullPath} />
      <a
        href={config.host + row._metadata.url}
        className="btn btn-primary btn-sm"
        onClick={(e) => signHandler(e)}
      >
        <Download />
      </a>
    </>
  );
}

interface ISampleImageList {
  blSampleId?: string;
  limit?: number;
}

function SampleImagesListMain(props: ISampleImageList) {
  const { blSampleId, limit: initialLimit } = props;
  const { skip, limit } = usePaging(initialLimit ?? 10, 0, 'sampleimage');
  const sampleImages = useSuspense(SampleImageResource.getList, {
    skip,
    limit,
    ...(blSampleId ? { blSampleId } : {}),
  });
  return (
    <>
      <h2>Sample Images</h2>
      <Table
        responsive
        titleColumn="blSampleImageId"
        urlPrefix="sampleimage"
        keyId="blSampleImageId"
        results={sampleImages.results}
        paginator={{
          total: sampleImages.total,
          skip: sampleImages.skip,
          limit: sampleImages.limit,
        }}
        emptyText="No sample images yet"
        columns={[
          { label: 'Id', key: 'blSampleImageId' },
          { label: 'Offset X', key: 'offsetX' },
          { label: 'Offset Y', key: 'offsetY' },
          {
            label: '',
            key: '_metadata.url',
            formatter: imageCell,
            formatterParams: {
              maxSize: 250,
              alt: (row: SampleImage) => `sampleimage${row.blSampleImageId}`,
            },
            className: 'text-end',
          },
          {
            label: '',
            key: 'actions',
            formatter: ActionsCell,
            className: 'text-end text-nowrap',
          },
        ]}
      />
    </>
  );
}

export default function SampleImageList(props: ISampleImageList) {
  return (
    <Suspense fallback={<p>Loading</p>}>
      <SampleImagesListMain {...props} />
    </Suspense>
  );
}
