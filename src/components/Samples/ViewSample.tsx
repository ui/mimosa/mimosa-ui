import { useController, useSuspense } from '@data-client/react';
import { useParams, Link } from 'react-router-dom';
import { set } from 'lodash';

import NetworkErrorPage from 'components/NetworkErrorPage';
import { useSchema } from 'hooks/useSpec';
import { useProposalInfo } from 'hooks/useProposalInfo';
import { useProposal } from 'hooks/useProposal';
import { SampleResource } from 'api/resources/Sample';
import { ProteinResource } from 'api/resources/Protein';

import EventList from 'components/Events/EventsList';
import InlineEditable from 'components/RJSF/InlineEditable';
import { Search } from 'react-bootstrap-icons';
import useComponentsTitle from 'hooks/useComponentsTitle';
import { hiddenField } from 'components/RJSF/uiSchemaHelpers';
import SampleImageList from './SampleImageList';

function ViewSampleMain() {
  const { blSampleId } = useParams();
  const { proposalName } = useProposal();
  const proposal = useProposalInfo();
  const controller = useController();
  const componentsTitle = useComponentsTitle();

  const sample = useSuspense(
    SampleResource.get,
    blSampleId
      ? {
          blSampleId,
        }
      : null
  );

  const schema = useSchema('Sample', 'View Sample', true);
  const uiSchema = {
    proposalId: hiddenField,
    blSampleId: hiddenField,
    containerId: hiddenField,
    location: hiddenField,
    Crystal: {
      name: 'Parent',
      crystalId: hiddenField,
      proteinId: {
        'ui:title': componentsTitle.slice(0, -1),
        'ui:options': {
          resource: ProteinResource,
          params: {
            proposalId: proposal?.proposalId,
          },
          key: 'acronym',
          value: 'proteinId',
        },
        'ui:widget': 'remoteSelect',
      },
      Protein: hiddenField,
      cell_a: hiddenField,
      cell_b: hiddenField,
      cell_c: hiddenField,
      cell_alpha: hiddenField,
      cell_beta: hiddenField,
      cell_gamma: hiddenField,
    },
    _metadata: hiddenField,
  };

  if (!blSampleId) return <div>No sample id provided</div>;

  function onChange({ field, value }: { field: string; value: any }) {
    const obj = {};
    set(obj, field, value);
    // @ts-expect-error
    return controller.fetch(SampleResource.partialUpdate, { blSampleId }, obj);
  }

  const editable = [
    'root_name',
    'root_comments',
    'root_Crystal_cell_a',
    'root_Crystal_cell_b',
    'root_Crystal_cell_c',
    'root_Crystal_cell_alpha',
    'root_Crystal_cell_beta',
    'root_Crystal_cell_gamma',
    'root_Crystal_proteinId',
  ];

  const staticValues = {
    root_Crystal_proteinId: sample?.Crystal.Protein.acronym,
  };

  const extraComponents = {
    root_Crystal_proteinId: sample?.Crystal.proteinId ? (
      <Link
        className="btn btn-primary btn-sm"
        role="button"
        title={`View ${componentsTitle}`}
        to={`/proposals/${proposalName}/${componentsTitle.toLowerCase()}/${
          sample?.Crystal.proteinId
        }`}
      >
        <Search />
      </Link>
    ) : null,
  };

  return (
    <div>
      <section>
        {proposal &&
          proposal._metadata.uiGroups &&
          proposal._metadata.uiGroups.includes('mapping') && (
            <div className="text-end">
              <Link
                className="btn btn-primary btn-sm"
                role="button"
                title="Sample Review"
                to={`/proposals/${proposal.proposal}/samples/review?blSampleId=${blSampleId}`}
              >
                Sample Review
              </Link>
            </div>
          )}
        <InlineEditable
          editable={editable}
          schema={schema}
          uiSchema={uiSchema}
          formData={sample}
          onChange={onChange}
          staticValues={staticValues}
          extraComponents={extraComponents}
        />
      </section>
      <section>
        <SampleImageList blSampleId={blSampleId} limit={5} />
      </section>
      <section>
        <EventList blSampleId={blSampleId} limit={5} />
      </section>
    </div>
  );
}

export default function ViewSample() {
  return (
    <NetworkErrorPage>
      <ViewSampleMain />
    </NetworkErrorPage>
  );
}
