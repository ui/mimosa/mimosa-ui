import { useSuspense } from '@data-client/react';
import { useNavigate, Link } from 'react-router-dom';
import { List } from 'react-bootstrap-icons';

import Table from 'components/Layout/Table';
import { SessionResource } from 'api/resources/Session';
import { Session } from 'models/Session';
import { usePath } from 'hooks/usePath';
import { Badge } from 'react-bootstrap';
import { usePaging } from 'hooks/usePaging';
import formattedDateTime from 'utils/datetime';

export default function SessionList({ sortBy }: { sortBy?: string }) {
  const { skip, limit } = usePaging(10);
  const navigate = useNavigate();
  const proposal = usePath('proposal');
  const sessions = useSuspense(SessionResource.getList, {
    skip,
    limit,
    ...(proposal ? { proposal } : {}),
  });

  const onRowClick = (row: Session) => {
    navigate(
      `/proposals/${proposal}/samples/review?sessionId=${row.sessionId}`
    );
  };

  return (
    <section>
      <h1>Sessions</h1>
      <Table
        responsive
        titleColumn="session"
        keyId="sessionId"
        results={sessions.results}
        onRowClick={onRowClick}
        paginator={{
          total: sessions.total,
          skip: sessions.skip,
          limit: sessions.limit,
        }}
        columns={[
          { label: 'Id', key: 'sessionId' },
          { label: 'Session', key: 'session' },
          {
            label: 'Start',
            key: 'startDate',
            formatter: (row, column) =>
              formattedDateTime(row[column.key], false),
          },
          {
            label: 'End',
            key: 'endDate',
            formatter: (row, column) =>
              formattedDateTime(row[column.key], false),
          },
          { label: 'Beamline', key: 'beamLineName' },
          { label: 'Comments', key: 'comments' },
          {
            label: 'Type',
            key: 'Type',
            formatter: (row: Session) => row._metadata.sessionTypes.join(','),
          },
          { label: '# DCs', key: '_metadata.datacollections' },
          {
            label: 'Active',
            key: 'active',
            formatter: (row: Session) =>
              row._metadata.active ? <Badge bg="success">Active</Badge> : null,
          },
          {
            label: '',
            key: 'review',
            formatter: (row: Session) => (
              <Link
                className="btn btn-primary btn-sm"
                role="button"
                title="Data Collection List"
                to={`/proposals/${proposal}/sessions/${row.sessionId}`}
              >
                <List />
              </Link>
            ),
            className: 'text-end',
          },
        ]}
        emptyText="No sessions yet"
      />
    </section>
  );
}
