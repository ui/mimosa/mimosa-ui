import { useSuspense } from '@data-client/react';

import Table from 'components/Layout/Table';
import { PersonResource } from 'api/resources/Person';
import { usePaging } from 'hooks/usePaging';
import { Badge } from 'react-bootstrap';

export default function SessionPeople({ sessionId }: { sessionId: string }) {
  const { skip, limit } = usePaging(10, 0, 'person');
  const persons = useSuspense(PersonResource.getList, {
    skip,
    limit,
    ...(sessionId ? { sessionId } : {}),
  });

  return (
    <section>
      <Table
        urlPrefix="person"
        keyId="personId"
        results={persons.results}
        paginator={{
          total: persons.total,
          skip: persons.skip,
          limit: persons.limit,
        }}
        columns={[
          { label: 'Given Name', key: 'givenName' },
          { label: 'Surname', key: 'familyName' },
          { label: 'Remote', key: '_metadata.remote' },
          { label: 'Role', key: '_metadata.role' },
          {
            label: 'Sessions',
            key: '_metadata.sessions',
            formatter: (row) =>
              row._metadata.sessions === 0 ? (
                <Badge bg="danger">New</Badge>
              ) : (
                <>
                  {row._metadata.sessions} (Last: {row._metadata.lastSession})
                </>
              ),
          },
        ]}
        emptyText="No people yet"
      />
    </section>
  );
}
