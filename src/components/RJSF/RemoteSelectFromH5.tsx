import { createRef, Suspense, useCallback, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import { useSuspense } from '@data-client/react';
import {
  WidgetProps,
  UIOptionsType,
  EnumOptionsType,
  StrictRJSFSchema,
  FormContextType,
  RJSFSchema,
} from '@rjsf/utils';
import { EventResource } from 'api/resources/Event';
import { H5MetaEndpoint } from 'api/resources/H5Grove/Meta';
import NetworkErrorPage from 'components/NetworkErrorPage';

interface SelectH5Options<
  T = any,
  S extends StrictRJSFSchema = RJSFSchema,
  F extends FormContextType = any
> extends UIOptionsType<T, S, F> {
  dataCollectionId: number;
  shape: number;
}

interface SelectH5WidgetProps<
  T = any,
  S extends StrictRJSFSchema = RJSFSchema,
  F extends FormContextType = any
> extends Omit<WidgetProps, 'options'> {
  options: NonNullable<SelectH5Options<T, S, F>> & {
    enumOptions?: EnumOptionsType[];
  };
}

function SelectFromH5(props: SelectH5WidgetProps) {
  const ref = createRef<HTMLSelectElement>();

  const {
    id,
    value,
    disabled,
    readonly,
    autofocus,
    onBlur,
    onFocus,
    onChange,
    options,
  } = props;

  const dataCollection = useSuspense(EventResource.getList, {
    dataCollectionId: options.dataCollectionId,
  });

  const meta = useSuspense(H5MetaEndpoint, {
    dataCollectionId: options.dataCollectionId,
    // @ts-expect-error
    path: dataCollection.results[0].Item.imageContainerSubPath,
  });

  const setValue = useCallback(() => {
    setTimeout(() => {
      if (ref.current) onChange(ref.current.value);
    }, 1000);
  }, [ref, onChange]);

  useEffect(() => {
    if (value === undefined) setValue();
  }, [value, setValue, ref]);

  return (
    <Form.Control
      ref={ref}
      as="select"
      className="remote-select"
      value={value}
      id={id}
      disabled={disabled || readonly}
      autoFocus={autofocus}
      onChange={(event) => onChange(event.target.value)}
      onBlur={id && onBlur && ((event) => onBlur(id, event.target.value))}
      onFocus={id && onFocus && ((event) => onFocus(id, event.target.value))}
      defaultValue={meta && meta.children?.[0].name}
    >
      {options &&
        meta.children
          .filter((child) => child.shape.length === options.shape)
          .map((child) => (
            <option key={child.name} value={child.name}>
              {child.name}
            </option>
          ))}
    </Form.Control>
  );
}

function RemoteSelectFromH5(props: WidgetProps) {
  const selectProps = props as unknown as SelectH5WidgetProps;
  return (
    <NetworkErrorPage>
      <Suspense fallback={<>Loading Options...</>}>
        <SelectFromH5 {...selectProps} />
      </Suspense>
    </NetworkErrorPage>
  );
}

export default RemoteSelectFromH5;
