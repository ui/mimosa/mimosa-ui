export const hiddenField = {
  'ui:classNames': 'hidden-row',
  'ui:widget': 'hidden',
};

export const textArea = { 'ui:widget': 'textarea' };
