import { Suspense, useCallback, useState } from 'react';
import { useController } from '@data-client/react';
import {
  WidgetProps,
  UIOptionsType,
  EnumOptionsType,
  StrictRJSFSchema,
  FormContextType,
  RJSFSchema,
} from '@rjsf/utils';
import AsyncSelect from 'react-select/async';
import createPaginatedResource from 'api/resources/Base/Paginated';

interface SelectOptions<
  T = any,
  S extends StrictRJSFSchema = RJSFSchema,
  F extends FormContextType = any
> extends UIOptionsType<T, S, F> {
  resource: ReturnType<typeof createPaginatedResource>;
  params: Record<string, string>;
  key: string | ((row: any) => string);
  value: string;
}

interface SelectWidgetProps<
  T = any,
  S extends StrictRJSFSchema = RJSFSchema,
  F extends FormContextType = any
> extends Omit<WidgetProps, 'options'> {
  options: NonNullable<SelectOptions<T, S, F>> & {
    enumOptions?: EnumOptionsType[];
  };
}

export interface Option {
  readonly value: string;
  readonly label: string;
  readonly color: string;
  readonly isFixed?: boolean;
  readonly isDisabled?: boolean;
}

function ReactSelect(props: SelectWidgetProps) {
  const { fetch } = useController();
  const [currentOptions, setCurrentOptions] = useState<Option[]>([]);

  const { id, value, disabled, readonly, onBlur, onFocus, onChange, options } =
    props;

  const loadOptions = useCallback(
    async (inputValue: string, callback: (options: Option[]) => void) => {
      const selectOptions = await fetch(options.resource?.getList, {
        ...(options.params ? options.params : {}),
        search: inputValue,
      });

      const newOptions = selectOptions.results.map((c: any) => ({
        value: c[options.value],
        label:
          typeof options.key === 'string' ? c[options.key] : options.key(c),
        color: 'grey',
      }));
      setCurrentOptions(newOptions);
      callback(newOptions);
    },
    [fetch, options]
  );

  return (
    <div style={{ flex: 1 }}>
      <AsyncSelect
        className="remote-react-select"
        defaultValuevalue={
          currentOptions.find((row: Option) => row.value === value)?.label
        }
        defaultOptions={currentOptions}
        id={id}
        // @ts-expect-error
        loadOptions={loadOptions}
        disabled={disabled || readonly}
        // @ts-expect-error
        onChange={({ value }) => onChange(value)}
        onBlur={id && onBlur && ((event) => onBlur(id, event.target.value))}
        onFocus={id && onFocus && ((event) => onFocus(id, event.target.value))}
      />
    </div>
  );
}

function RemoteReactSelect(props: WidgetProps) {
  const selectProps = props as unknown as SelectWidgetProps;
  return (
    <Suspense fallback={<>Loading Options...</>}>
      <ReactSelect {...selectProps} />
    </Suspense>
  );
}

export default RemoteReactSelect;
