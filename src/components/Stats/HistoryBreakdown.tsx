import { useSuspense } from '@data-client/react';
import { useNavigate } from 'react-router-dom';

import { BreakdownEndpoint } from 'api/resources/Stats/Breakdown';
import { usePath } from 'hooks/usePath';
import PlotEnhancer from './PlotEnhancer';
// import { DateTime } from 'luxon';
import { getColors } from '../../utils/colours';

export default function HistoryBreakdown() {
  const navigate = useNavigate();
  const sessionId = usePath('sessionId');
  const breakdown = useSuspense(BreakdownEndpoint, {
    ...(sessionId ? { sessionId } : null),
  });
  // const types = Array.from(
  //   new Set(breakdown.history.map((obj) => obj.eventType))
  // );
  const proteins = Array.from(
    new Set(
      breakdown.history
        .filter((item) => item.protein)
        .map((item) => item.protein)
    )
  );
  const colours = getColors(proteins.length);
  const proteinColours = Object.fromEntries(
    proteins.map((protein, id) => [protein, colours[id]])
  );
  return (
    <PlotEnhancer
      data={breakdown.history.map((item) => ({
        y:
          item.eventType === 'dc' && item.status !== 'Successful'
            ? ['dc failed', 'dc failed']
            : [item.eventType, item.eventType],
        x: [item.startTime, item.endTime],
        text: item.subType,
        mode: 'lines',
        line: {
          width: 30,
          color: item.protein && proteinColours[item.protein],
        },
        dataCollectionId: item.dataCollectionId,
        sessionId: item.sessionId,
        proposal: item.proposal,
      }))}
      onClick={(e) => {
        const { points } = e;
        if (!points.length) return;
        const point = points[0];
        // @ts-expect-error
        const { proposal, sessionId, dataCollectionId } = point.data;
        if (!(proposal && sessionId && dataCollectionId)) return;
        navigate(
          `/proposals/${proposal}/sessions/${sessionId}?dataCollectionId=${dataCollectionId}`
        );
      }}
      layout={{
        yaxis: {
          //   visible: false,
          zeroline: false,
          //   range: [0.999, 1.001],
        },
      }}
    />
  );
}
