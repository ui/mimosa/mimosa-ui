import { useSuspense } from '@data-client/react';

import { TimesEndpoint } from 'api/resources/Stats/Times';
import { usePath } from 'hooks/usePath';
import PlotEnhancer from './PlotEnhancer';

interface SeriesType {
  label: string;
  color: string;
}

export const series: Record<string, SeriesType> = {
  startup: { label: 'Startup', color: 'yellow' },
  datacollection: { label: 'Data Collection', color: 'green' },
  centring: { label: 'Centring', color: 'cyan' },
  edge: { label: 'Energy Scans', color: 'orange' },
  xrf: { label: 'XFE Spectra', color: 'orange' },
  robot: { label: 'Sample Actions', color: 'blue' },
  thinking: { label: 'Thinking', color: 'purple' },
  remaining: { label: 'Remaining', color: 'red' },
  beamloss: { label: 'Beam Dump', color: 'black' },
};

export default function TimesPie() {
  const sessionId = usePath('sessionId');
  const times = useSuspense(TimesEndpoint, {
    ...(sessionId ? { sessionId } : null),
  });
  const { duration, ...plottableTimes } = times.average;
  return (
    <>
      <PlotEnhancer
        data={[
          {
            values: Object.values(plottableTimes),
            labels: Object.keys(plottableTimes).map(
              (seriesName) => series[seriesName].label
            ),
            type: 'pie',
            textposition: 'inside',
            marker: {
              colors: Object.keys(plottableTimes).map(
                (seriesName) => series[seriesName].color
              ),
            },
          },
        ]}
        layout={{
          title: {
            text: 'Breakdown of Total Session Time',
            font: { size: 14 },
          },
        }}
      />
    </>
  );
}
