import { useSuspense } from '@data-client/react';

import { BreakdownEndpoint } from 'api/resources/Stats/Breakdown';
import { usePath } from 'hooks/usePath';
import { SessionResource } from 'api/resources/Session';
import { Accordion, Col, Row } from 'react-bootstrap';
import formattedDateTime from 'utils/datetime';
import useBreakpoint from 'hooks/useBreakpoint';

export default function SessionOverview() {
  const sessionId = usePath('sessionId') || '0';
  const breakdown = useSuspense(BreakdownEndpoint, { sessionId });
  const session = useSuspense(SessionResource.get, { sessionId });
  const breakpoint = useBreakpoint();

  const { overview } = breakdown;
  return (
    <Accordion defaultActiveKey={breakpoint !== 'xs' ? '0' : undefined}>
      <Accordion.Item eventKey="0" className="border-0">
        {breakpoint === 'xs' && (
          <Accordion.Header className="mb-0 border p-0 rounded-top">
            Summary
          </Accordion.Header>
        )}
        <Accordion.Body className="p-0">
          <Row>
            <Col className="mt-1 text-nowrap fs-6" xs={12} sm={true}>
              <div className="rounded bg-light text-center p-1">
                Start: {formattedDateTime(session.startDate, false)}
              </div>
            </Col>
            <Col className="mt-1 text-nowrap fs-6" xs={12} sm={true}>
              <div className="rounded bg-light text-center p-1">
                End: {formattedDateTime(session.endDate, false)}
              </div>
            </Col>
            <Col className="mt-1 text-nowrap fs-6" xs={12} sm={true}>
              <div className="rounded bg-light text-center p-1">
                Beamline: {session.beamLineName}
              </div>
            </Col>
            <Col className="mt-1 text-nowrap fs-6" xs={12} sm={true}>
              <div className="rounded bg-light text-center p-1">
                Data Collections: {overview.counts.datacollections}
              </div>
            </Col>
            <Col className="mt-1 text-nowrap fs-6" xs={12} sm={true}>
              <div className="rounded bg-light text-center p-1">
                Failed: {overview.counts.failed}
              </div>
            </Col>
          </Row>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
