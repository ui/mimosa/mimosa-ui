import { debounce } from 'lodash';
import { useSuspense } from '@data-client/react';
import { Alert, Form, InputGroup } from 'react-bootstrap';
import {
  useSearchParams,
  createSearchParams,
  useNavigate,
} from 'react-router-dom';

import { DataCollectionTimesEndpoint } from 'api/resources/Stats/DataCollectionTimes';
import { usePath } from 'hooks/usePath';
import PlotEnhancer from './PlotEnhancer';
import { ChangeEvent, useRef } from 'react';

export default function DataCollectionTimes({
  beamLineName,
}: {
  beamLineName?: string;
}) {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  // @ts-ignore
  const searchParamsObj = Object.fromEntries([...searchParams]);

  const numPointsRef = useRef<HTMLInputElement>(null);
  const expTimeRef = useRef<HTMLInputElement>(null);
  const durationRef = useRef<HTMLInputElement>(null);

  const sessionId = usePath('sessionId');
  const datacollectionTimes = useSuspense(DataCollectionTimesEndpoint, {
    ...(beamLineName ? { beamLineName } : null),
    ...(sessionId ? { sessionId } : null),
  });

  const experimentType =
    (searchParamsObj.experimentType &&
      datacollectionTimes.beamlines &&
      datacollectionTimes.beamlines[0].experimentTypes &&
      datacollectionTimes.beamlines[0].experimentTypes.findIndex(
        (experimentType) =>
          experimentType.experimentType === searchParamsObj.experimentType
      )) ||
    0;

  if (!datacollectionTimes.beamlines) return null;
  if (!datacollectionTimes.beamlines.length) return null;
  if (!datacollectionTimes.beamlines[0].experimentTypes) return null;
  const maps = datacollectionTimes.beamlines[0].experimentTypes[experimentType];

  const data = maps.exposureTimes?.map((exposureTime) => ({
    name: exposureTime.exposureTime,
    x: exposureTime.times?.map((point) => point.numberOfImages),
    y: exposureTime.times?.map((point) => point.avgDuration),
    count: exposureTime.times?.map((point) => point.count),
    error_y: {
      type: 'data',
      symmetric: false,
      array: exposureTime.times?.map(
        (point) => point.maxDuration - point.avgDuration
      ),
      arrayminus: exposureTime.times?.map(
        (point) => point.avgDuration - point.minDuration
      ),
    },
  }));

  const dataFit = maps.exposureTimes?.map((exposureTime) => ({
    name: `${exposureTime.exposureTime} (R2=${exposureTime.fit?.r2})`,
    x: exposureTime.times?.map((point) => point.numberOfImages),
    y: exposureTime.times?.map(
      (point) =>
        point.numberOfImages * (exposureTime.fit?.slope || 0) +
        (exposureTime.fit?.intercept || 0)
    ),
  }));

  function selectExperimentType(e: React.ChangeEvent<HTMLInputElement>) {
    const { experimentType, ...exludeExperimentType } = searchParamsObj;
    navigate({
      pathname: '',
      search: createSearchParams({
        ...exludeExperimentType,
        experimentType: e.target.value,
      }).toString(),
    });
  }

  function calcDuration(e: ChangeEvent) {
    if (
      maps.fit?.slope &&
      maps.fit?.intercept &&
      expTimeRef.current &&
      expTimeRef.current.value &&
      numPointsRef.current &&
      numPointsRef.current.value
    ) {
      if (durationRef.current) {
        const duration =
          (maps.fit?.slope * parseFloat(expTimeRef.current?.value) +
            maps.fit?.intercept) *
          parseInt(numPointsRef.current?.value);
        durationRef.current.value = duration.toFixed(3);
      }
    }
  }

  const debouncedCalcDuration = debounce(calcDuration, 500);

  return (
    <>
      <Form>
        <Form.Control
          as="select"
          defaultValue={searchParamsObj.experimentType}
          onChange={selectExperimentType}
        >
          {datacollectionTimes.beamlines[0].experimentTypes.map(
            (experimentType) => (
              <option
                key={experimentType.experimentType}
                value={experimentType.experimentType}
              >
                {experimentType.experimentType}
              </option>
            )
          )}
        </Form.Control>
      </Form>
      <section className="mt-2">
        <Alert variant="light">
          duration (min) = ({maps.fit?.slope}*exposureTime +{' '}
          {maps.fit?.intercept} (R2=
          {maps.fit?.r2})) * numberOfPoints
        </Alert>
        <Form>
          <InputGroup>
            <InputGroup.Text>Exposure Time (ms)</InputGroup.Text>
            <Form.Control
              ref={expTimeRef}
              onChange={debouncedCalcDuration}
            ></Form.Control>
            <InputGroup.Text>Number of Points</InputGroup.Text>
            <Form.Control
              ref={numPointsRef}
              onChange={debouncedCalcDuration}
            ></Form.Control>
            <InputGroup.Text>Duration</InputGroup.Text>
            <Form.Control ref={durationRef}></Form.Control>
            <InputGroup.Text>min</InputGroup.Text>
          </InputGroup>
        </Form>
      </section>
      <PlotEnhancer
        //@ts-expect-error
        data={[...data, ...dataFit]}
        layout={{
          showlegend: true,
          title: {
            text: 'Data Collections Times',
            font: { size: 14 },
          },
          xaxis: {
            title: 'Number of Points',
          },
          yaxis: {
            title: 'Time (min)',
          },
        }}
      />
    </>
  );
}
