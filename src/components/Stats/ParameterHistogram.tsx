import { useSuspense } from '@data-client/react';

import { usePath } from 'hooks/usePath';
import PlotEnhancer from './PlotEnhancer';
import { ParameterHistogramsEndpoint } from 'api/resources/Stats/ParameterHistogram';

export default function ParameterHistogram({
  parameter,
  beamLineName,
}: {
  parameter: string;
  beamLineName?: string;
}) {
  const sessionId = usePath('sessionId');
  const parameterHistogram = useSuspense(ParameterHistogramsEndpoint, {
    parameter,
    ...(beamLineName ? { beamLineName } : null),
    ...(sessionId ? { sessionId } : null),
  });

  return (
    <>
      <PlotEnhancer
        data={parameterHistogram.beamLines.map((beamline) => ({
          x: beamline.bin,
          y: beamline.frequency,
          type: 'bar',
          name: beamline.beamLineName,
        }))}
        layout={{
          bargap: 0.1,
          xaxis: {
            title: `${parameterHistogram.parameter} (${parameterHistogram.unit})`,
          },
          yaxis: {
            title: 'Frequency',
          },
        }}
      />
    </>
  );
}
