import { Search } from 'react-bootstrap-icons';

import { IFrameFile } from 'api/resources/XHRFile';
import { ModalButton } from './Common/Modal';

export default function ButtonFileViewer({
  url,
  title,
  icon,
  buttonClasses,
}: {
  url: string;
  title: string;
  icon?: JSX.Element;
  buttonClasses?: string;
}) {
  return (
    <ModalButton
      title={title}
      buttonTitle={icon ? icon : <Search />}
      buttonClasses={buttonClasses}
    >
      {() => <IFrameFile src={url} style={{ width: '100%', height: '75vh' }} />}
    </ModalButton>
  );
}
