import type {
  BreadcrumbComponentProps,
  BreadcrumbComponentType,
  BreadcrumbMatch,
} from 'use-react-router-breadcrumbs';

import EventsRoutes from './Events';
import StatsRoutes from './Stats';
import { SessionRoutes, CalendarRoutes } from './Proposals';
import SampleRoutes from './Samples';
import ProteinRoutes, { ComponentsRoutes } from './Proteins';
import QueueRoutes from './Queue';

const ProposalBreadCrumb: BreadcrumbComponentType<'proposal'> = ({
  match,
}: BreadcrumbComponentProps<'proposal'>) => {
  return <>{match.params.proposal}</>;
};

const ProposalRoutes = {
  path: 'proposals/:proposal',
  breadcrumb: ProposalBreadCrumb,
  titleBreadcrumb: ({ match }: { match: BreadcrumbMatch<string> }) =>
    match.params.proposal,
  children: [
    SessionRoutes,
    CalendarRoutes,
    SampleRoutes,
    ProteinRoutes,
    ComponentsRoutes,
    EventsRoutes,
    StatsRoutes,
    QueueRoutes,
  ],
};

export default ProposalRoutes;
