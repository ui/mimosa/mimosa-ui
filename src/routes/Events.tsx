import React from 'react';
import type {
  BreadcrumbComponentProps,
  BreadcrumbComponentType,
  BreadcrumbMatch,
} from 'use-react-router-breadcrumbs';

const EventsList = React.lazy(() =>
  import('components/Events' /* webpackChunkName: "events" */).then((m) => ({
    default: m.EventsList,
  }))
);

const SampleChanger = React.lazy(() =>
  import('components/Samples' /* webpackChunkName: "samples" */).then((m) => ({
    default: m.SampleChanger,
  }))
);

const SessionBreadCrumb: BreadcrumbComponentType<'sessionId'> = ({
  match,
}: BreadcrumbComponentProps<'sessionId'>) => {
  return <>{match.params.sessionId}</>;
};

const EventsRoutes = {
  path: 'sessions',
  children: [
    {
      index: true,
      element: <EventsList refresh />,
      breadcrumb: 'Data Collections',
    },
    {
      path: ':sessionId',
      element: <EventsList refresh />,
      titleBreadcrumb: ({ match }: { match: BreadcrumbMatch<string> }) =>
        match.params.sessionId,
      breadcrumb: SessionBreadCrumb,
    },
    {
      path: ':sessionId/samples',
      element: <SampleChanger />,
      breadcrumb: 'Sample Changer',
    },
  ],
};

export default EventsRoutes;
