import {
  BreadcrumbsRoute,
  BreadcrumbMatch,
} from 'use-react-router-breadcrumbs';

import { ProposalsRoutes } from 'routes/Proposals';
import ProposalRoutes from 'routes/Proposal';
import AdminRoutes from 'routes/Admin';
import BeamLineRoutes from 'routes/BeamLine';

import PrivateRoute from 'components/Auth/PrivateRoute';
import React from 'react';

const Home = React.lazy(
  () => import('components/Home' /* webpackChunkName: "home" */)
);

const Calendar = React.lazy(
  () => import('components/Calendar' /* webpackChunkName: "calendar" */)
);

export interface TitledBreadcrumbsRoute extends BreadcrumbsRoute {
  titleBreadcrumb?: ({ match }: { match: BreadcrumbMatch<string> }) => string;
}

export function NotFound() {
  return <div>Cant find that page: 404</div>;
}

const routes: TitledBreadcrumbsRoute[] = [
  {
    path: '/',
    children: [
      {
        element: <PrivateRoute />,
        children: [
          {
            children: [
              { index: true, element: <Home />, breadcrumb: 'Home' },
              {
                path: 'calendar',
                element: <Calendar />,
                breadcrumb: 'Calendar',
              },
              ProposalsRoutes,
              ProposalRoutes,
              BeamLineRoutes,
              AdminRoutes,
            ],
          },
        ],
      },
      { path: '*', element: <NotFound /> },
    ],
  },
];

export default routes;
