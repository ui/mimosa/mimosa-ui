import React from 'react';

const SamplesList = React.lazy(() =>
  import('components/Samples' /* webpackChunkName: "samples" */).then((m) => ({
    default: m.SamplesList,
  }))
);

const ViewSample = React.lazy(() =>
  import('components/Samples').then((m) => ({
    default: m.ViewSample,
  }))
);

const SampleReview = React.lazy(() =>
  import('components/Samples').then((m) => ({
    default: m.SampleReview,
  }))
);

const ContainersList = React.lazy(() =>
  import('components/Samples').then((m) => ({
    default: m.ContainersList,
  }))
);

const ViewContainer = React.lazy(() =>
  import('components/Samples').then((m) => ({
    default: m.ViewContainer,
  }))
);

const SampleRoutes = {
  path: 'samples',
  children: [
    {
      index: true,
      element: <SamplesList focusSearch />,
      breadcrumb: 'Samples',
    },
    {
      path: 'containers',
      element: <ContainersList />,
      breadcrumb: 'Containers',
    },
    {
      path: 'containers/:containerId',
      element: <ViewContainer />,
      breadcrumb: 'Containers',
    },
    { path: 'review', element: <SampleReview />, breadcrumb: 'Review' },
    { path: ':blSampleId', element: <ViewSample />, breadcrumb: 'View' },
  ],
};

export default SampleRoutes;
