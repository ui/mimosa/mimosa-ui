import React from 'react';

const Options = React.lazy(() =>
  import('components/Admin' /* webpackChunkName: "admin" */).then((m) => ({
    default: m.Options,
  }))
);

const GroupsPermissions = React.lazy(() =>
  import('components/Admin').then((m) => ({
    default: m.GroupsPermissions,
  }))
);

const ViewGroup = React.lazy(() =>
  import('components/Admin').then((m) => ({
    default: m.ViewGroup,
  }))
);

const AdminRoutes = {
  path: 'admin',
  breadcrumb: 'Admin',
  children: [
    { path: 'options', element: <Options />, breadcrumb: 'Options' },
    {
      path: 'groups',
      children: [
        {
          index: true,
          element: <GroupsPermissions />,
          breadcrumb: 'Groups',
        },
        {
          path: ':userGroupId',
          element: <ViewGroup />,
          breadcrumb: 'View',
        },
      ],
    },
  ],
};

export default AdminRoutes;
