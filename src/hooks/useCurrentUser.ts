import { useSuspense } from '@data-client/react';
import { CurrentUserEndpoint } from 'api/resources/CurrentUser';

/**
 * Retrieve the current user
 * @returns
 */
export function useCurrentUser() {
  return useSuspense(CurrentUserEndpoint);
}
