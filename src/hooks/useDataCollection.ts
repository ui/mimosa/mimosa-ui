import { useSuspense } from '@data-client/react';
import { EventResource } from 'api/resources/Event';
import { DataCollection } from 'models/Event';

export function useDataCollection(dataCollectionId: number) {
  const dataCollections = useSuspense(EventResource.getList, {
    dataCollectionId,
  });
  if (
    dataCollections.results.length &&
    dataCollections.results[0].type === 'dc'
  )
    return dataCollections.results[0].Item as DataCollection;
}
