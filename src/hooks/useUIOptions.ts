import { useSuspense } from '@data-client/react';
import { UIOptionsEndpoint } from 'api/resources/UIOptions';

/**
 * Retrieve the UI Options
 * @returns
 */
export function useUIOptions() {
  return useSuspense(UIOptionsEndpoint);
}
