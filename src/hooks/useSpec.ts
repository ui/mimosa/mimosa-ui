import { retrieveSchema } from '@rjsf/utils';
import validator from '@rjsf/validator-ajv8';

import { useSuspense } from '@data-client/react';
import { OpenAPIEndpoint } from 'api/resources/Base/OpenAPI';
import { JSONSchema7 } from 'json-schema';

interface ExtendedJSONSchema7 extends JSONSchema7 {
  'ui:schema'?: Record<string, any>;
}

/**
 * Get the OpenAPI spec from the server
 * @returns - the OpenAPI resource
 */
export function useSpec() {
  return useSuspense(OpenAPIEndpoint);
}

/**
 * Get a specific schema from the OpenAPI spec
 * @param schema - the schema to get
 * @param title - a title to give the schema
 * @returns - the schema
 */
export function useSchema(
  schema: string,
  title: string,
  withRootComponents: boolean = false
): ExtendedJSONSchema7 {
  const spec = useSpec();
  const resolved = retrieveSchema(
    validator,
    { $ref: `#/components/schemas/${schema}` },
    spec as JSONSchema7
  );
  return {
    ...resolved,
    title: title,
    ...(withRootComponents ? { components: spec.components } : null),
  };
}

/**
 * Interpolates value placeholders in `ui:schema` with values from `formData`
 * Placeholders can come from the server definition of the `ui:schema` and be
 * replaced at runtime
 * @param uiSchema - the uiSchema
 * @param formData - the initial `formData`
 * @returns - the schema
 */
export function useUiSchemaWithFormData(
  uiSchema: Record<string, any>,
  formData: Record<string, any>
) {
  const uiSchemaCopy = JSON.parse(JSON.stringify(uiSchema)) as Record<
    string,
    any
  >;
  Object.entries(uiSchemaCopy).forEach(([field, options]) => {
    if ('ui:options' in options) {
      const uiOptions = options['ui:options'] as Record<string, any>;
      Object.entries(uiOptions).forEach(([optionKey, optionValue]) => {
        Object.keys(formData).forEach((dataKey) => {
          if (optionValue === `{${dataKey}}`) {
            uiOptions[optionKey] = formData[dataKey];
          }
        });
      });
    }
  });
  return uiSchemaCopy;
}
