import React from 'react';
import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';

import { createContext, useContext } from 'react';
import { useController } from '@data-client/react';

export interface AuthData {
  token: string;
  isAuthenticated: boolean;
  setToken: (token: string) => void;
  restoreToken: () => void;
  clearToken: () => void;
}

const AuthContext = createContext<AuthData>({
  token: '',
  isAuthenticated: false,
  setToken: (token: string) => undefined,
  restoreToken: () => undefined,
  clearToken: () => undefined,
});

export function AuthProvider({ children }: { children?: React.ReactNode }) {
  const [tokenState, setTokenState] = React.useState('');
  const [isAuthenticated, setIsAuthenticated] = React.useState(false);
  const { resetEntireStore } = useController();

  React.useEffect(() => {
    const tokenSession = window.localStorage.getItem('mimosa:token');
    if (tokenSession !== null) {
      setToken(tokenSession);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const setToken = (token: string) => {
    window.localStorage.setItem('mimosa:token', token);
    AuthenticatedEndpoint.accessToken = token;
    resetEntireStore();
    setTokenState(token);
    setIsAuthenticated(token.length > 0);
  };

  const restoreToken = () => {
    AuthenticatedEndpoint.accessToken = tokenState;
  };

  const clearToken = () => {
    setToken('');
  };

  return (
    <AuthContext.Provider
      value={{
        token: tokenState,
        isAuthenticated,
        setToken,
        restoreToken,
        clearToken,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

/**
 * Get the authentication state
 * @returns
 */
export function useAuth(): AuthData {
  return useContext(AuthContext);
}
