import { useNavigate, createSearchParams } from 'react-router-dom';
import { useSearchParamsObj } from './useSearchParamsObj';

interface HideImages {
  hiddenImages: number[];
  toggleImage: (imageId: number) => void;
  showAll: () => void;
}

export function useHideImages(): HideImages {
  const searchParamsObj = useSearchParamsObj();
  const navigate = useNavigate();
  const { hiddenImages, ...rest } = searchParamsObj;
  const hiddenImagesArr = hiddenImages
    ? hiddenImages
        .split(',')
        .map((blSampleImageId) => parseInt(blSampleImageId))
    : [];
  function toggleImage(blSampleImageId: number) {
    if (hiddenImagesArr.indexOf(blSampleImageId) > -1) {
      hiddenImagesArr.splice(hiddenImagesArr.indexOf(blSampleImageId), 1);
    } else {
      hiddenImagesArr.push(blSampleImageId);
    }

    navigate({
      pathname: '',
      search: createSearchParams({
        ...rest,
        ...(hiddenImagesArr.length
          ? { hiddenImages: hiddenImagesArr.join(',') }
          : null),
      }).toString(),
    });
  }

  function showAll() {
    navigate({
      pathname: '',
      search: createSearchParams({
        ...rest,
      }).toString(),
    });
  }

  return {
    hiddenImages: hiddenImagesArr,
    toggleImage,
    showAll,
  };
}
