import { useSearchParams } from 'react-router-dom';

/**
 * Get the query search parameters
 * @returns - the search parameters as an object
 */
export function useSearchParamsObj() {
  const [searchParams] = useSearchParams();
  return Object.fromEntries([...searchParams]);
}
