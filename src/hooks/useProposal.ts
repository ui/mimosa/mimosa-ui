import { matchPath, useLocation } from 'react-router-dom';

interface ProposalData {
  /**
   * The proposal name
   */
  proposalName: string | undefined;
}

/**
 * Get the current proposal from the path
 */
export function useProposal(): ProposalData {
  const { pathname } = useLocation();
  const match = matchPath('/proposals/:proposal/*', pathname);
  return {
    proposalName: match?.params.proposal,
  };
}
