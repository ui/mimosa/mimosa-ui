const customApiUrl = import.meta.env.VITE_API_URL || undefined;
const isProd = import.meta.env.PROD;
const requireHttps = import.meta.env.VITE_HTTPS === 'true';

const baseUrl =
  customApiUrl ||
  (isProd
    ? window.origin + '/api/v1'
    : `${requireHttps ? 'https' : 'http'}://localhost:8000/api/v1`);

const url = new URL(baseUrl);

const config = {
  baseUrl,
  host: `${url.protocol}//${url.host}`,
  path: url.pathname,
  routerBasename: import.meta.env.VITE_APP_BASENAME,
};

export default config;
