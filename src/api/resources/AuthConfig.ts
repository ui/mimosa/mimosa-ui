import { SiteEndpoint } from './Base/Site';
import { AuthConfigSingletonBase } from 'models/AuthConfig';

class AuthConfigEntity extends AuthConfigSingletonBase {
  static get key() {
    return 'AuthConfig';
  }
}

export const AuthConfigEndpoint = new SiteEndpoint({
  path: '/auth/config',
  schema: AuthConfigEntity,
});
