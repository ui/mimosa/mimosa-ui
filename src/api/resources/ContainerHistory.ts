import createPaginatedResource from './Base/Paginated';
import { ContainerHistoryBase } from 'models/ContainerHistory';

export class ContainerHistoryEntity extends ContainerHistoryBase {
  static get key() {
    return 'ContainerHistory';
  }
  readonly containerHistoryId: number;

  pk() {
    return this.containerHistoryId?.toString();
  }
}

export const ContainerHistoryResource = createPaginatedResource({
  path: '/containers/history/:containerHistoryId',
  schema: ContainerHistoryEntity,
});
