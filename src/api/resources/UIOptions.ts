import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { UIOptionsSingletonBase } from 'models/UIOptions';

class UIOptionsEntity extends UIOptionsSingletonBase {
  static get key() {
    return 'UIOptions';
  }
}

export const UIOptionsEndpoint = new AuthenticatedEndpoint({
  path: '/options/ui',
  schema: UIOptionsEntity,
});
