import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { DataHistogramBase } from 'models/DataHistogram';

export class H5HistogramEntity extends DataHistogramBase {
  static get key() {
    return 'H5Histogram';
  }
  readonly dataCollectionId: number;
  readonly path: string;
  readonly selection: any;

  pk() {
    return `${this.dataCollectionId}/${this.path}`;
  }
}

export const H5HistogramEndpoint = new AuthenticatedEndpoint({
  path: '/data/h5grove/histogram/',
  schema: H5HistogramEntity,
  searchParams: {} as {
    dataCollectionId: number;
    path: string;
    selection?: number;
  },
});
