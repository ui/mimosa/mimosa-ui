import createPaginatedResource from './Base/Paginated';
import { MapROIBase } from 'models/MapROI';

export class MapROIEntity extends MapROIBase {
  static get key() {
    return 'MapROI';
  }
  readonly xrfFluorescenceMappingROIId: number;

  pk() {
    return this.xrfFluorescenceMappingROIId?.toString();
  }
}

export const MapROIResource = createPaginatedResource({
  path: '/mapping/rois/:xrfFluorescenceMappingROIId',
  schema: MapROIEntity,
});
