import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { DataCollectionTimesBase } from 'models/DataCollectionTimes';

class DataCollectionTimesEntity extends DataCollectionTimesBase {
  static get key() {
    return 'DataCollectionTime';
  }
  readonly key: string;

  pk() {
    return this.key;
  }
}

export const DataCollectionTimesEndpoint = new AuthenticatedEndpoint({
  path: '/stats/datacollection/times',
  schema: DataCollectionTimesEntity,
  process(value, params) {
    value.key = params.sessionId
      ? `sessionId:${params.sessionId}`
      : params.runId
      ? `runId:${params.runId}/${params.beamLineName}`
      : params.beamLineName;
    return value;
  },
  searchParams: {} as {
    beamLineName?: string;
    runId?: string;
    sessionId?: string;
  },
});
