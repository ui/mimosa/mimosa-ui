import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { ParameterHistogramsBase } from 'models/ParameterHistograms';

class ParameterHistogramsEntity extends ParameterHistogramsBase {
  static get key() {
    return 'ParameterHistograms';
  }
  readonly key: string;

  pk() {
    return this.key;
  }
}

export const ParameterHistogramsEndpoint = new AuthenticatedEndpoint({
  path: '/stats/parameters/histogram',
  schema: ParameterHistogramsEntity,
  process(value, params) {
    value.key =
      params.parameter +
      (params.sessionId
        ? `sessionId:${params.sessionId}`
        : params.runId
        ? `runId:${params.runId}/${params.beamLineName}`
        : params.beamLineName);
    return value;
  },
  searchParams: {} as {
    beamLineName?: string;
    runId?: string;
    sessionId?: string;
    parameter?: string;
  },
});
