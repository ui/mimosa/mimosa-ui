import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { MapPixelValuesBase } from 'models/MapPixelValues';

class MapPixelValuesEntity extends MapPixelValuesBase {
  static get key() {
    return 'MapPixelValues';
  }
  readonly key: string;

  pk() {
    return this.key;
  }
}

export const MapPixelValuesEndpoint = new AuthenticatedEndpoint({
  path: '/mapping/pixel/values/:dataCollectionGroupId',
  schema: MapPixelValuesEntity,
  process(value, params) {
    value.key =
      params.x !== undefined
        ? `${params.dataCollectionGroupId}:${params.xrfFluorescenceMappingROIId}:${params.x}:${params.y}`
        : `${params.dataCollectionGroupId}:${params.xrfFluorescenceMappingROIId}`;
    return value;
  },
  searchParams: {} as {
    xrfFluorescenceMappingROIId: number;
    x?: number;
    y?: number;
  },
});
