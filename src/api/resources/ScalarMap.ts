import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { MapEntity } from './Map';

export const ScalarMapEndpoint = new AuthenticatedEndpoint({
  path: '/mapping/scalar',
  schema: MapEntity,
  method: 'POST',
});
