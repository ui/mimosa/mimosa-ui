import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { RegeneratedMapsResponseBase } from 'models/RegeneratedMapsResponse';

class RegeneratedMapsEntity extends RegeneratedMapsResponseBase {
  static get key() {
    return 'RegenerateMaps';
  }

  pk() {
    return `regenerated-maps-${Date.now()}`;
  }
}

export const RegenerateMapsEndpoint = new AuthenticatedEndpoint({
  path: '/mapping/regenerate',
  schema: RegeneratedMapsEntity,
  method: 'POST',
});
