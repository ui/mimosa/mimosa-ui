import createPaginatedResource from './Base/Paginated';
import { ContainerBase } from 'models/Container';

export class ContainerEntity extends ContainerBase {
  static get key() {
    return 'Container';
  }
  readonly containerId: number;

  pk() {
    return this.containerId?.toString();
  }
}

export const ContainerResource = createPaginatedResource({
  path: '/containers/:containerId',
  schema: ContainerEntity,
});

export const SessionContainerResource = createPaginatedResource({
  path: '/containers/session/:containerId',
  schema: ContainerEntity,
});
