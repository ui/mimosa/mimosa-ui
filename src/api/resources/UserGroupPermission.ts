import createPaginatedResource from './Base/Paginated';
import { PermissionBase } from 'models/Permission';

export class UserGroupPermissionEntity extends PermissionBase {
  static get key() {
    return 'UserGroupPermission';
  }
  readonly permissionId: number;

  pk() {
    return this.permissionId?.toString();
  }
}

export const UserGroupPermissionResource = createPaginatedResource({
  path: '/admin/groups/:userGroupId/permission/:permissionId',
  schema: UserGroupPermissionEntity,
});
