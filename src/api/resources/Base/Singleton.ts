import { Entity, Schema } from '@data-client/rest';

import { AuthenticatedEndpoint } from './Authenticated';
import { SiteEndpoint } from './Site';

export function createSingletonResource<U extends string, S extends Schema>({
  path,
  schema,
}: {
  readonly path: U;
  readonly schema: S;
}) {
  const get = new SiteEndpoint({
    path,
    schema,
  });

  return {
    get,
    partialUpdate: get.extend({ method: 'PATCH' }),
  };
}

export function createAuthenticatedSingletonResource<
  U extends string,
  S extends Schema
>({ path, schema }: { readonly path: U; readonly schema: S }) {
  const get = new AuthenticatedEndpoint({
    path,
    schema,
  });

  return {
    get,
    partialUpdate: get.extend({ method: 'PATCH' }),
  };
}

export class SingletonEntity extends Entity {
  pk() {
    return '1';
  }
}
