import type { RestGenerics } from '@data-client/rest';
import { RestEndpoint } from '@data-client/rest';
import config from 'config/config';

export class SiteEndpoint<
  O extends RestGenerics = any
> extends RestEndpoint<O> {
  public urlPrefix = config.baseUrl;
}
