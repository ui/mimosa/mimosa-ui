import type { Schema } from '@data-client/rest';
import { createResource, EndpointExtraOptions } from '@data-client/rest';

import { AuthenticatedEndpoint } from './Authenticated';

export default function createPaginatedResource<
  U extends string,
  S extends Schema
>({
  path,
  schema,
  endpointOptions,
}: {
  readonly path: U;
  readonly schema: S;
  readonly endpointOptions?: EndpointExtraOptions;
}) {
  const BaseResource = createResource({
    path,
    schema,
    Endpoint: AuthenticatedEndpoint,
    ...endpointOptions,
  });

  return {
    ...BaseResource,
    getList: BaseResource.getList.extend({
      schema: { results: [schema], total: 0, skip: 0, limit: 0 },
    }),
  };
}
