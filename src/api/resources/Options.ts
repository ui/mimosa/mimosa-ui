import { createAuthenticatedSingletonResource } from 'api/resources/Base/Singleton';
import { OptionsSingletonBase } from 'models/Options';

class OptionsEntity extends OptionsSingletonBase {
  static get key() {
    return 'Options';
  }
}

export const OptionsResource = createAuthenticatedSingletonResource({
  path: '/options',
  schema: OptionsEntity,
});
