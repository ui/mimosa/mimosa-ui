import { Entity } from '@data-client/rest';
import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { ImageHistogramBase } from 'models/ImageHistogram';

class ImageHeaderEntity extends Entity {
  static get key() {
    return 'ImageHeader';
  }
  readonly key: string;

  pk() {
    return this.key;
  }
}

export const ImageHeaderEndpoint = new AuthenticatedEndpoint({
  path: '/data/images/header',
  schema: ImageHeaderEntity,
  process(value, params) {
    value.key = `${params.dataCollectionId}:${params.imageNumber}`;
    return value;
  },
  searchParams: {} as {
    imageNumber: number;
    dataCollectionId?: string;
  },
});

class ImageHistogramEntity extends ImageHistogramBase {
  static get key() {
    return 'ImageHistogram';
  }
  readonly key: string;

  pk() {
    return this.key;
  }
}

export const ImageHistogramEndpoint = new AuthenticatedEndpoint({
  path: '/data/images/histogram',
  schema: ImageHistogramEntity,
  process(value, params) {
    value.key = `${params.dataCollectionId}:${params.imageNumber}`;
    return value;
  },
  searchParams: {} as {
    imageNumber: number;
    dataCollectionId?: string;
  },
});
