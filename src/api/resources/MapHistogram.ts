import createPaginatedResource from './Base/Paginated';
import { MapHistogramBase } from 'models/MapHistogram';

export class MapHistogramEntity extends MapHistogramBase {
  static get key() {
    return 'MapHistogram';
  }
  readonly xrfFluorescenceMappingId: number;

  pk() {
    return this.xrfFluorescenceMappingId?.toString();
  }
}

export const MapHistogramResource = createPaginatedResource({
  path: '/mapping/histogram/:xrfFluorescenceMappingId',
  schema: MapHistogramEntity,
});
