import createPaginatedResource from './Base/Paginated';
import { CompositeMapBase } from 'models/CompositeMap';

export class CompositeMapEntity extends CompositeMapBase {
  static get key() {
    return 'CompositeMap';
  }
  readonly xfeFluorescenceCompositeId: number;

  pk() {
    return this.xfeFluorescenceCompositeId?.toString();
  }
}

export const CompositeMapResource = createPaginatedResource({
  path: '/mapping/composites/:xfeFluorescenceCompositeId',
  schema: CompositeMapEntity,
});
