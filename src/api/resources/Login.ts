import { Entity } from '@data-client/rest';
import { SiteEndpoint } from './Base/Site';

class LoginEntity extends Entity {
  static get key() {
    return 'Login';
  }
  plugin: string;
  login: string;
  password: string;
  token: string;

  pk() {
    return this.login;
  }
}

export const LoginEndpoint = new SiteEndpoint({
  path: '/auth/login',
  schema: LoginEntity,
  method: 'POST',
});
