import { AuthenticatedEndpoint } from '../Base/Authenticated';
import { AvailableTasksSingletonBase } from 'models/AvailableTasks';

class AvailableTasksEntity extends AvailableTasksSingletonBase {
  static get key() {
    return 'AvailableTasks';
  }
}

export const AvailableTasksEndpoint = new AuthenticatedEndpoint({
  path: '/reprocess',
  schema: AvailableTasksEntity,
});
