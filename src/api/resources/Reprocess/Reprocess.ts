import { Entity } from '@data-client/rest';

export class ReprocessEntity extends Entity {
  static get key() {
    return 'Reprocess';
  }

  future_id: string;

  pk() {
    return this.future_id;
  }
}
