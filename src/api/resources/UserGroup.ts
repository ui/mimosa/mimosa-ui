import createPaginatedResource from './Base/Paginated';
import { UserGroupBase } from 'models/UserGroup';

export class UserGroupEntity extends UserGroupBase {
  static get key() {
    return 'UserGroup';
  }
  readonly userGroupId: number;

  pk() {
    return this.userGroupId?.toString();
  }
}

export const UserGroupResource = createPaginatedResource({
  path: '/admin/groups/:userGroupId',
  schema: UserGroupEntity,
});
