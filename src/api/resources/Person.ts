import createPaginatedResource from './Base/Paginated';
import { PersonBase } from 'models/Person';
export class PersonEntity extends PersonBase {
  static get key() {
    return 'Person';
  }
  readonly personId: number;

  pk() {
    return this.personId?.toString();
  }
}

export const PersonResource = createPaginatedResource({
  path: '/persons/:personId',
  schema: PersonEntity,
});
