import { AuthenticatedEndpoint } from 'api/resources/Base/Authenticated';
import { CurrentUserBase } from 'models/CurrentUser';

class CurrentUserEntity extends CurrentUserBase {
  static get key() {
    return 'CurrentUser';
  }
  readonly login: string;

  pk() {
    return this.login;
  }
}

export const CurrentUserEndpoint = new AuthenticatedEndpoint({
  path: '/user/current',
  schema: CurrentUserEntity,
});
