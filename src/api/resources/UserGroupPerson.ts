import createPaginatedResource from './Base/Paginated';
import { PersonBase } from 'models/Person';

export class UserGroupPersonEntity extends PersonBase {
  static get key() {
    return 'UserGroupPerson';
  }
  readonly personId: number;

  pk() {
    return this.personId?.toString();
  }
}

export const UserGroupPersonResource = createPaginatedResource({
  path: '/admin/groups/:userGroupId/person/:personId',
  schema: UserGroupPersonEntity,
});
