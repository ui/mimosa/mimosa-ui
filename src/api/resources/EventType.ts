import createPaginatedResource from './Base/Paginated';
import { EventTypeBase } from 'models/EventType';

export class EventTypeEntity extends EventTypeBase {
  static get key() {
    return 'EventType';
  }
  readonly eventType: string;

  pk() {
    return this.eventType;
  }
}

export const EventTypeResource = createPaginatedResource({
  path: '/events/types/:eventType',
  schema: EventTypeEntity,
});
