import createPaginatedResource from './Base/Paginated';
import { PermissionBase } from 'models/Permission';

export class PermissionEntity extends PermissionBase {
  static get key() {
    return 'Permission';
  }
  readonly permissionId: number;

  pk() {
    return this.permissionId?.toString();
  }
}

export const PermissionResource = createPaginatedResource({
  path: '/admin/permissions/:permissionId',
  schema: PermissionEntity,
});
