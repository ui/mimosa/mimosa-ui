# mimosa-ui

A react user interface for [mimosa](https://gitlab.esrf.fr/ui/mimosa/mimosa) forked from [py-ispyb-ui](https://github.com/ispyb/py-ispyb-ui)

Check the [latest documentation](https://ui.gitlab-pages.esrf.fr/mimosa/mimosa-ui)
